package br.com.travelcorporate.core.utils;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class CDIServiceLocator {

	private static InitialContext initialContext;

	private static BeanManager getBeanManager() {
		try {
			if (initialContext == null) {
				initialContext = new InitialContext();
			}

			return (BeanManager) initialContext.lookup("java:module/BeanManager");
		} catch (final NamingException e) {
			throw new RuntimeException("Não pôde encontrar BeanManager no JNDI.");
		}
	}

	public static <T> T getBean(Class<T> clazz) {
		return getBean(clazz, null);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(Class<T> clazz, Annotation qualificator) {
		final BeanManager bm = getBeanManager();

		final Set<Bean<?>> beans = bm.getBeans(clazz, qualificator);

		if (beans == null || beans.isEmpty()) {
			return null;
		}

		final Bean<T> bean = (Bean<T>) beans.iterator().next();

		final CreationalContext<T> ctx = bm.createCreationalContext(bean);
		final T o = (T) bm.getReference(bean, clazz, ctx);

		return o;
	}

}