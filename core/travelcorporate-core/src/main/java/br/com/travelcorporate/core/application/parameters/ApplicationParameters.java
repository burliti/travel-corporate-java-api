package br.com.travelcorporate.core.application.parameters;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ApplicationParameters implements Serializable {

	private static final long serialVersionUID = 1L;

	private int sessionTimeoutWeb;
	private int sessionTimeoutMobile;
	private int sessionTimeoutApi;

	public ApplicationParameters() {
		// Define padrao dos parametros

		// Timeout da WEB - 30 minutos
		this.setSessionTimeoutWeb(30);

		// Timeout do Mobile - 30 dias
		this.setSessionTimeoutMobile(60 * 24 * 30);

		// Timeout da API - 30 dias
		this.setSessionTimeoutApi(60 * 24 * 30);
	}

	/**
	 * Timeout Web for user sesions, in milliseconds
	 *
	 * @return Session Timeout
	 */
	public int getSessionTimeoutWebMilliSeconds() {
		return this.getSessionTimeoutWeb() * 1000 * 60;
	}

	public int getSessionTimeoutMobileMilliSeconds() {
		return this.getSessionTimeoutMobile() * 1000 * 60;
	}

	public int getSessionTimeoutWeb() {
		return this.sessionTimeoutWeb;
	}

	public void setSessionTimeoutWeb(int sessionTimeoutWeb) {
		this.sessionTimeoutWeb = sessionTimeoutWeb;
	}

	public int getSessionTimeoutMobile() {
		return this.sessionTimeoutMobile;
	}

	public void setSessionTimeoutMobile(int sessionTimeoutMobile) {
		this.sessionTimeoutMobile = sessionTimeoutMobile;
	}

	public int getSessionTimeoutApi() {
		return this.sessionTimeoutApi;
	}

	public void setSessionTimeoutApi(int sessionTimeoutApi) {
		this.sessionTimeoutApi = sessionTimeoutApi;
	}
}
