package br.com.travelcorporate.core.be;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.exceptions.ErrorField;
import br.com.travelcorporate.core.services.annotations.UserSession;
import br.com.travelcorporate.core.utils.CDIServiceLocator;
import br.com.travelcorporate.core.vo.IEntity;
import br.com.travelcorporate.core.vo.ISessaoUsuario;
import br.com.travelcorporate.core.vo.IgnoreEmpresa;

public abstract class AbstractService<K, V extends IEntity<K>, D extends AbstractDao<K, V>> {

	@Inject
	private Instance<D> dao;

	@Inject
	private EntityManager manager;

	// @Inject
	// @UserSession
	// private IEmpresa empresaSessao;

	protected D getDao() {
		return this.dao.get();
	}

	public void begin() {
		this.manager.getTransaction().begin();
	}

	public void end() {
		this.manager.getTransaction().commit();
	}

	public void back() {
		this.manager.getTransaction().rollback();
	}

	public void flush() {
		this.manager.flush();
	}

	public <T> void validate(T vo) throws BusinessException {
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		final Validator validator = factory.getValidator();

		final Set<ConstraintViolation<T>> constraintViolations = validator.validate(vo);

		if (constraintViolations.size() > 0) {
			final List<ErrorField> errors = new ArrayList<>();

			final Iterator<ConstraintViolation<T>> iterator = constraintViolations.iterator();

			while (iterator.hasNext()) {
				final ConstraintViolation<T> cv = iterator.next();
				errors.add(new ErrorField(cv.getPropertyPath().toString(), cv.getMessage()));
			}

			throw new BusinessException("Erro ao salvar dados", errors);
		}
	}

	public V inserir(V vo) throws BusinessException {
		if (this.getSessao() != null) {
			vo.setEmpresa(this.getSessao().getEmpresa());
		}

		this.doAntesInserir(vo);
		this.doAntesSalvar(vo);

		this.validate(vo);

		vo = this.getDao().inserir(vo);

		this.doDepoisInserir(vo);
		this.doDepoisSalvar(vo);

		return vo;
	}

	public V atualizar(V vo) throws BusinessException {

		final V buscar = this.getDao().buscar(vo);

		if (buscar == null) {
			throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + vo.getId());
		}

		if (!this.isIgnoreEmpresa()) {
			if (!buscar.getEmpresa().getId().equals(this.getSessao().getEmpresa().getId())) {
				throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + vo.getId());
			}
		}

		if (this.getSessao() != null) {
			vo.setEmpresa(this.getSessao().getEmpresa());
		}

		this.doAntesAtualizar(vo);
		this.doAntesSalvar(vo);

		this.validate(vo);

		vo = this.getDao().atualizar(vo);

		this.doDepoisAtualizar(vo);
		this.doDepoisSalvar(vo);

		return vo;
	}

	public V remover(K id) throws BusinessException {
		V vo = this.getDao().buscar(id);

		if (vo == null) {
			throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + id);
		}

		if (!this.isIgnoreEmpresa()) {
			if (!vo.getEmpresa().getId().equals(this.getSessao().getEmpresa().getId())) {
				throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + id);
			}
		}

		this.doAntesRemover(vo);

		vo = this.getDao().remover(vo);

		this.doDepoisRemover(vo);

		return vo;
	}

	public V remover(V vo) throws BusinessException {
		this.doAntesRemover(vo);

		vo = this.getDao().remover(vo);

		this.doDepoisRemover(vo);

		return vo;
	}

	public V buscar(V vo) {
		this.doAntesBuscar(vo);

		if (vo == null) {
			return null;
		}

		final V buscar = this.getDao().buscar(vo.getId());

		return this.doDepoisBuscar(buscar);

	}

	public V buscar(K id) {
		this.doAntesBuscar(id);

		V buscar = this.getDao().buscar(id);

		return this.doDepoisBuscar(buscar);
	}

	public ISessaoUsuario getSessao() {
		// Não dá pra injetar, então usa o CDI Service Locator
		ISessaoUsuario sessao = CDIServiceLocator.getBean(ISessaoUsuario.class, () -> UserSession.class);

		return sessao;
	}

	public List<V> consultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		if (filtros == null) {
			filtros = new HashMap<>();
		}
		// Injetar a sessao do usuario e filtrar pela empresa

		if (!this.isIgnoreEmpresa()) {
			// Somente pula o filtro se for a propria empresa
			if (this.getSessao().getEmpresa() == null) {
				return null;
			}

			filtros.put("empresa.id", this.getSessao().getEmpresa().getId().toString());
		}

		this.doAntesConsultar(filtros, pageSize, pageNumber, order);

		List<V> consultar = this.getDao().consultar(filtros, pageSize, pageNumber, order);

		this.doDepoisConsultar(consultar);

		return consultar;
	}

	public Integer total(Map<String, String> filtros) {
		if (filtros == null) {
			filtros = new HashMap<>();
		}
		// Injetar a sessao do usuario e filtrar pela empresa

		if (!this.isIgnoreEmpresa()) {
			// Somente pula o filtro se for a propria empresa
			if (this.getSessao().getEmpresa() == null) {
				return null;
			}

			filtros.put("empresa.id", this.getSessao().getEmpresa().getId().toString());
		}

		this.doAntesTotal(filtros);

		Integer total = this.getDao().total(filtros);

		this.doDepoisTotal(total);

		return total;
	}

	public void detach(V vo) {
		this.getDao().detach(vo);
	}

	public void detach(List<V> list) {
		list.forEach((v) -> this.getDao().detach(v));
	}

	public void doAntesSalvar(V vo) throws BusinessException {

	}

	public void doAntesInserir(V vo) throws BusinessException {

	}

	public void doAntesAtualizar(V vo) throws BusinessException {

	}

	public void doAntesRemover(V vo) throws BusinessException {

	}

	public void doAntesBuscar(V vo) {

	}

	public void doAntesBuscar(K id) {

	}

	public void doAntesConsultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {

	}

	public void doAntesTotal(Map<String, String> filtros) {

	}

	public void doDepoisSalvar(V vo) throws BusinessException {

	}

	public void doDepoisInserir(V vo) throws BusinessException {

	}

	public void doDepoisAtualizar(V vo) throws BusinessException {

	}

	public void doDepoisRemover(V vo) throws BusinessException {

	}

	public V doDepoisBuscar(V vo) {
		return vo;
	}

	public void doDepoisConsultar(List<V> consultar) {

	}

	public void doDepoisTotal(Integer total) {

	}

	/**
	 * Indica se a entidade deve ignorar a empresa
	 *
	 * @return True se a anotacao {@code @IgnoreEmpresa} estiver presente
	 */
	private boolean isIgnoreEmpresa() {
		return this.getVOClass().getAnnotation(IgnoreEmpresa.class) != null;
	}

	/**
	 * Retorna a classe VO que esta no Generics da classe.s
	 *
	 * @return Classe VO.
	 */
	@SuppressWarnings("unchecked")
	protected Class<V> getVOClass() {
		try {
			ParameterizedType type = (ParameterizedType) this.getClass().getSuperclass().getGenericSuperclass();
			return (Class<V>) type.getActualTypeArguments()[1];
		} catch (Exception e) {
			ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
			return (Class<V>) type.getActualTypeArguments()[1];
		}
	}
}