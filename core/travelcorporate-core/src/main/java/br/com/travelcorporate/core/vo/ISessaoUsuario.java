package br.com.travelcorporate.core.vo;

import java.util.Calendar;

import br.com.travelcorporate.core.enums.TipoSessao;

public interface ISessaoUsuario {

	Integer getId();

	void setId(Integer id);

	void setEmpresa(IEmpresa empresa);

	IEmpresa getEmpresa();

	Calendar getDataCriacao();

	void setDataCriacao(Calendar dataCriacao);

	Calendar getUltimaInteracao();

	void setUltimaInteracao(Calendar ultimaInteracao);

	String getEnderecoIp();

	void setEnderecoIp(String enderecoIp);

	String getHostName();

	void setHostName(String hostName);

	IFuncionario getFuncionario();

	void setFuncionario(IFuncionario usuario);

	TipoSessao getTipoSessao();

}