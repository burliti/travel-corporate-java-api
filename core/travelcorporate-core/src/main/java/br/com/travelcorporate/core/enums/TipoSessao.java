package br.com.travelcorporate.core.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoSessao {

	/*
	 * 1) Web 2) Mobile 3) Web API
	 */
	WEB(1, "Web"), MOBILE(2, "Mobile"), API(3, "Web API");

	private String descricao;
	private Integer valor;

	TipoSessao(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoSessao fromValue(String source) {
		for (final TipoSessao tipoSessao : TipoSessao.values()) {
			if (tipoSessao.getValor().toString().equals(source)) {
				return tipoSessao;
			} else if (tipoSessao.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return tipoSessao;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
