package br.com.travelcorporate.core.enums;

public interface IEnum<T> {
	T getValor();
}