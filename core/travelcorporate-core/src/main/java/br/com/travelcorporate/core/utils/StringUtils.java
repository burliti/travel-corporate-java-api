package br.com.travelcorporate.core.utils;

import java.text.Normalizer;

public class StringUtils {

	public static final String CHAIN_NUMEROS = "1234567890";
	public static final String FORMATO_DATA_PADRAO = "dd/MM/yyyy";
	public static final String FORMATO_DATAHORA_PADRAO = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMATO_DATAHORA_SEM_SEGUNDOS = "dd/MM/yyyy HH:mm";

	public static String filtrar(String source, String chain) {
		if (source == null || source.isEmpty()) {
			return "";
		}

		if (chain == null || chain.isEmpty()) {
			return source;
		}

		final StringBuilder sb = new StringBuilder();

		for (int i = 0; i < source.length(); i++) {
			final String s = source.substring(i, i + 1);
			if (chain.indexOf(s) >= 0) {
				sb.append(s);
			}
		}

		return sb.toString();
	}

	public static String removerAcentos(String source) {
		if (source == null || source.isEmpty()) {
			return "";
		}
		return Normalizer.normalize(source, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

	public static String filtrarNumero(String source) {
		return filtrar(source, CHAIN_NUMEROS);
	}
}
