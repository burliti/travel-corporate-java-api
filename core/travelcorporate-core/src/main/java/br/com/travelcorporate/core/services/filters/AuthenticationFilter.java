package br.com.travelcorporate.core.services.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.Provider;

import br.com.travelcorporate.core.enums.FormaLogoff;
import br.com.travelcorporate.core.enums.TipoSessao;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.exceptions.ErrorField;
import br.com.travelcorporate.core.services.ISessaoUsuarioService;
import br.com.travelcorporate.core.services.annotations.UserSession;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.core.utils.CDIServiceLocator;
import br.com.travelcorporate.core.utils.SegurancaUtils;
import br.com.travelcorporate.core.vo.ISessaoUsuario;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

	@Context
	private HttpServletRequest request;

	@Context
	private ResourceInfo resourceInfo;

	@Inject
	private ISessaoUsuarioService sessaoUsuarioService;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (this.resourceInfo == null) {
			return;
		}

		if (requestContext.getMethod().equalsIgnoreCase("OPTIONS")) {
			return;
		}

		AuthenticationRequired authenticationRequired = this.resourceInfo.getResourceMethod().getAnnotation(AuthenticationRequired.class);

		if (authenticationRequired == null) {
			authenticationRequired = this.resourceInfo.getResourceClass().getAnnotation(AuthenticationRequired.class);
		}

		// Não dá pra injetar, então usa o CDI Service Locator
		final ISessaoUsuario sessaoUsuario = CDIServiceLocator.getBean(ISessaoUsuario.class, () -> UserSession.class);

		// A regra do IP é só para a web, não para mobile
		if (sessaoUsuario != null && sessaoUsuario.getTipoSessao() == TipoSessao.WEB) {
			// Valida se a sessão é do mesmo IP

			final String ipAddress = SegurancaUtils.getIpFromRequest(this.request);
			final String computerName = SegurancaUtils.getComputerNameFromIP(ipAddress);

			if (!sessaoUsuario.getEnderecoIp().equals(ipAddress)) {
				// Expirar a sessao
				try {
					final String observacoes = "Ip original: [" + sessaoUsuario.getEnderecoIp() + "] Ip da Sessão: [" + ipAddress + "] Host original: [" + sessaoUsuario.getHostName() + "] Host da sessão: [" + computerName + "]";
					this.sessaoUsuarioService.logoff(sessaoUsuario, FormaLogoff.HOST_DIFERENTE, observacoes);

					ResponseBuilder builder = null;
					final ErrorField response = new ErrorField("Sessão inválida!");
					builder = Response.status(Response.Status.UNAUTHORIZED).encoding("UTF-8").entity(response);
					throw new WebApplicationException(builder.build());
				} catch (final BusinessException e) {
					e.printStackTrace();

					ResponseBuilder builder = null;
					final ErrorField response = new ErrorField(e.getMessage());
					builder = Response.status(Response.Status.UNAUTHORIZED).encoding("UTF-8").entity(response);
					throw new WebApplicationException(builder.build());
				}
			}

			// Atualizar a sessao (data da ultima interacao)
			try {
				this.sessaoUsuarioService.alive(sessaoUsuario);
			} catch (final BusinessException e) {
				e.printStackTrace();

				ResponseBuilder builder = null;
				final ErrorField response = new ErrorField(e.getMessage());
				builder = Response.status(Response.Status.UNAUTHORIZED).encoding("UTF-8").entity(response);
				throw new WebApplicationException(builder.build());
			}
		}

		if (authenticationRequired != null) {
			// Valida se ele tem sessão, cookies, etc.
			if (sessaoUsuario == null) {
				ResponseBuilder builder = null;
				final ErrorField response = new ErrorField("Autenticação requerida!");
				builder = Response.status(Response.Status.UNAUTHORIZED).encoding("UTF-8").entity(response);
				throw new WebApplicationException(builder.build());
			}
		}
	}
}