package br.com.travelcorporate.core.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FormatUtils {

	public static BigDecimal parseNumber(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		} else {
			String v = source.trim();
			v = v.replaceAll("%", "").replaceAll(" ", "");

			if (v.indexOf("R$") >= 0) {
				v = v.replaceAll("R", "").replaceAll("\\$", "").replace(".", "");
			} else {
				v = v.replaceAll("\\.", ",");
			}

			final NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));

			try {
				final Number parse = nf.parse(v);

				return new BigDecimal(parse.doubleValue());
			} catch (final ParseException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	public static Integer parseInt(String source) {
		try {
			return source != null ? Integer.parseInt(source.trim()) : null;
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static String formatMoney(BigDecimal source) {
		if (source != null) {
			final NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			return nf.format(source);
		}
		return null;
	}

	public static String formatNumber(BigDecimal source) {
		if (source != null) {
			final NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
			return nf.format(source);
		}
		return null;
	}

	public static String formatDateTime(Calendar dataHora) {
		if (dataHora == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATAHORA_PADRAO);
		return sdf.format(dataHora.getTime());
	}

	public static String formatDateTimeSemSegundos(Calendar dataHora) {
		if (dataHora == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATAHORA_SEM_SEGUNDOS);
		return sdf.format(dataHora.getTime());
	}

	public static String formatDate(Calendar dataHora) {
		if (dataHora == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATA_PADRAO);
		return sdf.format(dataHora.getTime());
	}

	public static Calendar parseDate(String data) {
		if (data == null) {
			return null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATA_PADRAO);
		try {
			Date date = sdf.parse(data);

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			return cal;

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Calendar parseDateTime(String dataHora) {
		if (dataHora == null) {
			return null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATAHORA_PADRAO);
		try {
			Date date = sdf.parse(dataHora);

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			return cal;

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Calendar parseDateTimeSemSegundos(String dataHora) {
		if (dataHora == null) {
			return null;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATAHORA_SEM_SEGUNDOS);
		try {
			Date date = sdf.parse(dataHora);

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			return cal;

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String fillLeft(Integer numero, String fillWith, int size) {
		return fillLeft(numero != null ? numero.toString() : "", fillWith, size);
	}

	public static String fillLeft(String numero, String fillWith, int size) {
		String s = numero == null ? "" : numero;

		while (s.length() < size) {
			s = fillWith + s;
		}

		return s;
	}
}