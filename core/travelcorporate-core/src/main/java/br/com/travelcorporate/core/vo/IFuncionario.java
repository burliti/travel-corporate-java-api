package br.com.travelcorporate.core.vo;

public interface IFuncionario {

	Integer getId();

	void setId(Integer id);

	String getNome();

	void setNome(String nome);

	String getEmail();

	void setEmail(String email);

	IEmpresa getEmpresa();

	void setEmpresa(IEmpresa empresa);
}