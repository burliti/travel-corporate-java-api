package br.com.travelcorporate.core.dao;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import br.com.travelcorporate.core.utils.DaoUtils;
import br.com.travelcorporate.core.utils.ReflectionUtils;
import br.com.travelcorporate.core.vo.IEntity;

public abstract class AbstractDao<K, V extends IEntity<K>> {

	@Inject
	private EntityManager manager;

	@Inject
	protected Logger log;

	public V buscar(K id) {
		if (id == null) {
			return null;
		}
		return this.manager.find(this.getVOClass(), id);
	}

	public V buscar(V vo) {
		if (vo == null || vo.getId() == null) {
			return null;
		}
		return this.manager.find(this.getVOClass(), vo.getId());
	}

	public V inserir(V vo) {
		return this.inserir(vo, true);
	}

	public V inserir(V vo, boolean autoIncrement) {
		if (autoIncrement) {
			vo.setId(null);
		}
		this.manager.persist(vo);
		return vo;
	}

	public V atualizar(V vo) {
		V vo2 = this.buscar(vo.getId());

		if (vo2 != null) {
			ReflectionUtils.copy(vo, vo2, false);

			// Clonar relacionamentos com @OneToMany
			vo2 = this.manager.merge(vo2);
		} else {
			throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + vo.getId());
		}

		return vo2;
	}

	public V remover(V vo) {
		if (vo == null) {
			throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID Nulo!");
		}

		final V entityToRemove = this.buscar(vo.getId());

		if (entityToRemove == null) {
			throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + vo.getId());
		}

		this.manager.remove(entityToRemove);

		return vo;
	}

	public V remover(K id) {
		final V entityToRemove = this.buscar(id);

		if (entityToRemove == null) {
			throw new EntityNotFoundException("Entidade " + this.getVOClass().getSimpleName() + " não encontrada com o ID " + id);
		}

		return this.remover(entityToRemove);
	}

	public List<V> todos() {
		return this.todos(null);
	}

	public List<V> todos(String orderBy) {
		this.manager.clear();

		final TypedQuery<V> query = this.manager.createQuery("SELECT e FROM " + this.getVOClass().getSimpleName() + " e " + (orderBy == null ? "" : " ORDER BY " + orderBy), this.getVOClass());

		return query.getResultList();
	}

	public List<V> consultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		this.manager.clear();

		if (filtros == null) {
			return null;
		}

		String hql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e ";
		hql += this.getCustomHQL();
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro

		final Map<String, Object> parametros = new HashMap<>();

		final String whereStatement = DaoUtils.buildWhereStatement(filtros, this.getVOClass(), parametros);

		hql += whereStatement;

		hql += this.getCustomFilter();

		if (order != null && order.size() > 0) {
			hql += " ORDER BY ";
			boolean first = true;
			final Iterator<String> iterator = order.keySet().iterator();

			while (iterator.hasNext()) {
				final String campo = iterator.next();

				if (!first) {
					hql += ", ";
				}

				hql += campo + " " + order.get(campo);
				first = false;
			}
		}

		this.log.debug("HQL: " + hql);

		final TypedQuery<V> query = this.manager.createQuery(hql, this.getVOClass());

		// Seta os parametros da query
		final Iterator<String> paramsIterator = parametros.keySet().iterator();

		while (paramsIterator.hasNext()) {
			final String paramName = paramsIterator.next();
			this.log.debug("Parametro: " + paramName + " valor: " + parametros.get(paramName));
			query.setParameter(paramName, parametros.get(paramName));
		}

		this.setCustomParameters(query);

		// Paginacao
		if (pageSize != null && pageNumber != null) {
			query.setMaxResults(pageSize);

			final int firstResult = (pageNumber - 1) * pageSize;

			query.setFirstResult(firstResult);

			this.log.debug("Page Number: " + pageNumber);
			this.log.debug("Page Size: " + pageSize);
			this.log.debug("Max results calculados: " + query.getMaxResults());
			this.log.debug("First result calculado: " + query.getFirstResult());
		}

		return query.getResultList();
	}

	public void setCustomParameters(Query query) {

	}

	public String getCustomHQL() {
		return "";
	}

	public String getCustomFilter() {
		return "";
	}

	public Integer total(Map<String, String> filtros) {
		this.manager.clear();

		if (filtros == null) {
			return null;
		}

		String hql = "SELECT COUNT(*) FROM " + this.getVOClass().getSimpleName() + " e ";
		hql += this.getCustomHQL();
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		final Map<String, Object> parametros = new HashMap<>();

		final String whereStatement = DaoUtils.buildWhereStatement(filtros, this.getVOClass(), parametros);

		hql += whereStatement;

		hql += this.getCustomFilter();

		this.log.debug("HQL: " + hql);

		final TypedQuery<Number> query = this.manager.createQuery(hql, Number.class);

		// Seta os parametros da query
		final Iterator<String> paramsIterator = parametros.keySet().iterator();

		while (paramsIterator.hasNext()) {
			final String paramName = paramsIterator.next();
			query.setParameter(paramName, parametros.get(paramName));
		}

		this.setCustomParameters(query);

		final int count = query.getSingleResult().intValue();

		return count;
	}

	public void detach(V vo) {
		if (vo != null) {
			this.getManager().detach(vo);
		}
	}

	/**
	 * Retorna a classe VO que esta no Generics da classe.s
	 *
	 * @return Classe VO.
	 */
	@SuppressWarnings("unchecked")
	protected Class<V> getVOClass() {
		try {
			final ParameterizedType type = (ParameterizedType) this.getClass().getSuperclass().getGenericSuperclass();
			return (Class<V>) type.getActualTypeArguments()[1];
		} catch (final Exception e) {
			final ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
			return (Class<V>) type.getActualTypeArguments()[1];
		}
	}

	public Logger getLog() {
		return this.log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	public EntityManager getManager() {
		return this.manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}
}
