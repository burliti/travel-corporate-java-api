package br.com.travelcorporate.core.vo;

public interface ISetor {

	Integer getId();

	void setId(Integer id);

	String getNome();

	void setNome(String nome);

	ISetor getPai();

	void setPai(ISetor pai);

	IEmpresa getEmpresa();

	void setEmpresa(IEmpresa empresa);
}
