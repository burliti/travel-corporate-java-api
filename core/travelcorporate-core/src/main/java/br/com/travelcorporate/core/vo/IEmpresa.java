package br.com.travelcorporate.core.vo;

import java.io.InputStream;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface IEmpresa {

	Integer getId();

	void setId(Integer id);

	String getNome();

	void setNome(String nome);

	String getLogo();

	@JsonIgnore
	InputStream getLogoStream();

	@JsonIgnore
	IEmpresa getEmpresaPrincipal();
}
