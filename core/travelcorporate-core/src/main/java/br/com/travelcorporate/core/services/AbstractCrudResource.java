package br.com.travelcorporate.core.services;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.exceptions.ErrorField;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.core.vo.IEntity;

public abstract class AbstractCrudResource<K, V extends IEntity<K>, D extends AbstractDao<K, V>, S extends AbstractService<K, V, D>> extends AbstractResource {

	@Inject
	private Instance<S> service;

	protected S getService() {
		return this.service.get();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response inserir(@Context HttpServletRequest request, V vo) throws BusinessException {
		try {
			final V inserir = this.getService().inserir(vo);

			this.doBeforeWriteVo(inserir);

			final CadastroResponse<V> retorno = new CadastroResponse<>(inserir, true, "Registro inserido com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {

			e.printStackTrace();

			final CadastroResponse<V> erro = new CadastroResponse<>(vo, false, e.getMessage());

			erro.setMessage(e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			} else {
				erro.setErrors(new ArrayList<>());
				erro.getErrors().add(new ErrorField(e.getMessage()));
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response atualizar(V vo) throws BusinessException {
		try {
			final V alterar = this.getService().atualizar(vo);

			this.doBeforeWriteVo(alterar);

			final CadastroResponse<V> retorno = new CadastroResponse<>(alterar, true, "Registro alterado com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<V> erro = new CadastroResponse<>(vo, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response remover(@PathParam("id") K id) throws BusinessException {
		try {
			final V excluir = this.getService().remover(id);
			final CadastroResponse<V> retorno = new CadastroResponse<>(excluir, true, "Registro excluído com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Throwable e) {
			final CadastroResponse<V> erro = new CadastroResponse<>(null, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@POST
	@Path("pesquisar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response pesquisar(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<V> lista = this.getService().consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
		final Integer total = this.getService().total(request.getFiltro());

		// Devolve a lista desatachada
		if (lista != null) {
			this.detach(lista);
		}

		this.doBeforeWriteListaPesquisar(lista);

		return this.json().status(Status.OK).entity(new ConsultaResponse(lista, total)).build();
	}

	public void detach(List<V> lista) {
		for (final V vo : lista) {
			this.detach(vo);
		}
	}

	public void detach(V vo) {
		this.getService().detach(vo);
	}

	public void doBeforeWriteListaPesquisar(List<V> lista) {
		if (lista != null) {
			for (final V vo : lista) {
				this.doBeforeWriteVo(vo);
			}
		}
	}

	public void doBeforeWriteVo(V vo) {

	}

	@POST
	@Path("validar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response validar(V vo) {
		try {
			this.getService().validate(vo);
			final CadastroResponse<V> retorno = new CadastroResponse<>(vo, true, "Dados validados com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<V> erro = new CadastroResponse<>(null, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response buscar(@PathParam("id") K id) {
		final V vo = this.getService().buscar(id);

		this.detach(vo);

		this.doBeforeWriteVo(vo);

		return this.json().status(Status.OK).entity(vo).build();
	}

	@OPTIONS
	@Path("pesquisar")
	public Response optionsPesquisar() {
		return this.getOptionsResponse();
	}

	@OPTIONS
	@Path("{id}")
	public Response optionsComId() {
		return this.getOptionsResponse();
	}

	@OPTIONS
	public Response optionsSemId() {
		return this.getOptionsResponse();
	}
}