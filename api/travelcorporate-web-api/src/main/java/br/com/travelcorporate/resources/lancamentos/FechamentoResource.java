package br.com.travelcorporate.resources.lancamentos;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.lancamentos.dao.FechamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;
import br.com.travelcorporate.model.lancamentos.service.FechamentoService;
import br.com.travelcorporate.model.lancamentos.service.LancamentoService;

@Path("/fechamento/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class FechamentoResource extends AbstractCrudResource<Integer, Fechamento, FechamentoDao, FechamentoService> {

	@Inject
	LancamentoService lancamentoService;

	@POST
	@Path("/lancamentos/{idFechamento}")
	public Response getLancamentosNaoFechados(@PathParam("idFechamento") Integer idFechamento) {
		Fechamento f = this.getService().buscar(idFechamento);

		return this.json().entity(this.lancamentoService.getLancamentosNaoFechados(f)).status(Status.OK).build();
	}

}