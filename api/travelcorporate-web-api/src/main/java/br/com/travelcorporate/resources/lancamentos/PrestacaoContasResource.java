package br.com.travelcorporate.resources.lancamentos;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.lancamentos.dao.LancamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.service.PrestacaoContasService;

@Path("/prestacao-contas/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class PrestacaoContasResource extends AbstractCrudResource<Integer, Lancamento, LancamentoDao, PrestacaoContasService> {

}