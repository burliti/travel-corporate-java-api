package br.com.travelcorporate.resources.geral;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.CadastroResponse;
import br.com.travelcorporate.core.services.annotations.UserSession;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.core.utils.CDIServiceLocator;
import br.com.travelcorporate.core.vo.ISessaoUsuario;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuario;
import br.com.travelcorporate.model.cadastros.service.SessaoUsuarioService;

@Path("/ping/v1")
public class PingResource extends AbstractResource {

	@Inject
	private SessaoUsuarioService sessaoUsuarioService;

	@GET
	@POST
	@AuthenticationRequired
	@Transactional
	public Response ping() {
		// Não dá pra injetar, então usa o CDI Service Locator
		final ISessaoUsuario sessao = CDIServiceLocator.getBean(ISessaoUsuario.class, () -> UserSession.class);

		try {
			if (sessao != null) {
				this.sessaoUsuarioService.alive(sessao);

				final CadastroResponse<SessaoUsuario> response = new CadastroResponse<>(null, true, null);

				return this.json().status(Response.Status.OK).entity(response).build();
			} else {
				final CadastroResponse<SessaoUsuario> response = new CadastroResponse<>(null, false, "Sessão não encontrada");

				return this.json().status(Response.Status.OK).entity(response).build();
			}
		} catch (final BusinessException e) {
			final CadastroResponse<SessaoUsuario> response = new CadastroResponse<>(null, false, e.getMessage());

			return this.json().status(Response.Status.OK).entity(response).build();
		}
	}
}
