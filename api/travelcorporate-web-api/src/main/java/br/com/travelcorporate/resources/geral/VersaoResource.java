package br.com.travelcorporate.resources.geral;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/versao/v1")
public class VersaoResource {

	@Resource(mappedName = "java:app/AppName")
	private String applicationName;

	public String getApplicationName() {
		return this.applicationName == null ? "" : this.applicationName;
	}

	@GET
	public String getVersao() {
		return this.getApplicationName().replaceAll("travelcorporate-web-api-", "");
	}

}
