package br.com.travelcorporate.security.jobs;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuario;
import br.com.travelcorporate.model.cadastros.service.SessaoUsuarioService;

@Singleton
@Startup
public class JobUserSessions {

	@Inject
	private transient Logger log;

	@Inject
	private SessaoUsuarioService sessaoUsuarioService;

	@Schedule(second = "0", hour = "*", minute = "*", persistent = false)
	public void run() {
		this.log.debug("Checando sessões...");

		for (SessaoUsuario sessaoUsuario : this.sessaoUsuarioService.getAtivos()) {
			try {
				this.sessaoUsuarioService.checkSession(sessaoUsuario);
			} catch (final BusinessException e) {
				e.printStackTrace();
			}
		}
	}
}