package br.com.travelcorporate.resources.cadastros;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dao.SetorDao;
import br.com.travelcorporate.model.cadastros.entities.Setor;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.cadastros.service.SetorService;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;

@Path("/setor/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class SetorResource extends AbstractCrudResource<Integer, Setor, SetorDao, SetorService> {

	@Inject
	private EmpresaService empresaService;

	@Inject
	private CentroCustoService centroCustoService;

	@POST
	@Path("/filiais")
	public Response getFiliais() {
		return this.json().entity(this.empresaService.getAtivos()).status(Status.OK).build();
	}

	@POST
	@Path("/centroCusto")
	public Response getCentrosCusto() {
		return this.json().entity(this.centroCustoService.getAtivos()).status(Status.OK).build();
	}
}
