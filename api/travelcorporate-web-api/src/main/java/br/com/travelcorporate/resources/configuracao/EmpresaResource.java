package br.com.travelcorporate.resources.configuracao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.configuracao.dao.EmpresaDao;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;

@Path("/empresa/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class EmpresaResource extends AbstractCrudResource<Integer, Empresa, EmpresaDao, EmpresaService> {

	@Inject
	private CentroCustoService centroCustoService;

	@POST
	@Path("/centroCusto")
	public Response getCentrosCusto() {
		return this.json().entity(this.centroCustoService.getAtivos()).status(Status.OK).build();
	}

}
