package br.com.travelcorporate.resources.cadastros;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dao.PoliticaDao;
import br.com.travelcorporate.model.cadastros.entities.Politica;
import br.com.travelcorporate.model.cadastros.service.PoliticaService;

@Path("/politica/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class PoliticaResource extends AbstractCrudResource<Integer, Politica, PoliticaDao, PoliticaService> {

}
