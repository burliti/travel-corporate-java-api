package br.com.travelcorporate.resources.geral;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.mensageria.services.FormaNotificacaoService;
import br.com.travelcorporate.model.tabelas.service.AcaoService;
import br.com.travelcorporate.model.tabelas.service.CidadeService;
import br.com.travelcorporate.model.tabelas.service.EstadoService;
import br.com.travelcorporate.model.tabelas.service.RecursoService;

@Path("/start/v1")
@Stateless
public class ApplicationResource {

	@Inject
	EstadoService estadoService;

	@Inject
	CidadeService cidadeService;

	@Inject
	AcaoService acaoService;

	@Inject
	RecursoService recursoService;

	@Inject
	FormaNotificacaoService formaNotificacaoService;

	@GET
	public void start() {
		try {
			// estadoService.criarEstados();
			// cidadeService.criarCidades();
			this.acaoService.createAcoes();
			this.recursoService.createRecursos();
			this.formaNotificacaoService.criarFormaNotificacaoPadrao();
		} catch (final BusinessException e) {
			e.printStackTrace();
		}
	}
}
