package br.com.travelcorporate.resources.lancamentos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;
import br.com.travelcorporate.model.lancamentos.dao.LancamentoDao;
import br.com.travelcorporate.model.lancamentos.dto.LancamentoDTO;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.service.AdiantamentoService;

@Path("/adiantamento/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class AdiantamentoResource extends AbstractCrudResource<Integer, Lancamento, LancamentoDao, AdiantamentoService> {

	@Inject
	private CategoriaService categoriaService;

	@Inject
	private FuncionarioService funcionarioService;

	@Override
	@POST
	@Path("pesquisar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response pesquisar(ConsultaRequest request) {

		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		List<LancamentoDTO> listaDTO = new ArrayList<>();

		final List<Lancamento> lista = this.getService().consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());

		lista.forEach(l -> {
			LancamentoDTO e = new LancamentoDTO();

			e.setId(l.getId());

			e.setFuncionario(new FuncionarioDTO());
			e.getFuncionario().setNome(l.getFuncionario().getNome());

			e.setCategoria(new CategoriaDTO());
			e.getCategoria().setNome(l.getCategoria().getNome());
			e.setDataInicial(l.getDataInicial());
			e.setDataFinal(l.getDataFinal());
			e.setTipoLancamento(l.getTipoLancamento());
			e.setValorLancamento(l.getValorLancamento());

			listaDTO.add(e);
		});

		final Integer total = this.getService().total(request.getFiltro());

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@POST
	@Path("/categorias/{tipo}")
	@AuthenticationRequired
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCategorias(@PathParam("tipo") TipoLancamento tipo) {
		ConsultaResponse response = new ConsultaResponse();

		response.setLista(this.categoriaService.getAtivos(tipo));
		response.setTotal(response.getLista().size());

		return this.json().entity(response).status(Status.OK).build();
	}

	@POST
	@Path("/categorias")
	@AuthenticationRequired
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCategorias() {
		ConsultaResponse response = new ConsultaResponse();

		response.setLista(this.categoriaService.getAtivos(null));
		response.setTotal(response.getLista().size());

		return this.json().entity(response).status(Status.OK).build();
	}

	@POST
	@Path("/funcionarios")
	@AuthenticationRequired
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFuncionarios() {
		ConsultaResponse response = new ConsultaResponse();

		response.setLista(this.funcionarioService.getFuncionariosAtivosDTO(true));
		response.setTotal(response.getLista().size());

		return this.json().entity(response).status(Status.OK).build();
	}
}
