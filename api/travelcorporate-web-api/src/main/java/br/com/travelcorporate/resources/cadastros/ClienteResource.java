package br.com.travelcorporate.resources.cadastros;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dao.ClienteDao;
import br.com.travelcorporate.model.cadastros.entities.Cliente;
import br.com.travelcorporate.model.cadastros.entities.Projeto;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.cadastros.service.ClienteService;
import br.com.travelcorporate.model.cadastros.service.ProjetoService;

@Path("/cliente/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class ClienteResource extends AbstractCrudResource<Integer, Cliente, ClienteDao, ClienteService> {

	@Inject
	private ProjetoService projetoService;

	@Inject
	private CentroCustoService centroCustoService;

	@POST
	@Path("/projetos")
	public Response getProjetos() {
		final List<Projeto> projetos = this.projetoService.getAtivos();

		for (final Projeto projeto : projetos) {
			this.projetoService.detach(projeto);
			projeto.setClientes(null);
		}

		return this.json().entity(projetos).status(Status.OK).build();
	}

	@POST
	@Path("/centroCusto")
	public Response getCentrosCusto() {
		return this.json().entity(this.centroCustoService.getAtivos()).status(Status.OK).build();
	}
}
