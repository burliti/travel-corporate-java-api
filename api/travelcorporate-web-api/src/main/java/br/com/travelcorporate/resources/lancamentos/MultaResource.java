package br.com.travelcorporate.resources.lancamentos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.lancamentos.dao.MultaDao;
import br.com.travelcorporate.model.lancamentos.dto.MultaDTO;
import br.com.travelcorporate.model.lancamentos.dto.MultaDadosDTO;
import br.com.travelcorporate.model.lancamentos.entities.Multa;
import br.com.travelcorporate.model.lancamentos.service.MultaService;
import br.com.travelcorporate.model.tabelas.entities.Cidade;
import br.com.travelcorporate.model.tabelas.entities.dto.CidadeDTO;
import br.com.travelcorporate.model.tabelas.entities.dto.EstadoDTO;
import br.com.travelcorporate.model.tabelas.service.CidadeService;

@Path("/multa/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class MultaResource extends AbstractCrudResource<Integer, Multa, MultaDao, MultaService> {

	private static final int TAMANHO_MAXIMO_PAGINA = 100;

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private CidadeService cidadeService;

	@Override
	@POST
	@Path("pesquisar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response pesquisar(ConsultaRequest request) {

		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		List<MultaDTO> listaDTO = new ArrayList<>();

		final List<Multa> lista = this.getService().consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());

		lista.forEach(multa -> {
			MultaDTO dto = new MultaDTO();

			dto.setId(multa.getId());

			dto.setDataHora(multa.getDataHora());
			dto.setNumeroMulta(multa.getNumeroMulta());
			dto.setValorMulta(multa.getValorMulta());
			dto.setTipoVeiculo(multa.getTipoVeiculo());
			dto.setNomeCidade(multa.getNomeCidade());
			dto.setUf(multa.getUf());

			if (multa.getFuncionario() != null) {
				dto.setFuncionario(new FuncionarioDTO());
				dto.getFuncionario().setId(multa.getFuncionario().getId());
				dto.getFuncionario().setNome(multa.getFuncionario().getNome());
			}

			listaDTO.add(dto);
		});

		final Integer total = this.getService().total(request.getFiltro());

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@Override
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response buscar(@PathParam("id") Integer id) {
		final Multa multa = this.getService().buscar(id);

		MultaDTO dto = new MultaDTO();

		dto.setId(multa.getId());

		dto.setUf(multa.getUf());
		dto.setDataHora(multa.getDataHora());
		dto.setNumeroMulta(multa.getNumeroMulta());
		dto.setTextoMulta(multa.getTextoMulta());
		dto.setValorMulta(multa.getValorMulta());
		dto.setCodigoMulta(multa.getCodigoMulta());
		dto.setTipoVeiculo(multa.getTipoVeiculo());
		dto.setNomeCidade(multa.getNomeCidade());

		if (multa.getCidade() != null) {
			dto.setCidade(new CidadeDTO());
			dto.getCidade().setId(multa.getCidade().getId());
			dto.getCidade().setNome(multa.getCidade().getNome());
			dto.getCidade().setEstado(new EstadoDTO());
			dto.getCidade().getEstado().setNome(multa.getCidade().getEstado().getNome());
			dto.getCidade().getEstado().setSigla(multa.getCidade().getEstado().getSigla());
		}

		if (multa.getFuncionario() != null) {
			dto.setFuncionario(new FuncionarioDTO());
			dto.getFuncionario().setId(multa.getFuncionario().getId());
			dto.getFuncionario().setNome(multa.getFuncionario().getNome());
		}

		return this.json().status(Status.OK).entity(dto).build();
	}

	@POST
	@Path("/dados")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response dados() {
		MultaDadosDTO dados = new MultaDadosDTO();

		this.funcionarioService.getAtivos().forEach((f) -> {
			FuncionarioDTO dto = new FuncionarioDTO();
			dto.setId(f.getId());
			dto.setNome(f.getNome());
			dados.getFuncionarios().add(dto);
		});

		return this.json().status(Status.OK).entity(dados).build();
	}

	@GET
	@Path("/cidades")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response cidades(@QueryParam("buscarPor") String buscarPor, @QueryParam("pagina") Integer pagina, @QueryParam("tamanhoPagina") Integer tamanhoPagina) {
		ConsultaRequest request = new ConsultaRequest();

		request.setPageNumber(pagina == null ? 1 : pagina);
		request.setPageSize(tamanhoPagina == null ? TAMANHO_MAXIMO_PAGINA : tamanhoPagina > TAMANHO_MAXIMO_PAGINA ? TAMANHO_MAXIMO_PAGINA : tamanhoPagina);
		request.getFiltro().put("nome", buscarPor);
		request.getOrder().put("nome", "ASC");

		List<CidadeDTO> listaDTO = new ArrayList<>();

		final List<Cidade> lista = this.cidadeService.consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());

		lista.forEach(cidade -> {
			CidadeDTO dto = new CidadeDTO();

			dto.setId(cidade.getId());
			dto.setNome(cidade.getNome());

			EstadoDTO estado = new EstadoDTO();
			estado.setSigla(cidade.getEstado().getSigla());
			dto.setEstado(estado);

			listaDTO.add(dto);
		});

		final Integer total = this.cidadeService.total(request.getFiltro());

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

}