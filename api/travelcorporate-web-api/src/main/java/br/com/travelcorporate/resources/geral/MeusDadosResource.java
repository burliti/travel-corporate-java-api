package br.com.travelcorporate.resources.geral;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.lancamentos.dto.FechamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.MeusDadosDTO;
import br.com.travelcorporate.model.lancamentos.dto.ViagemDTO;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import br.com.travelcorporate.model.lancamentos.service.ViagemService;

@Path("/meus-dados/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class MeusDadosResource extends AbstractResource {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private ViagemService viagemService;

	@OPTIONS
	@Produces(MediaType.APPLICATION_JSON)
	public Response optionsSemId() {
		return this.getOptionsResponse();
	}

	@POST
	@AuthenticationRequired
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMeusDados() {
		try {
			MeusDadosDTO dados = new MeusDadosDTO();

			Viagem viagem = this.viagemService.getUltimaViagemAprovadaSessao();
			ViagemDTO ultimaViagem = new ViagemDTO();

			if (viagem != null) {
				ultimaViagem.setId(viagem.getId());
				ultimaViagem.setFechamento(new FechamentoDTO());
				ultimaViagem.getFechamento().setSaldoFinal(viagem.getFechamento().getSaldoFinal());
			}

			Viagem viagemAberta = this.viagemService.getUltimaViagemAbertaSessao();
			ViagemDTO ultimaViagemAberta = new ViagemDTO();

			if (viagemAberta != null) {
				ultimaViagemAberta.setId(viagemAberta.getId());
				ultimaViagemAberta.setFechamento(new FechamentoDTO());
				ultimaViagemAberta.getFechamento().setSaldoFinal(viagemAberta.getFechamento().getSaldoFinal());
			}

			dados.setSaldo(this.funcionarioService.getSaldoFuncionarioSessao());
			dados.setViagensAberto(this.funcionarioService.getViagensEmAbertoSessao());

			dados.setUltimaViagem(ultimaViagem);
			dados.setUltimaViagemAberta(ultimaViagemAberta);

			return this.json().entity(dados).status(Status.OK).build();
		} catch (Exception e) {
			e.printStackTrace();
			return this.json().entity(e.getMessage()).status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
