package br.com.travelcorporate.resources.cadastros;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dao.FuncaoDao;
import br.com.travelcorporate.model.cadastros.entities.Funcao;
import br.com.travelcorporate.model.cadastros.service.FuncaoService;

@Path("/funcao/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class FuncaoResource extends AbstractCrudResource<Integer, Funcao, FuncaoDao, FuncaoService> {

}