package br.com.travelcorporate.resources.lancamentos;

import java.util.List;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.lancamentos.dao.ViagemDao;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import br.com.travelcorporate.model.lancamentos.service.ViagemService;

@Path("/viagem/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class ViagemResource extends AbstractCrudResource<Integer, Viagem, ViagemDao, ViagemService> {

	@Path("/minhas-viagens")
	@POST
	@AuthenticationRequired
	public Response minhasViagens(ConsultaRequest request) {
		final List<Viagem> lista = this.getService().getViagensUsuarioSessao(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
		final Integer total = this.getService().getTotalViagensFuncionarioSessao(request.getFiltro());

		// Devolve a lista desatachada
		if (lista != null) {
			this.detach(lista);
		}

		this.doBeforeWriteListaPesquisar(lista);

		return this.json().status(Status.OK).entity(new ConsultaResponse(lista, total)).build();
	}
}