package br.com.travelcorporate.resources.cadastros;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.configuracao.dao.CategoriaDao;
import br.com.travelcorporate.model.configuracao.entities.Categoria;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;

@Path("/categoria/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class CategoriaResource extends AbstractCrudResource<Integer, Categoria, CategoriaDao, CategoriaService> {

}
