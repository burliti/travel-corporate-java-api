package br.com.travelcorporate.resources.configuracao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.configuracao.dao.ConfiguracaoAprovacaoViagemDao;
import br.com.travelcorporate.model.configuracao.dto.ConfiguracaoDTO;
import br.com.travelcorporate.model.configuracao.dto.EmpresaDTO;
import br.com.travelcorporate.model.configuracao.entities.ConfiguracaoAprovacaoViagem;
import br.com.travelcorporate.model.configuracao.service.ConfiguracaoAprovacaoViagemService;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;

@Path("/configuracaoAprovacaoViagem/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class ConfiguracaoAprovacaoViagemResource extends AbstractCrudResource<Integer, ConfiguracaoAprovacaoViagem, ConfiguracaoAprovacaoViagemDao, ConfiguracaoAprovacaoViagemService> {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private EmpresaService empresaService;

	@POST
	@Path("/dados")
	public Response getCentrosCusto() {

		ConfiguracaoDTO dados = new ConfiguracaoDTO();

		this.funcionarioService.getAtivos().forEach((f) -> {
			FuncionarioDTO dto = new FuncionarioDTO();
			dto.setId(f.getId());
			dto.setNome(f.getNome());

			dados.getFuncionarios().add(dto);
		});

		this.empresaService.getAtivos().forEach((e) -> {
			EmpresaDTO dto = new EmpresaDTO();
			dto.setId(e.getId());
			dto.setNome(e.getNome());

			dados.getEmpresas().add(dto);
		});

		return this.json().entity(dados).status(Status.OK).build();
	}
}
