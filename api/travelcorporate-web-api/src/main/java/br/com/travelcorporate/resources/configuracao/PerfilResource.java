package br.com.travelcorporate.resources.configuracao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.configuracao.dao.PerfilDao;
import br.com.travelcorporate.model.configuracao.entities.Perfil;
import br.com.travelcorporate.model.configuracao.service.PerfilService;
import br.com.travelcorporate.model.tabelas.service.RecursoService;

@Path("/perfil/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class PerfilResource extends AbstractCrudResource<Integer, Perfil, PerfilDao, PerfilService> {

	@Inject
	private RecursoService recursoService;

	@POST
	@Path("/recursos")
	public Response getRecursos() {
		return this.json().status(Status.OK).entity(this.recursoService.getRecursosToNode()).build();
	}

	@Override
	public void doBeforeWriteVo(Perfil vo) {
		super.doBeforeWriteVo(vo);

		// Monta a lista de nodes preenchida
		vo.setNodes(this.recursoService.getRecursosToNode(vo, false));
	}
}
