package br.com.travelcorporate.resources.geral;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.CadastroResponse;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.SessaoUsuarioDTO;
import br.com.travelcorporate.model.cadastros.entities.AlterarSenhaRequest;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.entities.LoginRequest;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuario;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.configuracao.dto.PerfilDTO;
import br.com.travelcorporate.model.tabelas.service.RecursoService;

@Path("/acesso/v1")
@Stateless
@Transactional
public class AcessoResource extends AbstractResource {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private RecursoService recursoService;

	@OPTIONS
	public Response optionsSemId() {
		return this.getOptionsResponse();
	}

	@OPTIONS
	@Path("alterarSenha")
	public Response optionsAlterarSenha() {
		return this.getOptionsResponse();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response login(LoginRequest request) {
		if (request == null) {
			final CadastroResponse<SessaoUsuarioDTO> response = new CadastroResponse<>(null, false, "Dados não informados!");
			return this.json().entity(response).status(Status.BAD_REQUEST).build();
		}

		try {
			final SessaoUsuario userSession = this.funcionarioService.entrar(request.getEmail(), request.getSenhaMD5(), request.getTipoSessao());

			final Funcionario funcionario = (Funcionario) userSession.getFuncionario();

			FuncionarioDTO f = new FuncionarioDTO();
			f.setId(funcionario.getId());
			f.setCelular(funcionario.getCelular());
			f.setNome(funcionario.getNome());
			f.setStatus(funcionario.getStatus());
			f.setEmail(funcionario.getEmail());
			f.setResetarSenha(funcionario.getResetarSenha());

			f.setPerfil(new PerfilDTO());
			f.getPerfil().setId(funcionario.getPerfil().getId());
			f.getPerfil().setNome(funcionario.getPerfil().getNome());
			f.getPerfil().setNodes(this.recursoService.getRecursosToNode(funcionario.getPerfil(), !funcionario.isAdministrador()));

			SessaoUsuarioDTO s = new SessaoUsuarioDTO();
			s.setFuncionario(f);
			s.setId(userSession.getId());
			s.setChaveSessao(userSession.getChaveSessao());
			s.setDataCriacao(userSession.getDataCriacao());
			s.setTipoSessao(userSession.getTipoSessao());
			s.setUltimaInteracao(userSession.getUltimaInteracao());
			s.setEmpresa(userSession.getEmpresa());

			final CadastroResponse<SessaoUsuarioDTO> response = new CadastroResponse<>(s, true, "Usuário autenticado com sucesso!");

			return this.json().entity(response).status(Status.OK).build();
		} catch (final BusinessException e) {
			final CadastroResponse<SessaoUsuario> response = new CadastroResponse<>(null, false, e.getMessage());
			return this.json().entity(response).status(Status.OK).build();
		}
	}

	@POST
	@Path("/alterarSenha")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response alterarSenha(AlterarSenhaRequest request) {
		try {
			this.funcionarioService.alterarSenha(request.getSenha(), request.getNovaSenha(), request.getConfirmacaoNovaSenha());
			final CadastroResponse<SessaoUsuario> response = new CadastroResponse<>(null, true, "Senha alterada com sucesso!");
			return this.json().entity(response).status(Status.OK).build();
		} catch (final BusinessException e) {
			final CadastroResponse<SessaoUsuario> response = new CadastroResponse<>(null, false, e.getMessage());
			response.setErrors(e.getErrors());
			return this.json().entity(response).status(Status.OK).build();
		}
	}
}