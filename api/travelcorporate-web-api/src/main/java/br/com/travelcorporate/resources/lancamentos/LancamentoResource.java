package br.com.travelcorporate.resources.lancamentos;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.model.lancamentos.dao.LancamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.service.LancamentoService;

public class LancamentoResource extends AbstractCrudResource<Integer, Lancamento, LancamentoDao, LancamentoService> {

}