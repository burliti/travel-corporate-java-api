package br.com.travelcorporate.resources.cadastros;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dao.CentroCustoDao;
import br.com.travelcorporate.model.cadastros.entities.CentroCusto;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;

@Path("/centroCusto/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class CentroCursoResource extends AbstractCrudResource<Integer, CentroCusto, CentroCustoDao, CentroCustoService> {

}