package br.com.travelcorporate.resources.lancamentos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.exceptions.ErrorField;
import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.CadastroResponse;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.ClienteDTO;
import br.com.travelcorporate.model.cadastros.dto.ProjetoDTO;
import br.com.travelcorporate.model.cadastros.entities.Cliente;
import br.com.travelcorporate.model.cadastros.entities.Projeto;
import br.com.travelcorporate.model.cadastros.service.ClienteService;
import br.com.travelcorporate.model.cadastros.service.ProjetoService;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.configuracao.entities.Categoria;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;
import br.com.travelcorporate.model.lancamentos.dto.FechamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.ViagemDTO;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import br.com.travelcorporate.model.lancamentos.service.MinhasViagensService;

@Path("/minhas-viagens/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class MinhasViagensResource extends AbstractResource {

	@Inject
	private MinhasViagensService service;

	@Inject
	private ClienteService clienteService;

	@Inject
	private ProjetoService projetoService;

	@Inject
	private CategoriaService categoriaService;

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response buscar(@PathParam("id") Integer id) {
		final Viagem viagem = this.service.buscar(id);

		ViagemDTO v = this.service.convertViagemToDto(viagem);

		return this.json().status(Status.OK).entity(v).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response inserir(@Context HttpServletRequest request, Viagem vo) throws BusinessException {
		try {
			final Viagem inserir = this.service.inserir(vo);

			final CadastroResponse<ViagemDTO> retorno = new CadastroResponse<>(this.service.convertViagemToDto(inserir), true, "Registro inserido com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {

			e.printStackTrace();

			final CadastroResponse<ViagemDTO> erro = new CadastroResponse<>(this.service.convertViagemToDto(vo), false, e.getMessage());

			erro.setMessage(e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			} else {
				erro.setErrors(new ArrayList<>());
				erro.getErrors().add(new ErrorField(e.getMessage()));
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response atualizar(Viagem vo) throws BusinessException {
		try {
			final Viagem alterar = this.service.atualizar(vo);

			final CadastroResponse<ViagemDTO> retorno = new CadastroResponse<>(this.service.convertViagemToDto(alterar), true, "Registro alterado com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<ViagemDTO> erro = new CadastroResponse<>(this.service.convertViagemToDto(vo), false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@POST
	@Path("pesquisar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response pesquisar(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<Viagem> lista = this.service.consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());

		List<ViagemDTO> listaDTO = new ArrayList<>(lista.size());

		for (Viagem viagem : lista) {
			ViagemDTO v = new ViagemDTO();
			v.setDataIda(viagem.getDataIda());
			v.setDataVolta(viagem.getDataVolta());
			v.setId(viagem.getId());
			v.setSequencial(viagem.getSequencial());
			v.setStatus(viagem.getStatus());
			v.setDescricao(viagem.getDescricao());

			v.setFechamento(new FechamentoDTO());
			v.getFechamento().setSaldoAnterior(viagem.getFechamento().getSaldoAnterior());
			v.getFechamento().setSaldoFinal(viagem.getFechamento().getSaldoFinal());

			listaDTO.add(v);
		}

		final Integer total = this.service.total(request.getFiltro());

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	@Path("finalizar")
	public Response finalizar(Viagem vo) throws BusinessException {
		try {
			final Viagem alterar = this.service.finalizar(vo);

			final CadastroResponse<Viagem> retorno = new CadastroResponse<>(alterar, true, "Relatório de viagem finalizado com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<Viagem> erro = new CadastroResponse<>(vo, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response remover(@PathParam("id") Integer id) throws BusinessException {
		try {
			final Viagem excluir = this.service.remover(id);
			final CadastroResponse<Viagem> retorno = new CadastroResponse<>(excluir, true, "Registro excluído com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Throwable e) {
			final CadastroResponse<Viagem> erro = new CadastroResponse<>(null, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	@Path("clientes")
	public Response clientes() throws BusinessException {
		try {
			final List<Cliente> lista = this.clienteService.getAtivos();

			List<ClienteDTO> listaDTO = new ArrayList<>(lista.size());

			for (Cliente cliente : lista) {
				ClienteDTO c = new ClienteDTO();
				c.setId(cliente.getId());
				c.setNome(cliente.getNome());
				listaDTO.add(c);
			}

			return this.json().status(Status.OK).entity(listaDTO).build();
		} catch (final Exception e) {
			return this.json().status(Status.OK).entity(e.getMessage()).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	@Path("projetos")
	public Response projetos() throws BusinessException {
		try {
			final List<Projeto> lista = this.projetoService.getAtivos();

			List<ProjetoDTO> listaDTO = new ArrayList<>(lista.size());

			for (Projeto projeto : lista) {
				ProjetoDTO p = new ProjetoDTO();
				p.setId(projeto.getId());
				p.setNome(projeto.getNome());
				listaDTO.add(p);
			}

			return this.json().status(Status.OK).entity(listaDTO).build();
		} catch (final Exception e) {
			return this.json().status(Status.OK).entity(e.getMessage()).build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	@Path("categorias")
	public Response categorias() throws BusinessException {
		try {
			final List<Categoria> lista = this.categoriaService.getAtivos(null);

			List<CategoriaDTO> listaDTO = new ArrayList<>(lista.size());

			for (Categoria categoria : lista) {
				CategoriaDTO c = new CategoriaDTO();
				c.setId(categoria.getId());
				c.setNome(categoria.getNome());
				c.setTipo(categoria.getTipo());
				c.setInterna(categoria.getInterna());
				listaDTO.add(c);
			}

			return this.json().status(Status.OK).entity(listaDTO).build();
		} catch (final Exception e) {
			return this.json().status(Status.OK).entity(e.getMessage()).build();
		}
	}

	@GET
	@Produces("application/pdf")
	@AuthenticationRequired
	@Path("relatorio/{id}/{download}")
	public Response relatorioDeViagem(@PathParam("id") Integer id, @PathParam("download") boolean download) {
		ResponseBuilder response = Response.ok(this.service.relatorioDeViagem(id));

		if (download) {
			response.header("Content-Disposition", "attachment;filename=relatorio_de_viagem_" + id + ".pdf");
		}
		return response.build();
	}

	@GET
	@Produces("application/pdf")
	@AuthenticationRequired
	@Path("relatorio/{id}")
	public Response relatorioDeViagem(@PathParam("id") Integer id) {
		return this.relatorioDeViagem(id, false);
	}

	@OPTIONS
	@Path("pesquisar")
	public Response optionsPesquisar() {
		return this.getOptionsResponse();
	}

	@OPTIONS
	@Path("{id}")
	public Response optionsComId() {
		return this.getOptionsResponse();
	}

	@OPTIONS
	public Response optionsSemId() {
		return this.getOptionsResponse();
	}
}