package br.com.travelcorporate.resources.lancamentos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.CadastroResponse;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.lancamentos.dao.AprovacaoContasDao;
import br.com.travelcorporate.model.lancamentos.dto.FechamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.ViagemDTO;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import br.com.travelcorporate.model.lancamentos.service.AprovacaoContasService;

@Path("/aprovacao-contas/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class AprovacaoContasResource extends AbstractCrudResource<Integer, Viagem, AprovacaoContasDao, AprovacaoContasService> {

	@Override
	public Response pesquisar(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<Viagem> lista = this.getService().consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());

		List<ViagemDTO> listaDTO = new ArrayList<>(lista.size());

		for (Viagem viagem : lista) {
			ViagemDTO v = new ViagemDTO();
			v.setDataIda(viagem.getDataIda());
			v.setDataVolta(viagem.getDataVolta());
			v.setId(viagem.getId());
			v.setSequencial(viagem.getSequencial());
			v.setStatus(viagem.getStatus());

			v.setFechamento(new FechamentoDTO());
			v.getFechamento().setSaldoAnterior(viagem.getFechamento().getSaldoAnterior());
			v.getFechamento().setTotalCreditos(viagem.getFechamento().getTotalCreditos());
			v.getFechamento().setTotalDebitos(viagem.getFechamento().getTotalDebitos());
			v.getFechamento().setSaldoFinal(viagem.getFechamento().getSaldoFinal());

			v.setFuncionario(new FuncionarioDTO());
			v.getFuncionario().setNome(viagem.getFuncionario().getNome());

			listaDTO.add(v);
		}

		final Integer total = this.getService().total(request.getFiltro());

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	@Path("aprovar")
	public Response aprovar(Viagem vo) throws BusinessException {
		try {
			final Viagem alterar = this.getService().aprovar(vo);

			final CadastroResponse<Viagem> retorno = new CadastroResponse<>(alterar, true, "Relatório de viagem aprovado com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<Viagem> erro = new CadastroResponse<>(vo, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	@Path("rejeitar")
	public Response rejeitar(Viagem vo) throws BusinessException {
		try {
			final Viagem alterar = this.getService().rejeitar(vo);

			this.doBeforeWriteVo(alterar);

			final CadastroResponse<Viagem> retorno = new CadastroResponse<>(alterar, true, "Relatório de viagem rejeitado com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<Viagem> erro = new CadastroResponse<>(vo, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}
}
