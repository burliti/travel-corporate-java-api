package br.com.travelcorporate.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class ApplicationService extends Application {

	public ApplicationService() {
	}
}
