package br.com.travelcorporate.resources.cadastros;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dao.FuncionarioDao;
import br.com.travelcorporate.model.cadastros.dto.CentroCustoDTO;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.SetorDTO;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.cadastros.service.SetorService;
import br.com.travelcorporate.model.configuracao.dto.EmpresaDTO;
import br.com.travelcorporate.model.configuracao.dto.PerfilDTO;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.configuracao.service.PerfilService;
import br.com.travelcorporate.model.relatorios.exportacaodados.dto.ExportacaoDadosFiltrosDTO;

@Path("/funcionario/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class FuncionarioResource extends AbstractCrudResource<Integer, Funcionario, FuncionarioDao, FuncionarioService> {

	@Inject
	private PerfilService perfilService;

	@Inject
	private SetorService setorService;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private CentroCustoService centroCustoService;

	@Override
	@POST
	@Path("pesquisar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response pesquisar(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<Funcionario> lista = this.getService().consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
		final Integer total = this.getService().total(request.getFiltro());

		List<FuncionarioDTO> listaDTO = new ArrayList<>(lista.size());

		for (Funcionario funcionario : lista) {
			FuncionarioDTO f = new FuncionarioDTO();

			f.setId(funcionario.getId());
			f.setCelular(funcionario.getCelular());
			f.setNome(funcionario.getNome());
			f.setStatus(funcionario.getStatus());
			f.setEmail(funcionario.getEmail());

			f.setPerfil(new PerfilDTO());
			f.getPerfil().setId(funcionario.getPerfil().getId());
			f.getPerfil().setNome(funcionario.getPerfil().getNome());

			f.setSetor(new SetorDTO());

			if (funcionario.getSetor() != null) {
				f.getSetor().setId(funcionario.getSetor().getId());
				f.getSetor().setNome(funcionario.getSetor().getNome());
			}

			f.setSaldo(this.getService().getSaldoFuncionario(funcionario));

			listaDTO.add(f);
		}

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@Override
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response buscar(@PathParam("id") Integer id) {
		final Funcionario vo = this.getService().buscar(id);

		FuncionarioDTO f = new FuncionarioDTO();
		f.setId(vo.getId());
		f.setCodigoReferencia(vo.getCodigoReferencia());
		f.setAdministrador(vo.getAdministrador());
		f.setResetarSenha(vo.getResetarSenha());
		f.setStatus(vo.getStatus());
		f.setCelular(vo.getCelular());
		f.setEmail(vo.getEmail());
		f.setNome(vo.getNome());

		f.setPerfil(new PerfilDTO());
		f.getPerfil().setId(vo.getPerfil().getId());
		f.getPerfil().setNome(vo.getPerfil().getNome());

		f.setSetor(new SetorDTO());

		if (vo.getSetor() != null) {
			f.getSetor().setId(vo.getSetor().getId());
			f.getSetor().setNome(vo.getSetor().getNome());
		}

		f.setFilial(new EmpresaDTO());

		if (vo.getFilial() != null) {
			f.getFilial().setId(vo.getFilial().getId());
			f.getFilial().setNome(vo.getFilial().getNome());
		}

		f.setCentroCusto(new CentroCustoDTO());

		if (vo.getCentroCusto() != null) {
			f.getCentroCusto().setId(vo.getCentroCusto().getId());
			f.getCentroCusto().setNome(vo.getCentroCusto().getNome());
		}

		return this.json().status(Status.OK).entity(f).build();
	}

	@POST
	@Path("/dados")
	public Response getDados() {

		ExportacaoDadosFiltrosDTO filtros = new ExportacaoDadosFiltrosDTO();

		this.setorService.getAtivos().forEach(s -> {
			SetorDTO dto = new SetorDTO();
			dto.setId(s.getId());
			dto.setNome(s.getNome());
			filtros.getSetores().add(dto);
		});

		this.empresaService.getAtivos().forEach(e -> {
			EmpresaDTO dto = new EmpresaDTO();
			dto.setId(e.getId());
			dto.setNome(e.getNome());
			dto.setFantasia(e.getFantasia());
			filtros.getEmpresas().add(dto);
		});

		this.centroCustoService.getAtivos().forEach(c -> {
			CentroCustoDTO dto = new CentroCustoDTO();
			dto.setId(c.getId());
			dto.setCodigo(c.getCodigo());
			dto.setNome(c.getNome());
			filtros.getCentrosCusto().add(dto);
		});

		this.perfilService.getAtivos().forEach(c -> {
			PerfilDTO dto = new PerfilDTO();
			dto.setId(c.getId());
			dto.setNome(c.getNome());
			filtros.getPerfis().add(dto);
		});

		return this.json().status(Status.OK).entity(filtros).build();
	}
}