package br.com.travelcorporate.resources.lancamentos;

import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.Path;

import br.com.travelcorporate.core.services.AbstractCrudResource;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.lancamentos.dao.ReciboDao;
import br.com.travelcorporate.model.lancamentos.entities.Recibo;
import br.com.travelcorporate.model.lancamentos.service.ReciboService;

@Path("/recibo/v1")
@Stateless
@Transactional
@AuthenticationRequired
public class ReciboResource extends AbstractCrudResource<Integer, Recibo, ReciboDao, ReciboService> {

}
