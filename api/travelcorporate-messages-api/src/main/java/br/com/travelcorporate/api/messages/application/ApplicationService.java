package br.com.travelcorporate.api.messages.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class ApplicationService extends Application {

	public ApplicationService() {
	}
}
