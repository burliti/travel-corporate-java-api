package br.com.travelcorporate.api.messages.resources.messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.CadastroResponse;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.geral.enums.TipoExclusaoMensagem;
import br.com.travelcorporate.model.mensageria.dto.FormaNotificacaoDTO;
import br.com.travelcorporate.model.mensageria.dto.FormaNotificacaoEmpresaDTO;
import br.com.travelcorporate.model.mensageria.dto.MensagemDTO;
import br.com.travelcorporate.model.mensageria.dto.MensagemDadosDTO;
import br.com.travelcorporate.model.mensageria.entities.Mensagem;
import br.com.travelcorporate.model.mensageria.services.FormaNotificacaoEmpresaService;
import br.com.travelcorporate.model.mensageria.services.MinhasMensagensService;

@Path("minhas-mensagens/v1")
@AuthenticationRequired
public class MinhasMensagensResource extends AbstractResource {

	@Inject
	private MinhasMensagensService service;

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private FormaNotificacaoEmpresaService formaNotificacaoEmpresaService;

	@POST
	@Path("/salvar")
	public Response salvarMensagem(Mensagem vo) {
		try {
			final Mensagem mensagem = this.service.salvarRascunho(vo);

			final CadastroResponse<Mensagem> retorno = new CadastroResponse<>(mensagem, true, "Mensagem salva com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<Mensagem> erro = new CadastroResponse<>(vo, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@POST
	@Path("/enviar")
	public Response enviarMensagem(Mensagem vo) {
		try {
			final Mensagem mensagem = this.service.enviar(vo);

			final CadastroResponse<Mensagem> retorno = new CadastroResponse<>(mensagem, true, "Mensagem enviada com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Exception e) {
			final CadastroResponse<Mensagem> erro = new CadastroResponse<>(vo, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@POST
	@Path("/recebidas")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response recebidas(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<Mensagem> lista = this.service.recebidas(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
		final Integer total = this.service.totalRecebidas(request.getFiltro());

		List<MensagemDTO> listaDTO = new ArrayList<>();

		lista.forEach((m) -> listaDTO.add(this.service.convertToDTO(m)));

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@POST
	@Path("/marcarLida/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response marcarLida(@PathParam("id") Integer id) {

		try {
			this.service.marcarLida(id);

			final CadastroResponse<Mensagem> retorno = new CadastroResponse<>(null, true, "OK");

			return this.json().status(Status.OK).entity(retorno).build();

		} catch (final Exception e) {
			final CadastroResponse<Mensagem> erro = new CadastroResponse<>(null, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@POST
	@Path("/dados")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dados() {

		MensagemDadosDTO dados = new MensagemDadosDTO();

		this.funcionarioService.getAtivos().forEach((f) -> {
			FuncionarioDTO dto = new FuncionarioDTO();
			dto.setId(f.getId());
			dto.setNome(f.getNome());
			dados.getFuncionarios().add(dto);
		});

		this.formaNotificacaoEmpresaService.getAtivos().forEach((f) -> {
			FormaNotificacaoEmpresaDTO dto = new FormaNotificacaoEmpresaDTO();
			dto.setId(f.getId());
			dto.setFormaNotificacao(new FormaNotificacaoDTO());
			dto.getFormaNotificacao().setId(f.getFormaNotificacao().getId());
			dto.getFormaNotificacao().setNome(f.getFormaNotificacao().getNome());
			dados.getFormasNotificacao().add(dto);
		});

		return this.json().status(Status.OK).entity(dados).build();
	}

	@POST
	@Path("/totais")
	@Produces(MediaType.APPLICATION_JSON)
	public Response totais() {

		MensagemDadosDTO dados = new MensagemDadosDTO();

		dados.setTotalEnviadas(this.service.totalEnviadas(new HashMap<>()));
		dados.setTotalRecebidas(this.service.totalRecebidas(new HashMap<>()));
		dados.setTotalRascunhos(this.service.totalRascunhos(new HashMap<>()));

		return this.json().status(Status.OK).entity(dados).build();
	}

	@POST
	@Path("/enviadas")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response enviadas(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<Mensagem> lista = this.service.enviadas(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
		final Integer total = this.service.totalEnviadas(request.getFiltro());

		List<MensagemDTO> listaDTO = new ArrayList<>();

		lista.forEach((m) -> listaDTO.add(this.service.convertToDTO(m)));

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@POST
	@Path("/rascunhos")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response rascunhos(ConsultaRequest request) {
		if (request == null) {
			return this.json().status(Status.BAD_REQUEST).entity(new ConsultaResponse()).build();
		}

		final List<Mensagem> lista = this.service.rascunhos(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
		final Integer total = this.service.totalRascunhos(request.getFiltro());

		List<MensagemDTO> listaDTO = new ArrayList<>();

		lista.forEach((m) -> listaDTO.add(this.service.convertToDTO(m)));

		return this.json().status(Status.OK).entity(new ConsultaResponse(listaDTO, total)).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response buscar(@PathParam("id") Integer id) {
		final Mensagem vo = this.service.buscar(id);

		return this.json().status(Status.OK).entity(this.service.convertToDTOFull(vo)).build();
	}

	@DELETE
	@Path("{id}/{tipo}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response remover(@PathParam("id") Integer id, @PathParam("tipo") String tipo) throws BusinessException {
		try {
			final Mensagem excluir = this.service.remover(id, TipoExclusaoMensagem.fromString(tipo));
			final CadastroResponse<Mensagem> retorno = new CadastroResponse<>(excluir, true, "Registro excluído com sucesso!");

			return this.json().status(Status.OK).entity(retorno).build();
		} catch (final Throwable e) {
			final CadastroResponse<Mensagem> erro = new CadastroResponse<>(null, false, e.getMessage());

			if (e instanceof BusinessException) {
				erro.setErrors(((BusinessException) e).getErrors());
			}
			return this.json().status(Status.OK).entity(erro).build();
		}
	}

	@OPTIONS
	@Path("{id}")
	public Response optionsComId() {
		return this.getOptionsResponse();
	}

	@OPTIONS
	public Response optionsSemId() {
		return this.getOptionsResponse();
	}

}
