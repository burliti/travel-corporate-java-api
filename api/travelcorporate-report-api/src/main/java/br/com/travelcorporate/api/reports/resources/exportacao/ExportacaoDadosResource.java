package br.com.travelcorporate.api.reports.resources.exportacao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.ConsultaResponse;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.ClienteDTO;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.ProjetoDTO;
import br.com.travelcorporate.model.cadastros.dto.SetorDTO;
import br.com.travelcorporate.model.cadastros.service.ClienteService;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.cadastros.service.ProjetoService;
import br.com.travelcorporate.model.cadastros.service.SetorService;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.configuracao.dto.EmpresaDTO;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.lancamentos.dto.FechamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.LancamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.ViagemDTO;
import br.com.travelcorporate.model.lancamentos.service.AdiantamentoService;
import br.com.travelcorporate.model.lancamentos.service.ViagemService;
import br.com.travelcorporate.model.relatorios.exportacaodados.dto.ExportacaoDadosFiltrosDTO;
import br.com.travelcorporate.model.relatorios.exportacaodados.service.ExportacaoDadosService;

@Path("/exportacao/v1")
@Stateless
@AuthenticationRequired
public class ExportacaoDadosResource extends AbstractResource {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private SetorService setorService;

	@Inject
	private CategoriaService categoriaService;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private ProjetoService projetoService;

	@Inject
	private ClienteService clienteService;

	@Inject
	private ViagemService viagemService;

	@Inject
	private AdiantamentoService adiantamentoService;

	@Inject
	private ExportacaoDadosService exportacaoDadosService;

	@GET
	@Path("/dados")
	@AuthenticationRequired
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dadosParaFiltros() {
		ExportacaoDadosFiltrosDTO filtros = new ExportacaoDadosFiltrosDTO();

		this.setorService.getAtivos().forEach(s -> {
			SetorDTO dto = new SetorDTO();
			dto.setId(s.getId());
			dto.setNome(s.getNome());
			filtros.getSetores().add(dto);
		});

		this.funcionarioService.getAtivos().forEach(f -> {
			FuncionarioDTO dto = new FuncionarioDTO();
			dto.setId(f.getId());
			dto.setNome(f.getNome());
			filtros.getFuncionarios().add(dto);
		});

		this.categoriaService.getAtivos().forEach(c -> {
			CategoriaDTO dto = new CategoriaDTO();
			dto.setId(c.getId());
			dto.setNome(c.getNome());
			filtros.getCategorias().add(dto);
		});

		this.empresaService.getAtivos().forEach(e -> {
			EmpresaDTO dto = new EmpresaDTO();
			dto.setId(e.getId());
			dto.setNome(e.getNome());
			dto.setFantasia(e.getFantasia());
			filtros.getEmpresas().add(dto);
		});

		this.projetoService.getAtivos().forEach(p -> {
			ProjetoDTO dto = new ProjetoDTO();
			dto.setId(p.getId());
			dto.setNome(p.getNome());
			filtros.getProjetos().add(dto);
		});

		this.clienteService.getAtivos().forEach(c -> {
			ClienteDTO dto = new ClienteDTO();
			dto.setId(c.getId());
			dto.setNome(c.getNome());
			filtros.getClientes().add(dto);
		});

		return this.json().entity(filtros).status(Status.OK).build();
	}

	@Path("/preview/viagem")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response previewViagem(ConsultaRequest request) {
		List<ViagemDTO> list = new ArrayList<>();

		// @formatter:off
		this.viagemService
			.consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder())
			.forEach(viagem -> {
					ViagemDTO v = new ViagemDTO();
					v.setDataIda(viagem.getDataIda());
					v.setDataVolta(viagem.getDataVolta());
					v.setId(viagem.getId());
					v.setSequencial(viagem.getSequencial());
					v.setStatus(viagem.getStatus());
					v.setDescricao(viagem.getDescricao());

					v.setFechamento(new FechamentoDTO());
					v.getFechamento().setSaldoAnterior(viagem.getFechamento().getSaldoAnterior());
					v.getFechamento().setSaldoFinal(viagem.getFechamento().getSaldoFinal());

					list.add(v);
			});
		Integer total = this.viagemService.total(request.getFiltro());
		// @formatter:on

		return this.json().entity(new ConsultaResponse(list, total)).status(Status.OK).build();
	}

	@Path("/preview/lancamento")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response previewLancamentos(ConsultaRequest request) {

		List<LancamentoDTO> list = new ArrayList<>();

		// @formatter:off
		this.adiantamentoService
			.consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder())
			.forEach(lancamento -> {
				LancamentoDTO e = new LancamentoDTO();

				e.setId(lancamento.getId());

				e.setFuncionario(new FuncionarioDTO());
				e.getFuncionario().setNome(lancamento.getFuncionario().getNome());

				e.setCategoria(new CategoriaDTO());
				e.getCategoria().setNome(lancamento.getCategoria().getNome());
				e.setDataInicial(lancamento.getDataInicial());
				e.setDataFinal(lancamento.getDataFinal());
				e.setTipoLancamento(lancamento.getTipoLancamento());
				e.setValorLancamento(lancamento.getValorLancamento());

				list.add(e);
			});
		Integer total = this.adiantamentoService.total(request.getFiltro());
		// @formatter:on
		return this.json().entity(new ConsultaResponse(list, total)).status(Status.OK).build();
	}

	@Path("/download/viagem")
	@POST
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response downloadViagem(ConsultaRequest request) {
		if (request != null) {
			try {
				byte[] arquivo = this.exportacaoDadosService.processarExportacaoViagem(request);

				return this.json().header("Content-Disposition", "attachment; filename=viagens.xlsx").entity(arquivo).status(Status.OK).build();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return this.json().status(Status.OK).build();
	}

	@Path("/download/lancamento")
	@POST
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response downloadLancamento(ConsultaRequest request) {
		if (request != null) {
			try {
				byte[] arquivo = this.exportacaoDadosService.processarExportacaoLancamento(request);

				return this.json().header("Content-Disposition", "attachment; filename=lancamentos.xlsx").entity(arquivo).status(Status.OK).build();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return this.json().status(Status.OK).build();
	}
}