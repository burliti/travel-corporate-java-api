package br.com.travelcorporate.api.reports.resources.despesas;

import java.io.IOException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.travelcorporate.core.services.AbstractResource;
import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.services.filters.annotations.AuthenticationRequired;
import br.com.travelcorporate.model.cadastros.dto.CentroCustoDTO;
import br.com.travelcorporate.model.cadastros.dto.ClienteDTO;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.ProjetoDTO;
import br.com.travelcorporate.model.cadastros.dto.SetorDTO;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.cadastros.service.ClienteService;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.cadastros.service.ProjetoService;
import br.com.travelcorporate.model.cadastros.service.SetorService;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.configuracao.dto.EmpresaDTO;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.relatorios.despesas.service.RelatorioDespesasService;
import br.com.travelcorporate.model.relatorios.exportacaodados.dto.ExportacaoDadosFiltrosDTO;

@Path("/despesas/v1")
@Stateless
@AuthenticationRequired
public class RelatorioDespesasResource extends AbstractResource {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private SetorService setorService;

	@Inject
	private CategoriaService categoriaService;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private ProjetoService projetoService;

	@Inject
	private ClienteService clienteService;

	@Inject
	private CentroCustoService centroCustoService;

	@Inject
	private RelatorioDespesasService relatorioDespesasService;

	@GET
	@Path("/dados")
	@AuthenticationRequired
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response dadosParaFiltros() {
		ExportacaoDadosFiltrosDTO filtros = new ExportacaoDadosFiltrosDTO();

		this.setorService.getAtivos().forEach(s -> {
			SetorDTO dto = new SetorDTO();
			dto.setId(s.getId());
			dto.setNome(s.getNome());
			filtros.getSetores().add(dto);
		});

		this.funcionarioService.getAtivos().forEach(f -> {
			FuncionarioDTO dto = new FuncionarioDTO();
			dto.setId(f.getId());
			dto.setNome(f.getNome());
			filtros.getFuncionarios().add(dto);
		});

		this.categoriaService.getAtivos().forEach(c -> {
			CategoriaDTO dto = new CategoriaDTO();
			dto.setId(c.getId());
			dto.setNome(c.getNome());
			filtros.getCategorias().add(dto);
		});

		this.empresaService.getAtivos().forEach(e -> {
			EmpresaDTO dto = new EmpresaDTO();
			dto.setId(e.getId());
			dto.setNome(e.getNome());
			dto.setFantasia(e.getFantasia());
			filtros.getEmpresas().add(dto);
		});

		this.projetoService.getAtivos().forEach(p -> {
			ProjetoDTO dto = new ProjetoDTO();
			dto.setId(p.getId());
			dto.setNome(p.getNome());
			filtros.getProjetos().add(dto);
		});

		this.clienteService.getAtivos().forEach(c -> {
			ClienteDTO dto = new ClienteDTO();
			dto.setId(c.getId());
			dto.setNome(c.getNome());
			filtros.getClientes().add(dto);
		});

		this.centroCustoService.getAtivos().forEach(c -> {
			CentroCustoDTO dto = new CentroCustoDTO();
			dto.setId(c.getId());
			dto.setNome(c.getDescricaoCompleta());
			filtros.getCentrosCusto().add(dto);
		});

		return this.json().entity(filtros).status(Status.OK).build();
	}

	@Path("/pdf")
	@POST
	@Produces("application/pdf")
	@Consumes(MediaType.APPLICATION_JSON)
	@AuthenticationRequired
	public Response downloadViagem(ConsultaRequest request) {
		if (request != null) {
			try {
				byte[] arquivo = this.relatorioDespesasService.processarRelatorio(request);

				return this.json().header("Content-Disposition", "attachment; filename=despesas.pdf").entity(arquivo).status(Status.OK).build();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return this.json().status(Status.OK).build();
	}
}
