package br.com.travelcorporate.model.lancamentos.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.utils.DaoUtils;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class ViagemDao extends AbstractDao<Integer, Viagem> {

	public List<Viagem> getViagensByFuncionario(Funcionario f, Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		if (f == null) {
			return null;
		}

		if (filtros == null) {
			filtros = new HashMap<>();
		}

		String hql = "SELECT e FROM Viagem e \n";
		hql += " INNER JOIN e.funcionarios f \n";
		hql += " WHERE 1=1 AND f.id in :p_funcionario \n";

		final Map<String, Object> parametros = new HashMap<>();

		final String whereStatement = DaoUtils.buildWhereStatement(filtros, this.getVOClass(), parametros);

		hql += whereStatement;

		if (order != null && order.size() > 0) {
			hql += " ORDER BY ";
			boolean first = true;
			final Iterator<String> iterator = order.keySet().iterator();

			while (iterator.hasNext()) {
				final String campo = iterator.next();

				if (!first) {
					hql += ", ";
				}

				hql += campo + " " + order.get(campo);
				first = false;
			}
		}

		this.log.debug("HQL: " + hql);

		final TypedQuery<Viagem> query = this.getManager().createQuery(hql, Viagem.class);

		query.setParameter("p_funcionario", f.getId());

		// Seta os parametros da query
		final Iterator<String> paramsIterator = parametros.keySet().iterator();

		while (paramsIterator.hasNext()) {
			final String paramName = paramsIterator.next();
			this.log.debug("Parametro: " + paramName + " valor: " + parametros.get(paramName));
			query.setParameter(paramName, parametros.get(paramName));
		}

		// Paginacao
		if (pageSize != null && pageNumber != null) {
			query.setMaxResults(pageSize);

			final int firstResult = (pageNumber - 1) * pageSize;

			query.setFirstResult(firstResult);
		}

		return query.getResultList();
	}

	public Integer totalViagensPorFuncionario(Funcionario f, Map<String, String> filtros) {
		this.getManager().clear();

		if (filtros == null) {
			filtros = new HashMap<>();
		}

		String hql = "SELECT COUNT(*) FROM " + this.getVOClass().getSimpleName() + " e \n";
		hql += " INNER JOIN e.funcionarios f \n";
		hql += " WHERE 1 = 1 AND f.id in :p_funcionario \n";

		// Build SQL Filtro
		final Map<String, Object> parametros = new HashMap<>();

		final String whereStatement = DaoUtils.buildWhereStatement(filtros, this.getVOClass(), parametros);

		hql += whereStatement;

		this.log.debug("HQL: " + hql);

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_funcionario", f.getId());

		// Seta os parametros da query
		final Iterator<String> paramsIterator = parametros.keySet().iterator();

		while (paramsIterator.hasNext()) {
			final String paramName = paramsIterator.next();
			query.setParameter(paramName, parametros.get(paramName));
		}

		final int count = query.getSingleResult().intValue();

		return count;
	}

	public Integer getNextSequencialEmpresa(IEmpresa empresa) {
		Integer retorno = 0;

		String hql = "SELECT MAX(e.sequencial) FROM " + this.getVOClass().getSimpleName() + " e \n";
		hql += " WHERE 1 = 1 AND e.empresa.id = :p_empresa";

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_empresa", empresa.getId());

		Number singleResult = query.getSingleResult();
		if (singleResult != null) {
			retorno = singleResult.intValue();
		} else {
			retorno = 0;
		}

		return retorno + 1;
	}

	public Viagem getUltimaViagemAberta(IFuncionario funcionario) {
		if (funcionario == null) {
			return null;
		}

		String hql = "SELECT e FROM Viagem e \n";
		hql += " WHERE 1=1 \n";
		hql += "   AND e.funcionario.id = :p_funcionario \n";
		hql += "   AND e.status in :p_status \n";
		hql += " ORDER BY e.sequencial DESC";

		TypedQuery<Viagem> query = this.getManager().createQuery(hql, Viagem.class);

		query.setParameter("p_funcionario", funcionario.getId());

		List<StatusViagem> list = new ArrayList<>();
		list.add(StatusViagem.ANDAMENTO);
		list.add(StatusViagem.PLANEJAMENTO);
		list.add(StatusViagem.APROVADA_VIAGEM);
		list.add(StatusViagem.SOLICITADA);

		query.setParameter("p_status", list);

		query.setFirstResult(0);
		query.setMaxResults(1);

		List<Viagem> result = query.getResultList();

		if (result.size() > 0) {
			return result.get(0);
		}

		return null;
	}

	public Viagem getUltimaViagemAprovada(IFuncionario funcionario) {
		if (funcionario == null) {
			return null;
		}

		String hql = "SELECT e FROM Viagem e \n";
		hql += " WHERE 1=1 \n";
		hql += "   AND e.funcionario.id = :p_funcionario \n";
		hql += "   AND e.status in :p_status \n";
		hql += " ORDER BY e.sequencial DESC";

		TypedQuery<Viagem> query = this.getManager().createQuery(hql, Viagem.class);

		query.setParameter("p_funcionario", funcionario.getId());

		List<StatusViagem> list = new ArrayList<>();
		list.add(StatusViagem.APROVADA);

		query.setParameter("p_status", list);

		query.setFirstResult(0);
		query.setMaxResults(1);

		List<Viagem> result = query.getResultList();

		if (result.size() > 0) {
			return result.get(0);
		}

		return null;
	}

}