package br.com.travelcorporate.model.lancamentos.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Cliente;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.entities.Projeto;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.geral.enums.converters.StatusViagemConverter;

@Entity
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class Viagem extends AbstractEntity<Integer> implements Serializable {
	private static final String FORMATO_DATA_PADRAO = "dd/MM/yyyy";

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_viagem", sequenceName = "seq_viagem", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_viagem")
	@Column(name = "id_viagem")
	private Integer id;

	@NotNull(message = "Sequencial da viagem é obrigatório")
	private Integer sequencial;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_ida")
	private Calendar dataIda;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_volta")
	private Calendar dataVolta;

	@NotEmpty(message = "Informe uma descrição para a viagem")
	private String descricao;

	private String objetivo;

	private String observacao;

	@NotNull(message = "Informe o status da viagem")
	@Convert(converter = StatusViagemConverter.class)
	private StatusViagem status;

	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@ManyToOne
	@JoinColumn(name = "id_projeto")
	private Projeto projeto;

	@ManyToOne
	@JoinColumn(name = "id_funcionario")
	@NotNull(message = "Funcionário é obrigatório!")
	private Funcionario funcionario;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "viagem", orphanRemoval = true)
	@Valid
	private List<Lancamento> lancamentos;

	@Valid
	@OneToOne(mappedBy = "viagem", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Fechamento fechamento;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Calendar getDataIda() {
		return this.dataIda;
	}

	@JsonGetter("dataIda")
	public String getDataIdaFormatada() {
		if (this.getDataIda() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_DATA_PADRAO);
			return sdf.format(this.getDataIda().getTime());
		}
		return null;
	}

	public void setDataIda(Calendar dataIda) {
		this.dataIda = dataIda;
	}

	@JsonSetter
	public void setDataIda(String dataIda) {
		if (dataIda != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_DATA_PADRAO);

			Date parse = null;
			try {
				parse = sdf.parse(dataIda);
			} catch (final ParseException e) {
				e.printStackTrace();
			}

			if (parse != null) {
				this.dataIda = Calendar.getInstance();
				this.dataIda.setTimeInMillis(parse.getTime());
			} else {
				this.dataIda = null;
			}
		} else {
			this.dataIda = null;
		}
	}

	public Calendar getDataVolta() {
		return this.dataVolta;
	}

	@JsonGetter("dataVolta")
	public String getDataVoltaFormatada() {
		if (this.getDataVolta() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_DATA_PADRAO);
			return sdf.format(this.getDataVolta().getTime());
		}
		return null;
	}

	public void setDataVolta(Calendar dataVolta) {
		this.dataVolta = dataVolta;
	}

	@JsonSetter
	public void setDataVolta(String dataVolta) {
		if (dataVolta != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_DATA_PADRAO);

			Date parse = null;
			try {
				parse = sdf.parse(dataVolta);
			} catch (final ParseException e) {
				e.printStackTrace();
			}

			if (parse != null) {
				this.dataVolta = Calendar.getInstance();
				this.dataVolta.setTimeInMillis(parse.getTime());
			} else {
				this.dataVolta = null;
			}
		} else {
			this.dataVolta = null;
		}
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObjetivo() {
		return this.objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public StatusViagem getStatus() {
		return this.status;
	}

	public void setStatus(StatusViagem status) {
		this.status = status;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Projeto getProjeto() {
		return this.projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	public List<Lancamento> getLancamentos() {
		if (this.lancamentos == null) {
			this.lancamentos = new ArrayList<>();
		}
		return this.lancamentos;
	}

	public void setLancamentos(List<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}

	public Fechamento getFechamento() {
		return this.fechamento;
	}

	public void setFechamento(Fechamento fechamento) {
		this.fechamento = fechamento;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Integer getSequencial() {
		return this.sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	@JsonIgnore
	public boolean isAberto() {
		return this.status == StatusViagem.ANDAMENTO || this.status == StatusViagem.APROVADA_VIAGEM || this.status == StatusViagem.PLANEJAMENTO || this.status == StatusViagem.SOLICITADA;
	}

}