package br.com.travelcorporate.model.mensageria.entities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.StatusMensagem;
import br.com.travelcorporate.model.geral.enums.converters.FlagConverter;
import br.com.travelcorporate.model.geral.enums.converters.StatusMensagemConverter;

@Entity
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class Mensagem extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_mensagem", sequenceName = "seq_mensagem", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mensagem")
	@Column(name = "id_mensagem")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "id_assunto")
	private Assunto assunto;

	@Column(name = "assunto_descricao")
	@NotEmpty(message = "Informe o assunto")
	private String assuntoDescricao;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_autor")
	@NotNull(message = "Informe o autor")
	private Funcionario autor;

	@NotNull(message = "Mensagem sistema é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_mensagem_sistema")
	private Flag mensagemSistema;

	@NotNull(message = "Permite responder é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_permite_responder")
	private Flag permiteResponder;

	@NotNull(message = "Permite encaminhar é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_permite_encaminhar")
	private Flag permiteEncaminhar;

	@NotNull(message = "Exige confirmação é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_exige_confirmacao")
	private Flag exigeConfirmacao;

	@NotNull(message = "Bloqueia sem responder é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_bloqueia_sem_responder")
	private Flag bloqueiaSemResponder;

	@Column(name = "texto_botao_confirmacao")
	private String textoBotaoConfirmacao;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mensagem_origem_resposta")
	private Mensagem mensagemOrigemResposta;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mensagem_origem_encaminhada")
	private Mensagem mensagemOrigemEncaminhada;

	@NotNull(message = "Status mensagem é obrigatório")
	@Convert(converter = StatusMensagemConverter.class)
	private StatusMensagem status;

	@NotEmpty(message = "Informe o texto da mensagem")
	@Column(name = "mensagem")
	private String textoMensagem;

	@Column(name = "data_hora_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraCriacao;

	@Column(name = "data_hora_envio")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraEnvio;

	@Column(name = "data_hora_exclusao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dataHoraExclusao;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@OneToMany(mappedBy = "mensagem", cascade = CascadeType.ALL, orphanRemoval = true)
	@Valid
	private List<MensagemDestinatario> destinatarios;

	@OneToMany(mappedBy = "mensagem", cascade = CascadeType.ALL, orphanRemoval = true)
	@Valid
	private List<MensagemFormaNotificacao> formasNotificacao;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Assunto getAssunto() {
		return this.assunto;
	}

	public void setAssunto(Assunto assunto) {
		this.assunto = assunto;
	}

	public String getAssuntoDescricao() {
		return this.assuntoDescricao;
	}

	public void setAssuntoDescricao(String assuntoDescricao) {
		this.assuntoDescricao = assuntoDescricao;
	}

	public Funcionario getAutor() {
		return this.autor;
	}

	public void setAutor(Funcionario autor) {
		this.autor = autor;
	}

	public Flag getMensagemSistema() {
		return this.mensagemSistema;
	}

	public void setMensagemSistema(Flag mensagemSistema) {
		this.mensagemSistema = mensagemSistema;
	}

	public Flag getPermiteResponder() {
		return this.permiteResponder;
	}

	public void setPermiteResponder(Flag permiteResponder) {
		this.permiteResponder = permiteResponder;
	}

	public Flag getPermiteEncaminhar() {
		return this.permiteEncaminhar;
	}

	public void setPermiteEncaminhar(Flag permiteEncaminhar) {
		this.permiteEncaminhar = permiteEncaminhar;
	}

	public Flag getExigeConfirmacao() {
		return this.exigeConfirmacao;
	}

	public void setExigeConfirmacao(Flag exigeConfirmacao) {
		this.exigeConfirmacao = exigeConfirmacao;
	}

	public Flag getBloqueiaSemResponder() {
		return this.bloqueiaSemResponder;
	}

	public void setBloqueiaSemResponder(Flag bloqueiaSemResponder) {
		this.bloqueiaSemResponder = bloqueiaSemResponder;
	}

	public String getTextoBotaoConfirmacao() {
		return this.textoBotaoConfirmacao;
	}

	public void setTextoBotaoConfirmacao(String textoBotaoConfirmacao) {
		this.textoBotaoConfirmacao = textoBotaoConfirmacao;
	}

	public Mensagem getMensagemOrigemResposta() {
		return this.mensagemOrigemResposta;
	}

	public void setMensagemOrigemResposta(Mensagem mensagemOrigemResposta) {
		this.mensagemOrigemResposta = mensagemOrigemResposta;
	}

	public Mensagem getMensagemOrigemEncaminhada() {
		return this.mensagemOrigemEncaminhada;
	}

	public void setMensagemOrigemEncaminhada(Mensagem mensagemOrigemEncaminhada) {
		this.mensagemOrigemEncaminhada = mensagemOrigemEncaminhada;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public List<MensagemDestinatario> getDestinatarios() {
		if (this.destinatarios == null) {
			this.destinatarios = new ArrayList<>();
		}
		return this.destinatarios;
	}

	public void setDestinatarios(List<MensagemDestinatario> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public List<MensagemFormaNotificacao> getFormasNotificacao() {
		if (this.formasNotificacao == null) {
			this.formasNotificacao = new ArrayList<>();
		}
		return this.formasNotificacao;
	}

	public void setFormasNotificacao(List<MensagemFormaNotificacao> formasNotificacao) {
		this.formasNotificacao = formasNotificacao;
	}

	public String getTextoMensagem() {
		return this.textoMensagem;
	}

	public void setTextoMensagem(String textoMensagem) {
		this.textoMensagem = textoMensagem;
	}

	public Calendar getDataHoraCriacao() {
		return this.dataHoraCriacao;
	}

	public void setDataHoraCriacao(Calendar dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	@JsonGetter("dataHoraCriacao")
	public String getDataHoraCriacaoFormatada() {
		return FormatUtils.formatDateTime(this.getDataHoraCriacao());
	}

	@JsonSetter("dataHoraCriacao")
	public void setDataHoraCriacao(String dataHoraCriacao) {
		this.setDataHoraCriacao(FormatUtils.parseDateTime(dataHoraCriacao));
	}

	public Calendar getDataHoraEnvio() {
		return this.dataHoraEnvio;
	}

	public void setDataHoraEnvio(Calendar dataHoraEnvio) {
		this.dataHoraEnvio = dataHoraEnvio;
	}

	public Calendar getDataHoraExclusao() {
		return this.dataHoraExclusao;
	}

	public void setDataHoraExclusao(Calendar dataHoraExclusao) {
		this.dataHoraExclusao = dataHoraExclusao;
	}

	public StatusMensagem getStatus() {
		return this.status;
	}

	public void setStatus(StatusMensagem status) {
		this.status = status;
	}
}
