package br.com.travelcorporate.model.lancamentos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Convert;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.StringUtils;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.entities.Fornecedor;
import br.com.travelcorporate.model.geral.enums.TipoPessoa;
import br.com.travelcorporate.model.geral.enums.converters.TipoPessoaConverter;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReciboDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Calendar dataInserido;

	private Calendar dataReferencia;

	private String fotoAnexoRecibo;

	private String numeroDocumento;

	private FuncionarioDTO funcionario;

	private BigDecimal valor;

	private String chaveNfe;

	private Fornecedor fornecedor;

	private String urlNfe;

	private String nomeFornecedor;

	private String cpfCnpjFornecedor;

	@Convert(converter = TipoPessoaConverter.class)
	private TipoPessoa tipoFornecedor;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonGetter("dataInserido")
	public String getDataInseridoFormatada() {
		if (this.getDataInserido() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			return sdf.format(this.getDataInserido().getTime());
		}
		return null;
	}

	public Calendar getDataInserido() {
		return this.dataInserido;
	}

	public void setDataInserido(Calendar dataInserido) {
		this.dataInserido = dataInserido;
	}

	@JsonSetter("dataInserido")
	public void setDataInserido(String dataInserido) {
		//
	}

	public Calendar getDataReferencia() {
		return this.dataReferencia;
	}

	public void setDataReferencia(Calendar dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	@JsonGetter("dataReferencia")
	public String getDataReferenciaFormatada() {
		if (this.getDataReferencia() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATA_PADRAO);

			return sdf.format(this.getDataReferencia().getTime());
		}
		return null;
	}

	@JsonSetter("dataReferencia")
	public void setDataReferencia(String dataReferencia) {
		if (dataReferencia != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATA_PADRAO);

			Date parse = null;
			try {
				parse = sdf.parse(dataReferencia);
			} catch (final ParseException e) {
				e.printStackTrace();
			}

			if (parse != null) {
				this.dataReferencia = Calendar.getInstance();
				this.dataReferencia.setTimeInMillis(parse.getTime());
			} else {
				this.dataReferencia = null;
			}
		} else {
			this.dataReferencia = null;
		}
	}

	public String getFotoAnexoRecibo() {
		return this.fotoAnexoRecibo;
	}

	public void setFotoAnexoRecibo(String fotoAnexoRecibo) {
		this.fotoAnexoRecibo = fotoAnexoRecibo;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public FuncionarioDTO getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(FuncionarioDTO funcionario) {
		this.funcionario = funcionario;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getChaveNfe() {
		return this.chaveNfe;
	}

	public void setChaveNfe(String chaveNfe) {
		this.chaveNfe = chaveNfe;
	}

	public Fornecedor getFornecedor() {
		return this.fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getUrlNfe() {
		return this.urlNfe;
	}

	public void setUrlNfe(String urlNfe) {
		this.urlNfe = urlNfe;
	}

	public String getNomeFornecedor() {
		return this.nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public String getCpfCnpjFornecedor() {
		return this.cpfCnpjFornecedor;
	}

	public void setCpfCnpjFornecedor(String cpfCnpjFornecedor) {
		this.cpfCnpjFornecedor = cpfCnpjFornecedor;
	}

	public TipoPessoa getTipoFornecedor() {
		return this.tipoFornecedor;
	}

	public void setTipoFornecedor(TipoPessoa tipoFornecedor) {
		this.tipoFornecedor = tipoFornecedor;
	}
}
