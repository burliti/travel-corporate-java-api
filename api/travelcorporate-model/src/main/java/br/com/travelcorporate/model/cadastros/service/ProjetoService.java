package br.com.travelcorporate.model.cadastros.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dao.ProjetoDao;
import br.com.travelcorporate.model.cadastros.entities.Projeto;
import br.com.travelcorporate.model.geral.enums.Status;

@Stateless
public class ProjetoService extends AbstractService<Integer, Projeto, ProjetoDao> {

	@Inject
	private CentroCustoService centroCustoService;

	public List<Projeto> getAtivos() {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());
		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}

	@Override
	public void doAntesSalvar(Projeto vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getCentroCusto() != null && vo.getCentroCusto().getId() != null) {
			vo.setCentroCusto(this.centroCustoService.buscar(vo.getCentroCusto()));
		} else {
			vo.setCentroCusto(null);
		}
	}
}
