package br.com.travelcorporate.model.tabelas.service;

import static br.com.travelcorporate.model.tabelas.service.AcaoService.ACAO_EXECUTAR;
import static br.com.travelcorporate.model.tabelas.service.AcaoService.ACOES_CADASTRO_PADRAO;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.configuracao.entities.Perfil;
import br.com.travelcorporate.model.geral.Node;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.tabelas.dao.RecursoDao;
import br.com.travelcorporate.model.tabelas.entities.AcaoRecurso;
import br.com.travelcorporate.model.tabelas.entities.Recurso;

@Stateless
public class RecursoService extends AbstractService<Integer, Recurso, RecursoDao> {

	@Inject
	private AcaoService acaoService;

	public void createRecursos() throws BusinessException {

		// Antigos
		this.inativarRecurso("lancamentos");
		this.inativarRecurso("lancamento");
		this.inativarRecurso("adiantamento");
		this.inativarRecurso("politicaviagem");

		this.renomearRecurso("relatorio-viagem", "politicaviagem");
		this.renomearRecurso("aprovacaocontas", "aprovacao-contas");

		this.inativarRecurso("prestacao-contas");
		this.inativarRecurso("fechamento");

		// Principais
		final Recurso configuracoes = this.createRecursoIfNotExists("configuracoes", "Configurações", new String[] {}, null, 1, "fa fa-cogs fa-fw");
		final Recurso cadastros = this.createRecursoIfNotExists("cadastros", "Cadastros", new String[] {}, null, 2, "fa fa-bar-chart-o fa-fw");
		final Recurso colaborador = this.createRecursoIfNotExists("colaborador", "Colaborador", new String[] {}, null, 3, "fa fa-briefcase fa-fw");
		final Recurso financeiro = this.createRecursoIfNotExists("financeiro", "Financeiro", new String[] {}, null, 4, "fa fa-money fa-fw");
		final Recurso relatorios = this.createRecursoIfNotExists("relatorios", "Relatórios", new String[] {}, null, 5, "fa fa-pie-chart");

		// Configurações
		this.createRecursoIfNotExists("empresa", "Empresas (Matriz / Filiais)", ACOES_CADASTRO_PADRAO, configuracoes, 1);
		this.createRecursoIfNotExists("categoria", "Categoria de Lançamentos", ACOES_CADASTRO_PADRAO, configuracoes, 2);
		this.createRecursoIfNotExists("perfil", "Perfil de acesso", ACOES_CADASTRO_PADRAO, configuracoes, 3);
		this.createRecursoIfNotExists("setor", "Setores", ACOES_CADASTRO_PADRAO, configuracoes, 4);
		this.createRecursoIfNotExists("configuracao-aprovacao-viagem", "Config. de Aprovação de Viagem", ACOES_CADASTRO_PADRAO, configuracoes, 5);

		// Cadastros
		this.createRecursoIfNotExists("centro-custo", "Centro de Custo", ACOES_CADASTRO_PADRAO, cadastros, 1);
		this.createRecursoIfNotExists("cliente", "Clientes", ACOES_CADASTRO_PADRAO, cadastros, 2);
		this.createRecursoIfNotExists("funcionario", "Funcionários", ACOES_CADASTRO_PADRAO, cadastros, 3);
		this.createRecursoIfNotExists("projeto", "Projetos", ACOES_CADASTRO_PADRAO, cadastros, 4);
		this.createRecursoIfNotExists("politica", "Políticas de viagem", ACOES_CADASTRO_PADRAO, cadastros, 5);

		// Colaborador
		this.createRecursoIfNotExists("minhas-viagens", "Minhas Viagens", ACOES_CADASTRO_PADRAO, colaborador, 1);

		// Financeiro
		this.createRecursoIfNotExists("adiantamento", "Adiantamento / Lançamentos", ACOES_CADASTRO_PADRAO, financeiro, 1);
		this.createRecursoIfNotExists("aprovacao-contas", "Aprovação de contas", ACOES_CADASTRO_PADRAO, financeiro, 2);
		this.createRecursoIfNotExists("multa", "Multas", ACOES_CADASTRO_PADRAO, financeiro, 3);

		// Relatórios
		this.createRecursoIfNotExists("exportacao-dados", "Exportação de dados", ACAO_EXECUTAR, relatorios, 1);
		this.createRecursoIfNotExists("despesas", "Relatório de despesas", ACAO_EXECUTAR, relatorios, 2);
	}

	public List<Recurso> getAllRecursos() {
		return this.getDao().getAllRecursos();
	}

	public List<Node> getRecursosToNode() {
		return this.getRecursosToNode(null, false);
	}

	public List<Node> getRecursosToNode(Perfil perfil, boolean somenteAutorizados) {

		final List<Recurso> recursos = this.getAllRecursos();
		final List<Node> list = new ArrayList<>();

		// Primeiro nivel
		for (final Recurso recurso : recursos) {
			if (recurso.isAtivo()) {
				final Node n1 = new Node();
				n1.setId("recurso-" + recurso.getId() + "-" + recurso.getNome());
				n1.setNome(recurso.getDescricao());
				n1.setIcone(recurso.getIcone());
				n1.setUrl(recurso.getNome());

				if (perfil != null) {
					n1.setSelecionado(perfil.getRecursos().contains(recurso));
				} else {
					n1.setSelecionado(false);
				}

				if (n1.isSelecionado() || !somenteAutorizados) {
					list.add(n1);

					// Ordena os filhos
					recurso.getFilhos().sort((o1, o2) -> o1.getOrdem().compareTo(o2.getOrdem()));

					// Segundo nível
					for (final Recurso filho : recurso.getFilhos()) {
						if (filho.isAtivo()) {
							final Node n2 = new Node();
							n2.setId("recurso-" + filho.getId() + "-" + filho.getNome());
							n2.setNome(filho.getDescricao());
							n2.setIcone(filho.getIcone());
							n2.setUrl(filho.getNome());

							if (perfil != null) {
								n2.setSelecionado(perfil.getRecursos().contains(filho));
							} else {
								n2.setSelecionado(false);
							}

							if (n2.isSelecionado() || !somenteAutorizados) {
								n1.getFilhos().add(n2);

								for (final AcaoRecurso acaoRecurso : filho.getAcoes()) {
									final Node n3 = new Node();
									n3.setId("acaorecurso-" + acaoRecurso.getId() + "-" + acaoRecurso.getAcao().getNome());
									n3.setNome(acaoRecurso.getAcao().getDescricao());

									if (perfil != null) {
										n3.setSelecionado(perfil.getAcoes().contains(acaoRecurso));
									} else {
										n3.setSelecionado(false);
									}

									if (n3.isSelecionado() || !somenteAutorizados) {
										n2.getFilhos().add(n3);
									}
								}
							}
						}
					}
				}
			}
		}

		return list;
	}

	public Recurso getByNome(String nome) {
		return this.getDao().getByNome(nome);
	}

	public Recurso createRecursoIfNotExists(String nome, String descricao, String[] acoes, Recurso pai, Integer ordem) throws BusinessException {
		return this.createRecursoIfNotExists(nome, descricao, acoes, pai, ordem, "");
	}

	public Recurso createRecursoIfNotExists(String nome, String descricao, String[] acoes, Recurso pai, Integer ordem, String icone) throws BusinessException {
		Recurso recurso = this.getByNome(nome);

		if (recurso == null) {
			recurso = new Recurso();
		}

		recurso.setNome(nome);
		recurso.setDescricao(descricao);
		recurso.setStatus(Status.ATIVO);
		recurso.setRecursoPai(pai);
		recurso.setOrdem(ordem);
		recurso.setIcone(icone);

		// Verifica se tem todas as acoes
		for (final String nomeAcao : acoes) {
			boolean achou = false;
			for (final AcaoRecurso acaoRecurso : recurso.getAcoes()) {
				if (acaoRecurso.getAcao().getNome().equalsIgnoreCase(nomeAcao)) {
					achou = true;
					break;
				}
			}

			if (!achou) {
				recurso.getAcoes().add(new AcaoRecurso(this.acaoService.getByNome(nomeAcao), recurso));
			}
		}

		if (recurso.getId() == null) {
			this.inserir(recurso);
		} else {
			this.atualizar(recurso);
		}

		return recurso;
	}

	public void inativarRecurso(String nome) {
		Recurso recurso = this.getByNome(nome);

		if (recurso != null) {
			recurso.setStatus(Status.INATIVO);
		}
	}

	public void renomearRecurso(String nome, String novoNome) {
		Recurso recurso = this.getByNome(nome);

		if (recurso != null) {
			recurso.setStatus(Status.ATIVO);
			recurso.setNome(novoNome);
		}
	}

}
