package br.com.travelcorporate.model.geral.exceptions.viagem;

import br.com.travelcorporate.core.exceptions.BusinessException;

public class ViagemException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public ViagemException(String message) {
		super(message);
	}
}