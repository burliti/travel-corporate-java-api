package br.com.travelcorporate.model.geral.exceptions;

import br.com.travelcorporate.core.exceptions.BusinessException;

public class RegistroInvalidoException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public RegistroInvalidoException(String message) {
		super(message);
	}
}
