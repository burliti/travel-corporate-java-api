package br.com.travelcorporate.model.cadastros.entities;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.enums.TipoSessao;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.core.vo.ISessaoUsuario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.converters.TipoSessaoConverter;

@Entity
@Table(name = "sessao_usuario")
public class SessaoUsuario extends AbstractEntity<Integer> implements ISessaoUsuario {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_sessao_usuario", sequenceName = "seq_sessao_usuario", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sessao_usuario")
	@Column(name = "id_sessao")
	@JsonIgnore
	private Integer id;

	@Column(name = "chave_sessao")
	private String chaveSessao;

	@Column(name = "data_criacao")
	private Calendar dataCriacao;

	@Column(name = "endereco_ip")
	private String enderecoIp;

	private String hostName;

	@Convert(converter = TipoSessaoConverter.class)
	@Column(name = "tipo_sessao")
	private TipoSessao tipoSessao;

	@Column(name = "ultima_interacao")
	private Calendar ultimaInteracao;

	// @JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Funcionario.class)
	@JoinColumn(name = "id_funcionario")
	private IFuncionario funcionario;

	public SessaoUsuario() {
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getChaveSessao() {
		return this.chaveSessao;
	}

	public void setChaveSessao(String chaveSessao) {
		this.chaveSessao = chaveSessao;
	}

	@Override
	public Calendar getDataCriacao() {
		return this.dataCriacao;
	}

	@Override
	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public String getEnderecoIp() {
		return this.enderecoIp;
	}

	@Override
	public void setEnderecoIp(String enderecoIp) {
		this.enderecoIp = enderecoIp;
	}

	@Override
	public String getHostName() {
		return this.hostName;
	}

	@Override
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	@Override
	public TipoSessao getTipoSessao() {
		return this.tipoSessao;
	}

	public void setTipoSessao(TipoSessao tipoSessao) {
		this.tipoSessao = tipoSessao;
	}

	@Override
	public Calendar getUltimaInteracao() {
		return this.ultimaInteracao;
	}

	@Override
	public void setUltimaInteracao(Calendar ultimaInteracao) {
		this.ultimaInteracao = ultimaInteracao;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public IFuncionario getFuncionario() {
		return this.funcionario;
	}

	@Override
	public void setFuncionario(IFuncionario funcionario) {
		this.funcionario = funcionario;
	}

}