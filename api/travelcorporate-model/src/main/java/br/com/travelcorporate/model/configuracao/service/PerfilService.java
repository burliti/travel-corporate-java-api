package br.com.travelcorporate.model.configuracao.service;

import java.util.List;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.configuracao.dao.PerfilDao;
import br.com.travelcorporate.model.configuracao.entities.Perfil;
import br.com.travelcorporate.model.tabelas.entities.Recurso;

@Stateless
public class PerfilService extends AbstractService<Integer, Perfil, PerfilDao> {

	public List<Perfil> getAtivos() {
		return this.getDao().getAtivos(this.getSessao().getEmpresa());
	}

	public void removerPermissoes(Recurso recurso) {
		this.getDao().removerAcoesPermissao(recurso);
		this.getDao().removerRecursosPermissao(recurso);
	}
}