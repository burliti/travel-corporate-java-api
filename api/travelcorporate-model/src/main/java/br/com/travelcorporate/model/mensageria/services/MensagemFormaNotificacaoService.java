package br.com.travelcorporate.model.mensageria.services;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.mensageria.dao.MensagemFormaNotificacaoDao;
import br.com.travelcorporate.model.mensageria.dto.FormaNotificacaoDTO;
import br.com.travelcorporate.model.mensageria.dto.MensagemFormaNotificacaoDTO;
import br.com.travelcorporate.model.mensageria.entities.MensagemFormaNotificacao;

@Stateless
public class MensagemFormaNotificacaoService extends AbstractService<Integer, MensagemFormaNotificacao, MensagemFormaNotificacaoDao> {

	public MensagemFormaNotificacaoDTO convertDTO(MensagemFormaNotificacao mensagemFormaNotificacao) {
		if (mensagemFormaNotificacao == null) {
			return null;
		}

		MensagemFormaNotificacaoDTO dto = new MensagemFormaNotificacaoDTO();

		dto.setId(mensagemFormaNotificacao.getId());
		dto.setFormaNotificacao(new FormaNotificacaoDTO());
		dto.getFormaNotificacao().setId(mensagemFormaNotificacao.getFormaNotificacao().getId());
		dto.getFormaNotificacao().setNome(mensagemFormaNotificacao.getFormaNotificacao().getNome());

		return dto;
	}
}
