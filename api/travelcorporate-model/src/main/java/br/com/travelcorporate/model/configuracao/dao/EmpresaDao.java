package br.com.travelcorporate.model.configuracao.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.services.annotations.UserSession;
import br.com.travelcorporate.core.utils.CDIServiceLocator;
import br.com.travelcorporate.core.vo.ISessaoUsuario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;

@Stateless
public class EmpresaDao extends AbstractDao<Integer, Empresa> {

	// @Inject
	// @UserSession
	// private SessaoUsuario sessao;

	@Override
	public Empresa buscar(Integer id) {
		// Não dá pra injetar, então usa o CDI Service Locator
		final ISessaoUsuario sessao = CDIServiceLocator.getBean(ISessaoUsuario.class, () -> UserSession.class);

		Empresa buscar = super.buscar(id);
		if (buscar != null) {
			if (buscar.getId().equals(sessao.getEmpresa().getId())) {
				return buscar;
			} else if (buscar.getEmpresaPrincipal() != null && buscar.getEmpresaPrincipal().getId().equals(sessao.getEmpresa().getId())) {
				return buscar;
			}
		}

		return null;
	}

	public Empresa getByChave(String chave) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM " + Empresa.class.getSimpleName() + " e ");
		sql.append("WHERE e.chaveEmpresa = :p_chave ");

		final TypedQuery<Empresa> query = this.getManager().createQuery(sql.toString(), Empresa.class);

		query.setParameter("p_chave", chave);

		final List<Empresa> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	@Override
	public String getCustomFilter() {
		// Não dá pra injetar, então usa o CDI Service Locator
		final ISessaoUsuario sessao = CDIServiceLocator.getBean(ISessaoUsuario.class, () -> UserSession.class);

		String sql = " AND (";
		sql += " e.id = " + sessao.getFuncionario().getEmpresa().getId();
		sql += " OR e.empresaPrincipal.id = " + sessao.getFuncionario().getEmpresa().getId();
		sql += ")";
		return sql;
	}

}
