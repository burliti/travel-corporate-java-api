package br.com.travelcorporate.model.lancamentos.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MeusDadosDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private BigDecimal saldo;

	private BigDecimal saldoUltimaViagem;

	private Integer viagensAberto;

	private ViagemDTO ultimaViagem;

	private ViagemDTO ultimaViagemAberta;

	public BigDecimal getSaldo() {
		return this.saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public Integer getViagensAberto() {
		return this.viagensAberto;
	}

	public void setViagensAberto(Integer viagensAberto) {
		this.viagensAberto = viagensAberto;
	}

	public BigDecimal getSaldoUltimaViagem() {
		return this.saldoUltimaViagem;
	}

	public void setSaldoUltimaViagem(BigDecimal saldoUltimaViagem) {
		this.saldoUltimaViagem = saldoUltimaViagem;
	}

	public ViagemDTO getUltimaViagem() {
		return this.ultimaViagem;
	}

	public void setUltimaViagem(ViagemDTO ultimaViagem) {
		this.ultimaViagem = ultimaViagem;
	}

	public ViagemDTO getUltimaViagemAberta() {
		return this.ultimaViagemAberta;
	}

	public void setUltimaViagemAberta(ViagemDTO ultimaViagemAberta) {
		this.ultimaViagemAberta = ultimaViagemAberta;
	}
}
