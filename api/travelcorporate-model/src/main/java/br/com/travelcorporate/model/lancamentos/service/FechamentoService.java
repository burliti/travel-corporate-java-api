package br.com.travelcorporate.model.lancamentos.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.lancamentos.dao.FechamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;

@Stateless
public class FechamentoService extends AbstractService<Integer, Fechamento, FechamentoDao> {

	// @Inject
	// @UserSession
	// private SessaoUsuario sessao;

	@Override
	public void doAntesInserir(Fechamento vo) throws BusinessException {
		super.doAntesInserir(vo);

		// Verifica se já não existe um fechamento em aberto para este
		// funcionário

		if (this.getFechamentoAberto((Funcionario) this.getSessao().getFuncionario()) != null) {
			throw new BusinessException("Já existe um fechamento em aberto, finalize este antes de abrir outro!");
		}

		Fechamento lastFechamento = this.getLastFechamento(this.getSessao().getFuncionario());

		vo.setFuncionarioFechamento((Funcionario) this.getSessao().getFuncionario());

		vo.setDataHoraCadastro(Calendar.getInstance());
		vo.setSaldoAnterior(BigDecimal.ZERO);
		vo.setTotalCreditos(BigDecimal.ZERO);
		vo.setTotalDebitos(BigDecimal.ZERO);
		vo.setSaldoFinal(BigDecimal.ZERO);

		// Busca o lancamento anterior, se existir
		if (lastFechamento != null) {
			vo.setSaldoAnterior(lastFechamento.getSaldoFinal());
		}
	}

	@Override
	public void doAntesAtualizar(Fechamento vo) throws BusinessException {
		super.doAntesAtualizar(vo);

		// Nao vai ter efeito, é apenas para não dar erro na validacao
		vo.setDataHoraCadastro(Calendar.getInstance());
	}

	public Fechamento getFechamentoAberto(Funcionario funcionario) {
		return this.getDao().getFechamentoAberto(funcionario);
	}

	public Fechamento getLastFechamento(IFuncionario funcionario) {
		return this.getDao().getFechamento(funcionario, this.getDao().getLastSequencial(funcionario));
	}

	// @Override
	// public void doAntesSalvar(Fechamento vo) throws BusinessException {
	// super.doAntesSalvar(vo);
	//
	// // Calcula saldo final
	// vo.setTotalCreditos(BigDecimal.ZERO);
	// vo.setTotalDebitos(BigDecimal.ZERO);
	//
	// if (vo.getViagem() != null) {
	// for (Lancamento lancamento : vo.getViagem().getLancamentos()) {
	// if (lancamento.getTipoLancamento() == TipoLancamento.CREDITO) {
	// vo.setTotalCreditos(vo.getTotalCreditos().add(lancamento.getValorLancamento()));
	// } else if (lancamento.getTipoLancamento() == TipoLancamento.DEBITO) {
	// vo.setTotalDebitos(vo.getTotalDebitos().add(lancamento.getValorLancamento()));
	// }
	// }
	// }
	// }

	@Override
	public void doAntesConsultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		super.doAntesConsultar(filtros, pageSize, pageNumber, order);

		filtros.put("funcionario.id", this.getSessao().getFuncionario().getId().toString());
	}

	@Override
	public Fechamento doDepoisBuscar(Fechamento vo) {
		if (vo.getFuncionarioFechamento().getId().equals(this.getSessao().getFuncionario().getId())) {
			return vo;
		}
		return null;
	}
}