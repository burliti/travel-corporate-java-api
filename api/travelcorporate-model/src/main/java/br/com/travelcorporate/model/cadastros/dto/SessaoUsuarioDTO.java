package br.com.travelcorporate.model.cadastros.dto;

import java.io.Serializable;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.core.enums.TipoSessao;
import br.com.travelcorporate.core.vo.IEmpresa;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessaoUsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String chaveSessao;

	private Calendar dataCriacao;

	private String enderecoIp;

	private String hostName;

	private TipoSessao tipoSessao;

	private Calendar ultimaInteracao;

	private IEmpresa empresa;

	private FuncionarioDTO funcionario;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChaveSessao() {
		return this.chaveSessao;
	}

	public void setChaveSessao(String chaveSessao) {
		this.chaveSessao = chaveSessao;
	}

	public Calendar getDataCriacao() {
		return this.dataCriacao;
	}

	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getEnderecoIp() {
		return this.enderecoIp;
	}

	public void setEnderecoIp(String enderecoIp) {
		this.enderecoIp = enderecoIp;
	}

	public String getHostName() {
		return this.hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public TipoSessao getTipoSessao() {
		return this.tipoSessao;
	}

	public void setTipoSessao(TipoSessao tipoSessao) {
		this.tipoSessao = tipoSessao;
	}

	public Calendar getUltimaInteracao() {
		return this.ultimaInteracao;
	}

	public void setUltimaInteracao(Calendar ultimaInteracao) {
		this.ultimaInteracao = ultimaInteracao;
	}

	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public FuncionarioDTO getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(FuncionarioDTO funcionario) {
		this.funcionario = funcionario;
	}
}
