package br.com.travelcorporate.model.mensageria.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MensagemFormaNotificacaoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private FormaNotificacaoDTO formaNotificacao;

	private MensagemDTO mensagem;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FormaNotificacaoDTO getFormaNotificacao() {
		return this.formaNotificacao;
	}

	public void setFormaNotificacao(FormaNotificacaoDTO formaNotificacao) {
		this.formaNotificacao = formaNotificacao;
	}

	public MensagemDTO getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(MensagemDTO mensagem) {
		this.mensagem = mensagem;
	}

}
