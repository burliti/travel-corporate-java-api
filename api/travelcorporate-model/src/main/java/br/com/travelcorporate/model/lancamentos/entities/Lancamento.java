package br.com.travelcorporate.model.lancamentos.entities;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.core.utils.StringUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Cliente;
import br.com.travelcorporate.model.cadastros.entities.Fornecedor;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Categoria;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;
import br.com.travelcorporate.model.geral.enums.TipoPeriodo;
import br.com.travelcorporate.model.geral.enums.converters.FlagConverter;
import br.com.travelcorporate.model.geral.enums.converters.TipoLancamentoConverter;
import br.com.travelcorporate.model.geral.enums.converters.TipoPeriodoConverter;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Lancamento extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_lancamento", sequenceName = "seq_lancamento", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_lancamento")
	@Column(name = "id_lancamento")
	private Integer id;

	@NotNull(message = "Sequencial é obrigatório")
	private Integer sequencial;

	@Column(name = "data_hora_lancamento", updatable = false)
	private Calendar dataHoraLancamento;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicial")
	private Calendar dataInicial;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_final")
	private Calendar dataFinal;

	private String observacoes;

	@NotNull(message = "Tipo de lançamento é obrigatório")
	@Column(name = "tipo_lancamento")
	@Convert(converter = TipoLancamentoConverter.class)
	private TipoLancamento tipoLancamento;

	@NotNull(message = "Valor é obrigatório")
	@Column(name = "valor_lancamento")
	private BigDecimal valorLancamento;

	@NotNull(message = "Categoria é obrigatório")
	@ManyToOne
	@JoinColumn(name = "id_categoria")
	private Categoria categoria;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@NotNull(message = "Funcionário é obrigatório")
	@ManyToOne
	@JoinColumn(name = "id_funcionario")
	private Funcionario funcionario;

	@NotNull(message = "Funcionário de lançamento é obrigatório")
	@ManyToOne
	@JoinColumn(name = "id_funcionario_lancamento")
	private Funcionario funcionarioLancamento;

	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH }, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_recibo", insertable = true, updatable = true)
	private Recibo recibo;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_viagem")
	private Viagem viagem;

	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;

	@ManyToOne
	@JoinColumn(name = "id_lancamento_referenciado")
	private Lancamento referenciado;

	@ManyToOne
	@JoinColumn(name = "id_fornecedor")
	private Fornecedor fornecedor;

	@Convert(converter = TipoPeriodoConverter.class)
	@Column(name = "tipo_periodo")
	@NotNull(message = "Tipo de período é obrigatório!")
	private TipoPeriodo tipoPeriodo;

	@Convert(converter = FlagConverter.class)
	@Column(name = "flag_glosado")
	// @NotNull(message = "Flag glosado é obrigatório!")
	private Flag glosado;

	@Column(name = "valor_original")
	private BigDecimal valorOriginal;

	@Column(name = "valor_glosa")
	private BigDecimal valorGlosa;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_glosa")
	private Funcionario funcionarioGlosa;

	@Column(name = "observacoes_glosa")
	private String observacoesGlosa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Calendar getDataHoraLancamento() {
		return this.dataHoraLancamento;
	}

	@JsonGetter("dataHoraLancamento")
	public String getDataHoraLancamentoFormatada() {
		if (this.getDataHoraLancamento() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATAHORA_PADRAO);

			return sdf.format(this.getDataHoraLancamento().getTime());
		}
		return null;
	}

	public void setDataHoraLancamento(Calendar dataHoraLancamento) {
		this.dataHoraLancamento = dataHoraLancamento;
	}

	public void setDataHoraLancamento(String dataHoraLancamento) {
		//
	}

	public String getObservacoes() {
		return this.observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public TipoLancamento getTipoLancamento() {
		return this.tipoLancamento;
	}

	public void setTipoLancamento(TipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	@JsonGetter("valorLancamento")
	public BigDecimal getValorLancamento() {
		return this.valorLancamento;
	}

	public BigDecimal getValorLancamentoM() {
		return this.valorLancamento.multiply(new BigDecimal(this.getTipoLancamento().getMultiplicador()));
	}

	@JsonIgnore
	public String getValorLancamentoFormatado() {
		return FormatUtils.formatMoney(this.getValorLancamento());
	}

	public void setValorLancamento(BigDecimal valorLancamento) {
		this.valorLancamento = valorLancamento;
	}

	@JsonSetter("valorLancamento")
	public void setValorLancamento(String valorLancamento) {
		this.valorLancamento = FormatUtils.parseNumber(valorLancamento);
	}

	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Recibo getRecibo() {
		return this.recibo;
	}

	public void setRecibo(Recibo recibo) {
		this.recibo = recibo;
	}

	public Viagem getViagem() {
		return this.viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public Integer getSequencial() {
		return this.sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Lancamento getReferenciado() {
		return this.referenciado;
	}

	public void setReferenciado(Lancamento referenciado) {
		this.referenciado = referenciado;
	}

	public Fornecedor getFornecedor() {
		return this.fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public TipoPeriodo getTipoPeriodo() {
		return this.tipoPeriodo;
	}

	public void setTipoPeriodo(TipoPeriodo tipoPeriodo) {
		this.tipoPeriodo = tipoPeriodo;
	}

	@JsonGetter("dataFinal")
	public String getDataFinalFormatada() {
		return FormatUtils.formatDate(this.getDataFinal());
	}

	public Calendar getDataFinal() {
		return this.dataFinal;
	}

	@JsonSetter("dataFinal")
	public void setDataFinal(String dataFinal) {
		this.setDataFinal(FormatUtils.parseDate(dataFinal));
	}

	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}

	@JsonGetter("dataInicial")
	public String getDataInicialFormatada() {
		return FormatUtils.formatDate(this.getDataInicial());
	}

	public Calendar getDataInicial() {
		return this.dataInicial;
	}

	@JsonSetter("dataInicial")
	public void setDataInicial(String dataInicial) {
		this.setDataInicial(FormatUtils.parseDate(dataInicial));
	}

	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Funcionario getFuncionarioLancamento() {
		return this.funcionarioLancamento;
	}

	public void setFuncionarioLancamento(Funcionario funcionarioLancamento) {
		this.funcionarioLancamento = funcionarioLancamento;
	}

	public Flag getGlosado() {
		return this.glosado;
	}

	public void setGlosado(Flag glosado) {
		this.glosado = glosado;
	}

	public BigDecimal getValorOriginal() {
		return this.valorOriginal;
	}

	public void setValorOriginal(BigDecimal valorOriginal) {
		this.valorOriginal = valorOriginal;
	}

	public BigDecimal getValorGlosa() {
		return this.valorGlosa;
	}

	public void setValorGlosa(BigDecimal valorGlosa) {
		this.valorGlosa = valorGlosa;
	}

	public Funcionario getFuncionarioGlosa() {
		return this.funcionarioGlosa;
	}

	public void setFuncionarioGlosa(Funcionario funcionarioGlosa) {
		this.funcionarioGlosa = funcionarioGlosa;
	}

	public String getObservacoesGlosa() {
		return this.observacoesGlosa;
	}

	public void setObservacoesGlosa(String observacoesGlosa) {
		this.observacoesGlosa = observacoesGlosa;
	}
}