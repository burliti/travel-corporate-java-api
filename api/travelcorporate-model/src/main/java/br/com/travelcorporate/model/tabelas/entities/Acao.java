package br.com.travelcorporate.model.tabelas.entities;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IgnoreEmpresa;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@IgnoreEmpresa
public class Acao extends AbstractEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_acao", sequenceName = "seq_acao", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_acao")
	@Column(name = "id_acao")
	private Integer id;

	private String nome;

	private String descricao;

	@Column(name = "status")
	@Convert(converter = StatusConverter.class)
	private Status status;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@JsonIgnore
	@Override
	public IEmpresa getEmpresa() {
		return null;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}