package br.com.travelcorporate.model.geral.exceptions.financeiro;

import br.com.travelcorporate.core.exceptions.BusinessException;

public class AprovacaoContasException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public AprovacaoContasException(String message) {
		super(message);
	}
}
