package br.com.travelcorporate.model.lancamentos.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;

@Stateless
public class FechamentoDao extends AbstractDao<Integer, Fechamento> {

	public Integer getNextSequencial(IFuncionario iFuncionario) {
		return this.getLastSequencial(iFuncionario) + 1;
	}

	public Integer getLastSequencial(IFuncionario iFuncionario) {
		String sql = "SELECT MAX(e.sequencial) FROM " + this.getVOClass().getSimpleName() + " e WHERE e.funcionarioFechamento.id = :p_funcionario";

		TypedQuery<Integer> query = this.getManager().createQuery(sql, Integer.class);

		query.setParameter("p_funcionario", iFuncionario.getId());

		Integer result = query.getSingleResult();

		if (result == null) {
			result = 0;
		}

		return result;
	}

	public Fechamento getFechamentoAberto(Funcionario funcionario) {
		String sql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e WHERE e.funcionarioFechamento.id = :p_funcionario AND e.viagem.status IN (:p_status)";

		TypedQuery<Fechamento> query = this.getManager().createQuery(sql, Fechamento.class);

		List<StatusViagem> list = new ArrayList<>();
		list.add(StatusViagem.ANDAMENTO);
		list.add(StatusViagem.REJEITADA);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_status", list);

		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			System.err.println(e);
			return null;
		} catch (NoResultException e) {
			return null;
		}
	}

	public Fechamento getFechamento(IFuncionario funcionario, Integer sequencial) {
		String sql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e WHERE e.funcionarioFechamento.id = :p_funcionario AND e.sequencial = :p_sequencial";

		TypedQuery<Fechamento> query = this.getManager().createQuery(sql, Fechamento.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_sequencial", sequencial);

		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			System.err.println(e);
			return null;
		} catch (NoResultException e) {
			return null;
		}
	}
}
