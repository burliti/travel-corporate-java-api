package br.com.travelcorporate.model.relatorios.despesas.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.ServletContext;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.cadastros.service.SetorService;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Stateless
public class RelatorioDespesasService {

	@Inject
	ServletContext servletContext;

	@Inject
	private EntityManager manager;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private SetorService setorService;

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private CategoriaService categoriaService;

	@Inject
	private CentroCustoService centroCustoService;

	public byte[] processarRelatorio(ConsultaRequest request) throws IOException {
		String tipoRelatorio = request.getFiltro().get("tipoRelatorio");

		if (tipoRelatorio != null) {
			if (tipoRelatorio.equalsIgnoreCase("DESPESAS_DIA")) {
				return this.processarRelatorioPorDia(request);
			} else if (tipoRelatorio.equalsIgnoreCase("DESPESAS_DIA_CATEGORIA")) {
				return this.processarRelatorioPorDiaCategoria(request);
			} else if (tipoRelatorio.equalsIgnoreCase("DESPESAS_SEMANA")) {
				return this.processarRelatorioPorSemana(request);
			} else if (tipoRelatorio.equalsIgnoreCase("DESPESAS_DETALHADA")) {
				return this.processarRelatorioDetalhado(request);
			}
		}

		return null;
	}

	public byte[] processarRelatorioPorDia(ConsultaRequest request) {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			HashMap<String, Object> parameters = new HashMap<>();

			// Monta a linha de parâmetros do relatório
			this.montarLinhaParametros(request, parameters);

			IEmpresa empresa = this.funcionarioService.getSessao().getEmpresa();
			parameters.put("empresa", empresa);
			parameters.put("empresaId", empresa.getId());
			parameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			Session session = this.manager.unwrap(Session.class);
			Connection conn = ((SessionImpl) session).connection();

			JasperPrint print = JasperFillManager.fillReport(classloader.getResource("br/com/travelcorporate/reports/despesas/despesas_dia.jasper").openStream(), parameters, conn);
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] bytes = baos.toByteArray();

			return bytes;

		} catch (JRException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public byte[] processarRelatorioPorDiaCategoria(ConsultaRequest request) {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			HashMap<String, Object> parameters = new HashMap<>();

			// Monta a linha de parâmetros do relatório
			this.montarLinhaParametros(request, parameters);

			IEmpresa empresa = this.funcionarioService.getSessao().getEmpresa();
			parameters.put("empresa", empresa);
			parameters.put("empresaId", empresa.getId());
			parameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			Session session = this.manager.unwrap(Session.class);
			Connection conn = ((SessionImpl) session).connection();

			JasperPrint print = JasperFillManager.fillReport(classloader.getResource("br/com/travelcorporate/reports/despesas/despesas_dia_categoria.jasper").openStream(), parameters, conn);
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] bytes = baos.toByteArray();

			return bytes;

		} catch (JRException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public byte[] processarRelatorioPorSemana(ConsultaRequest request) {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			HashMap<String, Object> parameters = new HashMap<>();

			// Monta a linha de parâmetros do relatório
			this.montarLinhaParametros(request, parameters);

			IEmpresa empresa = this.funcionarioService.getSessao().getEmpresa();
			parameters.put("empresa", empresa);
			parameters.put("empresaId", empresa.getId());
			parameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			Session session = this.manager.unwrap(Session.class);
			Connection conn = ((SessionImpl) session).connection();

			JasperPrint print = JasperFillManager.fillReport(classloader.getResource("br/com/travelcorporate/reports/despesas/despesas_semana.jasper").openStream(), parameters, conn);
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] bytes = baos.toByteArray();

			return bytes;

		} catch (JRException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public byte[] processarRelatorioDetalhado(ConsultaRequest request) {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();

		try {

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			HashMap<String, Object> parameters = new HashMap<>();

			// Monta a linha de parâmetros do relatório
			this.montarLinhaParametros(request, parameters);

			IEmpresa empresa = this.funcionarioService.getSessao().getEmpresa();
			parameters.put("empresa", empresa);
			parameters.put("empresaId", empresa.getId());
			parameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			Session session = this.manager.unwrap(Session.class);
			Connection conn = ((SessionImpl) session).connection();

			JasperPrint print = JasperFillManager.fillReport(classloader.getResource("br/com/travelcorporate/reports/despesas/despesas_detalhada.jasper").openStream(), parameters, conn);
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] bytes = baos.toByteArray();

			return bytes;

		} catch (JRException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private void montarLinhaParametros(ConsultaRequest request, HashMap<String, Object> parameters) {

		int linha = 0;

		Integer filialId = FormatUtils.parseInt(request.getFiltro().get("filialId"));
		if (filialId != null) {
			parameters.put("linha" + ++linha, "Filial: " + this.empresaService.buscar(filialId).getNome());
			parameters.put("filialId", filialId);
		}

		Integer setorId = FormatUtils.parseInt(request.getFiltro().get("setorId"));
		if (setorId != null) {
			parameters.put("linha" + ++linha, "Setor: " + this.setorService.buscar(setorId).getNome());
			parameters.put("setorId", setorId);
		}

		Integer funcionarioId = FormatUtils.parseInt(request.getFiltro().get("funcionarioId"));
		if (funcionarioId != null) {
			parameters.put("linha" + ++linha, "Funcionário: " + this.funcionarioService.buscar(funcionarioId).getNome());
			parameters.put("funcionarioId", funcionarioId);
		}

		String statusViagem = request.getFiltro().get("statusViagem");
		if (statusViagem != null && StatusViagem.fromValue(statusViagem) != null) {
			parameters.put("linha" + ++linha, "Status da Viagem: " + StatusViagem.fromValue(statusViagem).getDescricao());
			parameters.put("statusViagem", StatusViagem.fromValue(statusViagem).getValor());
		}

		Integer categoriaId = FormatUtils.parseInt(request.getFiltro().get("categoriaId"));
		if (categoriaId != null) {
			parameters.put("linha" + ++linha, "Categoria: " + this.categoriaService.buscar(categoriaId).getNome());
			parameters.put("categoriaId", categoriaId);
		}

		Integer centroCustoId = FormatUtils.parseInt(request.getFiltro().get("centroCustoId"));
		if (centroCustoId != null) {
			if (this.centroCustoService.buscar(centroCustoId) != null) {
				parameters.put("linha" + ++linha, "C. Custo: " + this.centroCustoService.buscar(centroCustoId).getDescricaoCompleta());
			}
			parameters.put("centroCustoId", centroCustoId);
		}

		Calendar dataInicial = FormatUtils.parseDate(request.getFiltro().get("dataInicial"));
		if (dataInicial != null) {
			parameters.put("linha" + ++linha, "Data inicial: " + FormatUtils.formatDate(dataInicial));
			parameters.put("dataInicial", dataInicial.getTime());
		}

		Calendar dataFinal = FormatUtils.parseDate(request.getFiltro().get("dataFinal"));
		if (dataFinal != null) {
			parameters.put("linha" + ++linha, "Data final: " + FormatUtils.formatDate(dataFinal));
			parameters.put("dataFinal", dataFinal.getTime());
		}
	}
}
