package br.com.travelcorporate.model.mensageria.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Empresa;

@Entity
@Table(name = "mensagem_forma_notificacao")
public class MensagemFormaNotificacao extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_mensagem_forma_notificacao", sequenceName = "seq_mensagem_forma_notificacao", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mensagem_forma_notificacao")
	@Column(name = "id_mensagem_forma_notificacao")
	private Integer id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_forma_notificacao")
	private FormaNotificacao formaNotificacao;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_mensagem")
	private Mensagem mensagem;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public FormaNotificacao getFormaNotificacao() {
		return this.formaNotificacao;
	}

	public void setFormaNotificacao(FormaNotificacao formaNotificacao) {
		this.formaNotificacao = formaNotificacao;
	}

	public Mensagem getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}
}
