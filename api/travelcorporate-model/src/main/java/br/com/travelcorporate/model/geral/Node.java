package br.com.travelcorporate.model.geral;

import java.util.ArrayList;
import java.util.List;

public class Node {

	private String id;

	private String nome;

	private String url;

	private boolean selecionado;

	private String icone;

	private List<Node> filhos;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Node> getFilhos() {
		if (this.filhos == null) {
			this.filhos = new ArrayList<>();
		}
		return this.filhos;
	}

	public void setFilhos(List<Node> filhos) {
		this.filhos = filhos;
	}

	public boolean isSelecionado() {
		return this.selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getIcone() {
		return this.icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
