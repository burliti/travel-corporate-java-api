package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusFechamento {

	RASCUNHO(1, "Rascunho"), FECHADO(2, "Finalizado"), APROVADO(3, "Aprovado"), RECEBIDO(4, "Recebido"), REJEITADO(99, "Rejeitado");

	private String descricao;
	private Integer valor;

	StatusFechamento(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static StatusFechamento fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final StatusFechamento status : StatusFechamento.values()) {
			if (status.getValor().toString().equals(source.toUpperCase())) {
				return status;
			} else if (status.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return status;
			} else if (status.toString().equalsIgnoreCase(source)) {
				return status;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
