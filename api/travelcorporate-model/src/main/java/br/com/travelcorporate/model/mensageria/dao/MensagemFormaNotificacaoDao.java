package br.com.travelcorporate.model.mensageria.dao;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.mensageria.entities.MensagemFormaNotificacao;

@Stateless
public class MensagemFormaNotificacaoDao extends AbstractDao<Integer, MensagemFormaNotificacao> {

}
