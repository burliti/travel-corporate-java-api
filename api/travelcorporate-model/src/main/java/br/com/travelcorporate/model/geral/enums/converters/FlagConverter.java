package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.Flag;

@Converter(autoApply = true)
public class FlagConverter implements AttributeConverter<Flag, String> {

	@Override
	public String convertToDatabaseColumn(Flag flag) {
		if (flag == null) {
			return null;
		}
		return flag.getValor();
	}

	@Override
	public Flag convertToEntityAttribute(String source) {
		return Flag.fromValue(source);
	}

}