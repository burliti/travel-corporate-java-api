package br.com.travelcorporate.model.lancamentos.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.StringUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Fornecedor;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.TipoPessoa;
import br.com.travelcorporate.model.geral.enums.converters.TipoPessoaConverter;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recibo extends AbstractEntity<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_recibo")
	@SequenceGenerator(name = "seq_recibo", sequenceName = "seq_recibo", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_recibo")
	private Integer id;

	@Column(name = "data_inserido", updatable = false)
	private Calendar dataInserido;

	// @NotNull(message = "Data de referência do recibo é obrigatória")
	@Temporal(TemporalType.DATE)
	@Column(name = "data_referencia")
	private Calendar dataReferencia;

	@Column(name = "foto_anexo_recibo")
	private String fotoAnexoRecibo;

	// @NotNull(message = "Número do recibo é obrigatório")
	@Column(name = "numero_documento")
	private String numeroDocumento;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_funcionario")
	private Funcionario funcionario;

	// @NotNull(message = "Valor do recibo é obrigatório")
	@Column(name = "valor_recibo")
	private BigDecimal valor;

	@Column(name = "chave_nfe")
	private String chaveNfe;

	@JoinColumn(name = "id_fornecedor")
	@ManyToOne
	private Fornecedor fornecedor;

	@Column(name = "url_nfe")
	private String urlNfe;

	@Column(name = "nome_fornecedor")
	private String nomeFornecedor;

	@Column(name = "cpf_cnpj_fornecedor")
	private String cpfCnpjFornecedor;

	@Convert(converter = TipoPessoaConverter.class)
	@Column(name = "tipo_fornecedor")
	private TipoPessoa tipoFornecedor;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Calendar getDataInserido() {
		return this.dataInserido;
	}

	@JsonGetter("dataInserido")
	public String getDataInseridoFormatada() {
		if (this.getDataInserido() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

			return sdf.format(this.getDataInserido().getTime());
		}
		return null;
	}

	public void setDataInserido(Calendar dataInserido) {
		this.dataInserido = dataInserido;
	}

	@JsonSetter("dataInserido")
	public void setDataInserido(String dataInserido) {
		//
	}

	public Calendar getDataReferencia() {
		return this.dataReferencia;
	}

	@JsonGetter("dataReferencia")
	public String getDataReferenciaFormatada() {
		if (this.getDataReferencia() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATA_PADRAO);

			return sdf.format(this.getDataReferencia().getTime());
		}
		return null;
	}

	public void setDataReferencia(Calendar dataReferencia) {
		this.dataReferencia = dataReferencia;
	}

	@JsonSetter("dataReferencia")
	public void setDataReferencia(String dataReferencia) {
		if (dataReferencia != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(StringUtils.FORMATO_DATA_PADRAO);

			Date parse = null;
			try {
				parse = sdf.parse(dataReferencia);
			} catch (final ParseException e) {
				e.printStackTrace();
			}

			if (parse != null) {
				this.dataReferencia = Calendar.getInstance();
				this.dataReferencia.setTimeInMillis(parse.getTime());
			} else {
				this.dataReferencia = null;
			}
		} else {
			this.dataReferencia = null;
		}
	}

	public String getFotoAnexoRecibo() {
		return this.fotoAnexoRecibo;
	}

	public void setFotoAnexoRecibo(String fotoAnexoRecibo) {
		this.fotoAnexoRecibo = fotoAnexoRecibo;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	// @JsonGetter("valor")
	// public String getValorFormatado() {
	// return FormatUtils.formatMoney(this.getValor());
	// }
	//
	// @JsonSetter("valor")
	// public void setValor(String valor) {
	// this.valor = FormatUtils.parseNumber(valor);
	// }

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getChaveNfe() {
		return this.chaveNfe;
	}

	public void setChaveNfe(String chaveNfe) {
		this.chaveNfe = chaveNfe;
	}

	public String getUrlNfe() {
		return this.urlNfe;
	}

	public void setUrlNfe(String urlNfe) {
		this.urlNfe = urlNfe;
	}

	public String getNomeFornecedor() {
		return this.nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public String getCpfCnpjFornecedor() {
		return this.cpfCnpjFornecedor;
	}

	public void setCpfCnpjFornecedor(String cpfCnpjFornecedor) {
		this.cpfCnpjFornecedor = cpfCnpjFornecedor;
	}

	public TipoPessoa getTipoFornecedor() {
		return this.tipoFornecedor;
	}

	public void setTipoFornecedor(TipoPessoa tipoFornecedor) {
		this.tipoFornecedor = tipoFornecedor;
	}

}