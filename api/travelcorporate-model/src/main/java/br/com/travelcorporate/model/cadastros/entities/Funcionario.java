package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.utils.SegurancaUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.configuracao.entities.Perfil;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.FlagConverter;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class Funcionario extends AbstractEntity<Integer> implements IFuncionario, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_funcionario", sequenceName = "seq_funcionario", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_funcionario")
	@Column(name = "id_funcionario")
	private Integer id;

	@Column(name = "codigo_referencia")
	private String codigoReferencia;

	@Convert(converter = FlagConverter.class)
	@NotNull(message = "Administrador é obrigatório")
	private Flag administrador;

	@Convert(converter = FlagConverter.class)
	@NotNull(message = "Resetar Senha é obrigatório")
	@Column(name = "resetar_senha")
	private Flag resetarSenha;

	@Convert(converter = StatusConverter.class)
	@NotNull(message = "Status é obrigatório")
	private Status status;

	private String celular;

	@NotEmpty(message = "Email é obrigatório")
	@Email(message = "Email inválido")
	private String email;

	@NotEmpty(message = "Nome é obrigatório")
	private String nome;

	@NotEmpty(message = "Senha é obrigatória")
	private String senha;

	@Transient
	private String confirmacaoSenha;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	@NotNull(message = "Empresa é obrigatório")
	private IEmpresa empresa;

	@ManyToOne
	@JoinColumn(name = "id_perfil")
	@NotNull(message = "Perfil é obrigatório")
	private Perfil perfil;

	@ManyToOne
	@JoinColumn(name = "id_empresa_filial")
	private Empresa filial;

	@ManyToOne
	@JoinColumn(name = "id_setor")
	private Setor setor;

	@ManyToOne
	@JoinColumn(name = "id_centro_custo")
	private CentroCusto centroCusto;

	public Flag getAdministrador() {
		return this.administrador;
	}

	public void setAdministrador(Flag administrador) {
		this.administrador = administrador;
	}

	@Override
	public String getEmail() {
		return this.email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@JsonIgnore
	public String getSenhaMD5() {
		if (this.getSenha() != null) {
			return SegurancaUtils.md5(this.getSenha());
		}
		return null;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public Flag getResetarSenha() {
		return this.resetarSenha;
	}

	public void setResetarSenha(Flag resetarSenha) {
		this.resetarSenha = resetarSenha;
	}

	public String getConfirmacaoSenha() {
		return this.confirmacaoSenha;
	}

	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}

	public String getCodigoReferencia() {
		return this.codigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}

	public Perfil getPerfil() {
		return this.perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@Transient
	public boolean isAdministrador() {
		return this.getAdministrador() == Flag.SIM;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (this.getId() == null ? 0 : this.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Funcionario)) {
			return false;
		}
		Funcionario other = (Funcionario) obj;
		if (this.getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!this.getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

	public Empresa getFilial() {
		return this.filial;
	}

	public void setFilial(Empresa filial) {
		this.filial = filial;
	}

	public Setor getSetor() {
		return this.setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public CentroCusto getCentroCusto() {
		return this.centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}

}