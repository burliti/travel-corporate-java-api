package br.com.travelcorporate.model.cadastros.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dao.SetorDao;
import br.com.travelcorporate.model.cadastros.entities.Setor;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.geral.enums.Status;

@Stateless
public class SetorService extends AbstractService<Integer, Setor, SetorDao> {

	@Inject
	private EmpresaService empresaService;

	@Inject
	private CentroCustoService centroCustoService;

	public List<Setor> getAtivos() {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());

		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}

	@Override
	public void doAntesSalvar(Setor vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getFilial() != null) {
			if (vo.getFilial().getId() == null || vo.getFilial().getId() <= 0) {
				vo.setFilial(null);
			} else {
				vo.setFilial(this.empresaService.buscar(vo.getFilial().getId()));
			}
		}

		if (vo.getCentroCusto() != null) {
			if (vo.getCentroCusto().getId() == null || vo.getCentroCusto().getId() <= 0) {
				vo.setCentroCusto(null);
			} else {
				vo.setCentroCusto(this.centroCustoService.buscar(vo.getCentroCusto().getId()));
			}
		}
	}
}
