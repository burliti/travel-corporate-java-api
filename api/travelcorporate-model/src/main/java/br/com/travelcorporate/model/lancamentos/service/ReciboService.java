package br.com.travelcorporate.model.lancamentos.service;

import java.util.Calendar;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.lancamentos.dao.ReciboDao;
import br.com.travelcorporate.model.lancamentos.entities.Recibo;

@Stateless
public class ReciboService extends AbstractService<Integer, Recibo, ReciboDao> {

	@Override
	public void doAntesInserir(Recibo vo) throws BusinessException {
		super.doAntesInserir(vo);

		vo.setFuncionario((Funcionario) this.getSessao().getFuncionario());
		vo.setEmpresa(this.getSessao().getEmpresa());
		vo.setDataInserido(Calendar.getInstance());
	}
}
