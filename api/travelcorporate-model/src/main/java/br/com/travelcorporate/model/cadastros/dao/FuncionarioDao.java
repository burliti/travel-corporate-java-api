package br.com.travelcorporate.model.cadastros.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class FuncionarioDao extends AbstractDao<Integer, Funcionario> {

	public Funcionario buscarPorEmail(String email, Status ativo) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM " + Funcionario.class.getSimpleName() + " e ");
		sql.append("WHERE e.email = :p_email ");
		sql.append("  AND e.status = :p_status ");

		final TypedQuery<Funcionario> query = this.getManager().createQuery(sql.toString(), Funcionario.class);

		query.setParameter("p_email", email);
		query.setParameter("p_status", ativo);

		final List<Funcionario> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public List<Funcionario> getFuncionarios(List<Funcionario> funcionarios) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT e FROM " + Funcionario.class.getSimpleName() + " e ");
		sql.append(" WHERE e.id in :p_ids");

		final TypedQuery<Funcionario> query = this.getManager().createQuery(sql.toString(), Funcionario.class);

		final List<Integer> ids = new ArrayList<>();

		for (final Funcionario funcionario : funcionarios) {
			ids.add(funcionario.getId());
		}

		query.setParameter("p_ids", ids);

		final List<Funcionario> list = query.getResultList();

		return list;
	}

	public void atualizarSenha(Funcionario f) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE " + Funcionario.class.getSimpleName() + " e \n");
		sql.append("    SET \n");
		sql.append("     e.senha = :p_senha, e.resetarSenha = :p_resetarSenha \n");
		sql.append("  WHERE e.id = :p_id");

		Query query = this.getManager().createQuery(sql.toString());

		query.setParameter("p_senha", f.getSenha());
		query.setParameter("p_resetarSenha", f.getResetarSenha());
		query.setParameter("p_id", f.getId());

		query.executeUpdate();
	}

	// public List<Funcionario> getFuncionariosAtivos() {
	// final StringBuilder sql = new StringBuilder();
	// sql.append("SELECT e FROM " + Funcionario.class.getSimpleName() + " e ");
	// sql.append(" WHERE e.status = :p_status");
	//
	// final TypedQuery<Funcionario> query =
	// this.getManager().createQuery(sql.toString(), Funcionario.class);
	//
	// query.setParameter("p_status", Status.ATIVO);
	//
	// final List<Funcionario> list = query.getResultList();
	//
	// return list;
	// }

	public BigDecimal getSaldo(IFuncionario funcionario) {
		final StringBuilder sql = new StringBuilder();

		sql.append("SELECT  \n");
		sql.append("	COALESCE(SUM(l.valor_lancamento * (CASE WHEN l.tipo_lancamento = 'D' THEN -1 ELSE 1 END)) , 0) AS saldo \n");
		sql.append("  FROM lancamento l \n");
		sql.append(" WHERE l.id_funcionario = :p_funcionario \n");
		sql.append(" ORDER BY 1 DESC \n");

		Query query = this.getManager().createNativeQuery(sql.toString());
		query.setParameter("p_funcionario", funcionario.getId());

		BigDecimal result = (BigDecimal) query.getSingleResult();
		return result == null ? BigDecimal.ZERO : result;
	}

	public Integer getViagensEmAberto(IFuncionario funcionario) {
		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(e) FROM " + Viagem.class.getSimpleName() + " e \n");
		sql.append(" WHERE e.funcionario.id = :p_funcionario \n");
		sql.append("   AND e.status IN :p_status \n");

		Query query = this.getManager().createQuery(sql.toString());

		query.setParameter("p_funcionario", funcionario.getId());

		List<StatusViagem> list = new ArrayList<>();

		list.add(StatusViagem.ANDAMENTO);
		list.add(StatusViagem.APROVADA_VIAGEM);
		list.add(StatusViagem.REJEITADA);
		list.add(StatusViagem.SOLICITADA);

		query.setParameter("p_status", list);

		return ((Long) query.getSingleResult()).intValue();
	}
}