package br.com.travelcorporate.model.configuracao.dao;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.configuracao.entities.ConfiguracaoAprovacaoViagem;

@Stateless
public class ConfiguracaoAprovacaoViagemDao extends AbstractDao<Integer, ConfiguracaoAprovacaoViagem> {

}
