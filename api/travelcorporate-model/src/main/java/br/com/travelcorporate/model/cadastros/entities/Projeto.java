package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
public class Projeto extends AbstractEntity<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_projeto")
	@SequenceGenerator(name = "seq_projeto", sequenceName = "seq_projeto", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_projeto")
	private Integer id;

	@Convert(converter = StatusConverter.class)
	private Status status;

	private String nome;

	@Column(name = "percentual_alerta_gastos")
	private BigDecimal percentualAlertaGastos;

	@Column(name = "percentual_limite_gastos")
	private BigDecimal percentualLimiteGastos;

	private BigDecimal valor;

	@JoinTable(name = "cliente_projeto", joinColumns = { @JoinColumn(name = "id_projeto") }, inverseJoinColumns = { @JoinColumn(name = "id_cliente") })
	@ManyToMany
	private List<Cliente> clientes;

	@ManyToOne
	@JoinColumn(name = "id_centro_custo")
	private CentroCusto centroCusto;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getPercentualAlertaGastos() {
		return this.percentualAlertaGastos;
	}

	public void setPercentualAlertaGastos(BigDecimal percentualAlertaGastos) {
		this.percentualAlertaGastos = percentualAlertaGastos;
	}

	@JsonSetter("percentualAlertaGastos")
	public void setPercentualAlertaGastos(String percentualAlertaGastos) {
		this.percentualAlertaGastos = FormatUtils.parseNumber(percentualAlertaGastos);
	}

	public BigDecimal getPercentualLimiteGastos() {
		return this.percentualLimiteGastos;
	}

	public void setPercentualLimiteGastos(BigDecimal percentualLimiteGastos) {
		this.percentualLimiteGastos = percentualLimiteGastos;
	}

	@JsonSetter("percentualLimiteGastos")
	public void setPercentualLimiteGastos(String percentualLimiteGastos) {
		this.percentualLimiteGastos = FormatUtils.parseNumber(percentualLimiteGastos);
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	@JsonGetter("valor")
	public String getValorFormatado() {
		return FormatUtils.formatMoney(this.valor);
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@JsonSetter("valor")
	public void setValor(String valor) {
		this.valor = FormatUtils.parseNumber(valor);
	}

	public List<Cliente> getClientes() {
		return this.clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public CentroCusto getCentroCusto() {
		return centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}
}