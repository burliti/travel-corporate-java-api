package br.com.travelcorporate.model.mensageria.services;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.mensageria.dao.AssuntoDao;
import br.com.travelcorporate.model.mensageria.dto.AssuntoDTO;
import br.com.travelcorporate.model.mensageria.entities.Assunto;

@Stateless
public class AssuntoService extends AbstractService<Integer, Assunto, AssuntoDao> {

	public AssuntoDTO convertDTO(Assunto assunto) {
		if (assunto == null) {
			return null;
		}

		AssuntoDTO dto = new AssuntoDTO();

		dto.setId(assunto.getId());
		dto.setTextoBotaoConfirmacao(assunto.getTextoBotaoConfirmacao());

		return dto;
	}
}
