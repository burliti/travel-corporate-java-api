package br.com.travelcorporate.model.geral.exceptions.financeiro;

import br.com.travelcorporate.core.exceptions.BusinessException;

public class LancamentoException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public LancamentoException(String message) {
		super(message);
	}
}
