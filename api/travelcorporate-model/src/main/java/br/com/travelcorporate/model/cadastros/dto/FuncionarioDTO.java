package br.com.travelcorporate.model.cadastros.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.core.vo.ISetor;
import br.com.travelcorporate.model.configuracao.dto.EmpresaDTO;
import br.com.travelcorporate.model.configuracao.dto.PerfilDTO;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FuncionarioDTO implements IFuncionario, Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String codigoReferencia;

	private Flag administrador;

	private Flag resetarSenha;

	private Status status;

	private String celular;

	private String email;

	private String nome;

	private String senha;

	private String confirmacaoSenha;

	private PerfilDTO perfil;

	private BigDecimal saldo;

	private ISetor setor;

	private EmpresaDTO filial;

	private CentroCustoDTO centroCusto;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigoReferencia() {
		return this.codigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}

	public Flag getAdministrador() {
		return this.administrador;
	}

	public void setAdministrador(Flag administrador) {
		this.administrador = administrador;
	}

	public Flag getResetarSenha() {
		return this.resetarSenha;
	}

	public void setResetarSenha(Flag resetarSenha) {
		this.resetarSenha = resetarSenha;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	@Override
	public String getEmail() {
		return this.email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return this.senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getConfirmacaoSenha() {
		return this.confirmacaoSenha;
	}

	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}

	public PerfilDTO getPerfil() {
		return this.perfil;
	}

	public void setPerfil(PerfilDTO perfil) {
		this.perfil = perfil;
	}

	public BigDecimal getSaldo() {
		return this.saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	@Override
	public IEmpresa getEmpresa() {
		return null;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {

	}

	public ISetor getSetor() {
		return this.setor;
	}

	public void setSetor(ISetor setor) {
		this.setor = setor;
	}

	public EmpresaDTO getFilial() {
		return this.filial;
	}

	public void setFilial(EmpresaDTO filial) {
		this.filial = filial;
	}

	public CentroCustoDTO getCentroCusto() {
		return this.centroCusto;
	}

	public void setCentroCusto(CentroCustoDTO centroCusto) {
		this.centroCusto = centroCusto;
	}

}
