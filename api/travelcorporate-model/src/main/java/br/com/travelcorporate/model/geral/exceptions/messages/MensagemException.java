package br.com.travelcorporate.model.geral.exceptions.messages;

import br.com.travelcorporate.core.exceptions.BusinessException;

public class MensagemException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public MensagemException(String message) {
		super(message);
	}
}
