package br.com.travelcorporate.model.lancamentos.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.lancamentos.dao.MultaDao;
import br.com.travelcorporate.model.lancamentos.entities.Multa;
import br.com.travelcorporate.model.tabelas.service.CidadeService;

@Stateless
public class MultaService extends AbstractService<Integer, Multa, MultaDao> {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private CidadeService cidadeService;

	@Override
	public void doAntesSalvar(Multa vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getFuncionario() != null && vo.getFuncionario().getId() != null) {
			vo.setFuncionario(this.funcionarioService.buscar(vo.getFuncionario()));
		} else {
			vo.setFuncionario(null);
		}

		if (vo.getFuncionarioCadastro() != null && vo.getFuncionarioCadastro().getId() != null) {
			vo.setFuncionarioCadastro(this.funcionarioService.buscar(vo.getFuncionarioCadastro()));
		} else {
			vo.setFuncionarioCadastro(this.funcionarioService.buscar((Funcionario) this.getSessao().getFuncionario()));
		}

		if (vo.getCidade() != null && vo.getCidade().getId() != null) {
			vo.setCidade(this.cidadeService.buscar(vo.getCidade()));
		} else {
			vo.setCidade(null);
			vo.setNomeCidade(null);
			vo.setUf(null);
		}
	}

	@Override
	public void doAntesInserir(Multa vo) throws BusinessException {
		vo.setFuncionarioCadastro((Funcionario) this.getSessao().getFuncionario());

		super.doAntesInserir(vo);
	}
}
