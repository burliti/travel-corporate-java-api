package br.com.travelcorporate.model.cadastros.dao;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.cadastros.entities.Projeto;

@Stateless
public class ProjetoDao extends AbstractDao<Integer, Projeto> {

}
