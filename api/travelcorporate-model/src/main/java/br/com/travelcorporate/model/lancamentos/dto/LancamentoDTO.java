package br.com.travelcorporate.model.lancamentos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Convert;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;
import br.com.travelcorporate.model.geral.enums.TipoPeriodo;
import br.com.travelcorporate.model.geral.enums.converters.TipoLancamentoConverter;
import br.com.travelcorporate.model.geral.enums.converters.TipoPeriodoConverter;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LancamentoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Integer sequencial;

	private Calendar dataHoraLancamento;

	private Calendar dataInicial;

	private Calendar dataFinal;

	private String observacoes;

	@Convert(converter = TipoLancamentoConverter.class)
	private TipoLancamento tipoLancamento;

	private BigDecimal valorLancamento;

	private CategoriaDTO categoria;

	private FuncionarioDTO funcionario;

	private FuncionarioDTO funcionarioLancamento;

	private ReciboDTO recibo;

	@Convert(converter = TipoPeriodoConverter.class)
	private TipoPeriodo tipoPeriodo;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequencial() {
		return this.sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public Calendar getDataHoraLancamento() {
		return this.dataHoraLancamento;
	}

	public void setDataHoraLancamento(Calendar dataHoraLancamento) {
		this.dataHoraLancamento = dataHoraLancamento;
	}

	@JsonGetter("dataInicial")
	public String getDataInicialFormatada() {
		return FormatUtils.formatDate(this.getDataInicial());
	}

	public Calendar getDataInicial() {
		return this.dataInicial;
	}

	public void setDataInicial(Calendar dataInicial) {
		this.dataInicial = dataInicial;
	}

	@JsonSetter("dataInicial")
	public void setDataInicial(String dataInicial) {
		this.setDataInicial(FormatUtils.parseDate(dataInicial));
	}

	public Calendar getDataFinal() {
		return this.dataFinal;
	}

	@JsonSetter("dataFinal")
	public void setDataFinal(String dataFinal) {
		this.setDataFinal(FormatUtils.parseDate(dataFinal));
	}

	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}

	@JsonGetter("dataFinal")
	public String getDataFinalFormatada() {
		return FormatUtils.formatDate(this.getDataFinal());
	}

	public String getObservacoes() {
		return this.observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public TipoLancamento getTipoLancamento() {
		return this.tipoLancamento;
	}

	public void setTipoLancamento(TipoLancamento tipoLancamento) {
		this.tipoLancamento = tipoLancamento;
	}

	@JsonSetter("valorLancamento")
	public void setValorLancamento(String valorLancamento) {
		this.valorLancamento = FormatUtils.parseNumber(valorLancamento);
	}

	public BigDecimal getValorLancamento() {
		return this.valorLancamento;
	}

	public void setValorLancamento(BigDecimal valorLancamento) {
		this.valorLancamento = valorLancamento;
	}

	public CategoriaDTO getCategoria() {
		return this.categoria;
	}

	public void setCategoria(CategoriaDTO categoria) {
		this.categoria = categoria;
	}

	public FuncionarioDTO getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(FuncionarioDTO funcionario) {
		this.funcionario = funcionario;
	}

	public FuncionarioDTO getFuncionarioLancamento() {
		return this.funcionarioLancamento;
	}

	public void setFuncionarioLancamento(FuncionarioDTO funcionarioLancamento) {
		this.funcionarioLancamento = funcionarioLancamento;
	}

	public ReciboDTO getRecibo() {
		return this.recibo;
	}

	public void setRecibo(ReciboDTO recibo) {
		this.recibo = recibo;
	}

	public TipoPeriodo getTipoPeriodo() {
		return this.tipoPeriodo;
	}

	public void setTipoPeriodo(TipoPeriodo tipoPeriodo) {
		this.tipoPeriodo = tipoPeriodo;
	}
}
