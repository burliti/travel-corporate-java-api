package br.com.travelcorporate.model.tabelas.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IgnoreEmpresa;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@IgnoreEmpresa
public class Recurso extends AbstractEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_recurso", sequenceName = "seq_recurso", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_recurso")
	@Column(name = "id_recurso")
	private Integer id;

	private String nome;

	private String descricao;

	@Column(name = "status")
	@Convert(converter = StatusConverter.class)
	private Status status;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "recurso")
	private List<AcaoRecurso> acoes;

	@JsonIgnore
	@JoinColumn(name = "id_recurso_pai")
	@ManyToOne
	private Recurso recursoPai;

	@NotNull
	private Integer ordem;

	private String icone;

	@OneToMany(mappedBy = "recursoPai", orphanRemoval = true)
	private List<Recurso> filhos;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	@JsonIgnore
	public IEmpresa getEmpresa() {
		return null;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<AcaoRecurso> getAcoes() {
		if (this.acoes == null) {
			this.acoes = new ArrayList<>();
		}
		return this.acoes;
	}

	public void setAcoes(List<AcaoRecurso> acoes) {
		this.acoes = acoes;
	}

	public Recurso getRecursoPai() {
		return this.recursoPai;
	}

	public void setRecursoPai(Recurso recursoPai) {
		this.recursoPai = recursoPai;
	}

	public List<Recurso> getFilhos() {
		return this.filhos;
	}

	public void setFilhos(List<Recurso> filhos) {
		this.filhos = filhos;
	}

	public Integer getOrdem() {
		return this.ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (this.getId() == null ? 0 : this.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Recurso)) {
			return false;
		}
		final Recurso other = (Recurso) obj;
		if (this.getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!this.getId().equals(other.getId())) {
			return false;
		}
		return true;
	}

	public String getIcone() {
		return this.icone;
	}

	public void setIcone(String icone) {
		this.icone = icone;
	}

	@JsonIgnore
	public boolean isAtivo() {
		return this.getStatus() == Status.ATIVO;
	}
}