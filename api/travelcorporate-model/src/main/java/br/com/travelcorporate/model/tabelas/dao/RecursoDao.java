package br.com.travelcorporate.model.tabelas.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.tabelas.entities.Recurso;

@Stateless
public class RecursoDao extends AbstractDao<Integer, Recurso> {

	public Recurso getByNome(String nome) {
		final String sql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e WHERE e.nome = :p_nome AND e.status = :p_status";

		final TypedQuery<Recurso> query = this.getManager().createQuery(sql, this.getVOClass());

		query.setParameter("p_nome", nome);
		query.setParameter("p_status", Status.ATIVO);

		final List<Recurso> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public List<Recurso> getAllRecursos() {
		final String sql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e WHERE e.recursoPai IS NULL AND e.status = :p_status ORDER BY e.ordem";

		final TypedQuery<Recurso> query = this.getManager().createQuery(sql, this.getVOClass());

		query.setParameter("p_status", Status.ATIVO);

		return query.getResultList();

	}

	public void removeNative(Recurso recurso) {
		String sql = "DELETE FROM recurso r WHERE r.id_recurso = " + recurso.getId() + " OR r.id_recurso_pai = " + recurso.getId();

		Query query = this.getManager().createNativeQuery(sql);

		int executeUpdate = query.executeUpdate();

		this.log.debug("Foram excluídos " + executeUpdate + " registros.");

		this.getManager().flush();
	}

}
