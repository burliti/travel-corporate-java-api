package br.com.travelcorporate.model.tabelas.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.tabelas.dao.CidadeDao;
import br.com.travelcorporate.model.tabelas.entities.Cidade;

@Stateless
public class CidadeService extends AbstractService<Integer, Cidade, CidadeDao> {

	@Inject
	private EstadoService estadoService;

	public Cidade getByNomeAndUf(String ufSigla, String nome) {
		return getDao().getByNomeAndUf(ufSigla, nome);
	}

	public void criarCidades() throws BusinessException {

		final InputStream is = this.getClass().getClassLoader().getResourceAsStream("br/com/travelcorporate/arquivos/cidades.csv");

		BufferedReader br = null;

		int contador = 0;

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				// Cria a cidade

				final String[] strings = line.split("\\|");

				final String ufSigla = strings[1];
				final String nome = strings[2];

				createIfNotExists(ufSigla, nome);

				contador++;

				if (contador % 100 == 0) {
					System.err.println("Importadas " + contador + " cidades.");
				}
			}

			System.err.println("Importadas " + contador + " cidades.");

		} catch (final IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void createIfNotExists(String ufSigla, String nome) throws BusinessException {

		Cidade cidade = getByNomeAndUf(ufSigla, nome);

		if (cidade == null) {
			cidade = new Cidade();
			cidade.setNome(nome);
			cidade.setEstado(estadoService.getBySigla(ufSigla));

			inserir(cidade);
		} else {

			cidade.setNome(nome);
			cidade.setEstado(estadoService.getBySigla(ufSigla));

			atualizar(cidade);
		}
	}
}
