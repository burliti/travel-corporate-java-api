package br.com.travelcorporate.model.cadastros.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.cadastros.entities.Funcao;
import br.com.travelcorporate.model.geral.enums.Status;

@Stateless
public class FuncaoDao extends AbstractDao<Integer, Funcao> {

	public List<Funcao> getAtivos() {

		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());
		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}
}