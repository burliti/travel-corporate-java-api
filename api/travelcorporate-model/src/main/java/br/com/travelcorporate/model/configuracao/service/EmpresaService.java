package br.com.travelcorporate.model.configuracao.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.vo.IEntity;
import br.com.travelcorporate.model.cadastros.service.CentroCustoService;
import br.com.travelcorporate.model.configuracao.dao.EmpresaDao;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Flag;

@Stateless
public class EmpresaService extends AbstractService<Integer, Empresa, EmpresaDao> {

	@Inject
	private CentroCustoService centroCustoService;

	public Empresa getByChave(String chave) {
		return this.getDao().getByChave(chave);
	}

	@Override
	public void doAntesInserir(Empresa vo) throws BusinessException {
		super.doAntesInserir(vo);
		vo.setPrincipal(Flag.NAO);
		vo.setChaveEmpresa("");
		vo.setEmpresaPrincipal((Empresa) this.getSessao().getEmpresa());
	}

	@Override
	public void doAntesAtualizar(Empresa vo) throws BusinessException {
		super.doAntesAtualizar(vo);

		vo.setEmpresaPrincipal(this.buscar(vo).getEmpresaPrincipal());
	}

	@Override
	public void doAntesSalvar(Empresa vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getCentroCusto() != null && vo.getCentroCusto().getId() != null) {
			vo.setCentroCusto(this.centroCustoService.buscar(vo.getCentroCusto()));
		} else {
			vo.setCentroCusto(null);
		}
	}

	/**
	 * Verifica se o registro é da empresa da sessão
	 *
	 * @param e
	 * @param empresa
	 * @return
	 */
	public boolean checkEmpresa(IEntity<?> e) {
		// Se a entidade for nula, o registro é novo.
		if (e == null) {
			return true;
		}

		// Se a empresa for nula, o registro é novo.
		if (e.getEmpresa() == null) {
			return true;
		}

		// Se for a mesa empresa, ok.
		if (e.getEmpresa().getId().equals(this.getSessao().getEmpresa().getId())) {
			return true;
		}

		return false;
	}

	public List<Empresa> getAtivos() {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}
}
