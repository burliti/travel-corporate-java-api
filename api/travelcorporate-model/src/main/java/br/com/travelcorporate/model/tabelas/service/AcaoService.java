package br.com.travelcorporate.model.tabelas.service;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.tabelas.dao.AcaoDao;
import br.com.travelcorporate.model.tabelas.entities.Acao;

@Stateless
public class AcaoService extends AbstractService<Integer, Acao, AcaoDao> {

	public static final String CONSULTAR = "consultar";
	public static final String VISUALIZAR = "visualizar";
	public static final String EXCLUIR = "excluir";
	public static final String ALTERAR = "alterar";
	public static final String INSERIR = "inserir";
	public static final String EXECUTAR = "executar";
	public static final String[] ACOES_CADASTRO_PADRAO = new String[] { INSERIR, ALTERAR, EXCLUIR, VISUALIZAR, CONSULTAR };
	public static final String[] ACAO_EXECUTAR = new String[] { EXECUTAR };

	public Acao getByNome(String nome) {
		return this.getDao().getByNome(nome);
	}

	public void createAcoes() throws BusinessException {
		this.createAcaoIfNotExists(CONSULTAR, "Consultar");
		this.createAcaoIfNotExists(VISUALIZAR, "Visualizar");
		this.createAcaoIfNotExists(EXCLUIR, "Excluir");
		this.createAcaoIfNotExists(ALTERAR, "Alterar");
		this.createAcaoIfNotExists(INSERIR, "Inserir");
		this.createAcaoIfNotExists(EXECUTAR, "Executar");
	}

	public void createAcaoIfNotExists(String nome, String descricao) throws BusinessException {
		Acao acao = this.getByNome(nome);
		if (acao == null) {
			acao = new Acao();
		}

		acao.setNome(nome);
		acao.setDescricao(descricao);
		acao.setStatus(Status.ATIVO);

		if (acao.getId() == null) {
			this.inserir(acao);
		} else {
			this.atualizar(acao);
		}
	}
}
