package br.com.travelcorporate.model.mensageria.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.StatusMensagem;
import br.com.travelcorporate.model.geral.enums.StatusMensagemDestinatario;
import br.com.travelcorporate.model.geral.enums.TipoExclusaoMensagem;
import br.com.travelcorporate.model.geral.exceptions.messages.MensagemException;
import br.com.travelcorporate.model.mensageria.dao.MensagemDao;
import br.com.travelcorporate.model.mensageria.dto.MensagemDTO;
import br.com.travelcorporate.model.mensageria.dto.MensagemDestinatarioDTO;
import br.com.travelcorporate.model.mensageria.entities.Mensagem;
import br.com.travelcorporate.model.mensageria.entities.MensagemDestinatario;

@Stateless
public class MinhasMensagensService extends AbstractService<Integer, Mensagem, MensagemDao> {

	@Inject
	private MensagemService mensagemService;

	@Inject
	private AssuntoService assuntoService;

	@Inject
	private MensagemDestinatarioService mensagemDestinatarioService;

	@Inject
	private MensagemFormaNotificacaoService mensagemFormaNotificacaoService;

	public Mensagem salvarRascunho(Mensagem mensagem) throws BusinessException {
		Mensagem buscar = this.buscar(mensagem);

		mensagem = this.validarMensagem(mensagem);

		if (buscar == null) {
			mensagem.setStatus(StatusMensagem.RASCUNHO);
			return this.inserir(mensagem);
		} else {
			if (mensagem.getStatus() == StatusMensagem.RASCUNHO) {
				return this.atualizar(mensagem);
			} else {
				throw new MensagemException("A mensagem não pode ser alterada!");
			}
		}
	}

	public Mensagem remover(Integer id, TipoExclusaoMensagem tipo) throws BusinessException {
		Mensagem buscar = this.buscar(id);

		if (buscar != null) {
			return this.remover(buscar, tipo);
		}

		return null;
	}

	public Mensagem remover(Mensagem vo, TipoExclusaoMensagem tipo) throws BusinessException {
		vo = this.buscar(vo);

		if (vo.getAutor().getId().equals(this.getSessao().getFuncionario().getId()) && tipo != TipoExclusaoMensagem.DESTINATARIO || tipo == TipoExclusaoMensagem.AUTOR) {

			if (vo.getStatus() == StatusMensagem.RASCUNHO) {
				return super.remover(vo);
			}

			vo.setStatus(StatusMensagem.EXCLUIDA);

			return super.atualizar(vo);
		}

		// @formatter:off
		Optional<MensagemDestinatario> first = vo.getDestinatarios()
				.stream()
				.filter((d) -> d.getDestinatario().getId().equals(this.getSessao().getFuncionario().getId()))
				.findFirst();

		// @formatter:on
		if (first.isPresent()) {
			MensagemDestinatario destinatario = first.get();
			destinatario.setStatus(StatusMensagemDestinatario.EXCLUIDA);

			return super.atualizar(vo);
		}

		return null;
	}

	public Mensagem enviar(Mensagem mensagem) throws BusinessException {
		Mensagem buscar = this.buscar(mensagem);

		mensagem = this.validarMensagem(mensagem);

		if (mensagem.getDestinatarios().size() <= 0) {
			throw new MensagemException("Informe pelo menos um destinatário!");
		}

		if (buscar == null) {
			mensagem.setStatus(StatusMensagem.ENVIADA);
			return this.inserir(mensagem);
		} else {
			if (buscar.getStatus() != StatusMensagem.ENVIADA) {
				mensagem.setStatus(StatusMensagem.ENVIADA);
				return this.atualizar(mensagem);
			} else {
				throw new MensagemException("A mensagem não pode ser alterada!");
			}
		}
	}

	public Mensagem validarMensagem(Mensagem mensagem) {

		if (mensagem.getAssunto() != null && mensagem.getAssunto().getId() == null) {
			mensagem.setAssunto(null);
		}

		if (mensagem.getMensagemSistema() == null) {
			mensagem.setMensagemSistema(Flag.NAO);
		}

		if (mensagem.getPermiteEncaminhar() == null) {
			mensagem.setPermiteEncaminhar(Flag.SIM);
		}

		if (mensagem.getPermiteResponder() == null) {
			mensagem.setPermiteResponder(Flag.SIM);
		}

		if (mensagem.getBloqueiaSemResponder() == null) {
			mensagem.setBloqueiaSemResponder(Flag.NAO);
		}

		if (mensagem.getExigeConfirmacao() == null) {
			mensagem.setExigeConfirmacao(Flag.NAO);
		}

		return mensagem;
	}

	@Override
	public void doAntesInserir(Mensagem vo) throws BusinessException {
		vo.setDataHoraCriacao(Calendar.getInstance());
		vo.setAutor((Funcionario) this.getSessao().getFuncionario());

		super.doAntesInserir(vo);
	}

	@Override
	public void doAntesSalvar(Mensagem vo) throws BusinessException {
		this.validarMensagem(vo);

		this.mensagemService.doAntesSalvar(vo);

		super.doAntesSalvar(vo);
	}

	@Override
	public List<Mensagem> consultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		return null;
	}

	public List<Mensagem> recebidas(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		return this.getDao().recebidas(this.getSessao().getEmpresa(), this.getSessao().getFuncionario(), filtros, pageSize, pageNumber, order);
	}

	public List<Mensagem> enviadas(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		return this.getDao().enviadas(this.getSessao().getEmpresa(), this.getSessao().getFuncionario(), filtros, pageSize, pageNumber, order);
	}

	public List<Mensagem> rascunhos(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		return this.getDao().rascunhos(this.getSessao().getEmpresa(), this.getSessao().getFuncionario(), filtros, pageSize, pageNumber, order);
	}

	public Integer totalRecebidas(Map<String, String> filtros) {
		return this.getDao().totalRecebidas(this.getSessao().getEmpresa(), this.getSessao().getFuncionario(), filtros);
	}

	public Integer totalEnviadas(HashMap<String, String> filtros) {
		return this.getDao().totalEnviadas(this.getSessao().getEmpresa(), this.getSessao().getFuncionario(), filtros);
	}

	public Integer totalRascunhos(HashMap<String, String> filtros) {
		return this.getDao().totalRascunhos(this.getSessao().getEmpresa(), this.getSessao().getFuncionario(), filtros);
	}

	public MensagemDTO convertToDTO(Mensagem mensagem) {
		MensagemDTO dto = new MensagemDTO();
		dto.setId(mensagem.getId());
		dto.setAssuntoDescricao(mensagem.getAssuntoDescricao());
		dto.setTextoMensagem(mensagem.getTextoMensagem());
		dto.setAutor(new FuncionarioDTO());
		dto.getAutor().setNome(mensagem.getAutor().getNome());
		dto.setDataHoraCriacao(mensagem.getDataHoraCriacao());
		dto.setStatus(mensagem.getStatus());

		//
		mensagem.getDestinatarios().stream().filter((d) -> d.getDestinatario().getId().equals(this.getSessao().getFuncionario().getId())).forEach((d) -> {
			MensagemDestinatarioDTO destinatario = new MensagemDestinatarioDTO();
			destinatario.setDestinatario(new FuncionarioDTO());
			destinatario.getDestinatario().setId(this.getSessao().getFuncionario().getId());
			destinatario.setStatus(d.getStatus());
			dto.getDestinatarios().add(destinatario);
		});

		return dto;
	}

	public MensagemDTO convertToDTOFull(Mensagem mensagem) {
		MensagemDTO dto = new MensagemDTO();
		dto.setId(mensagem.getId());
		dto.setAssunto(this.assuntoService.convertDTO(mensagem.getAssunto()));
		dto.setAssuntoDescricao(mensagem.getAssuntoDescricao());
		dto.setBloqueiaSemResponder(mensagem.getBloqueiaSemResponder());
		dto.setDataHoraCriacao(mensagem.getDataHoraCriacao());
		dto.setDataHoraEnvio(mensagem.getDataHoraEnvio());
		dto.setDataHoraExclusao(mensagem.getDataHoraExclusao());
		dto.setExigeConfirmacao(mensagem.getExigeConfirmacao());
		dto.setMensagemSistema(mensagem.getMensagemSistema());
		dto.setPermiteEncaminhar(mensagem.getPermiteEncaminhar());
		dto.setPermiteResponder(mensagem.getPermiteResponder());
		dto.setStatus(mensagem.getStatus());
		dto.setTextoBotaoConfirmacao(mensagem.getTextoBotaoConfirmacao());
		dto.setTextoMensagem(mensagem.getTextoMensagem());

		if (mensagem.getMensagemOrigemEncaminhada() != null) {
			dto.setMensagemOrigemEncaminhada(new MensagemDTO());
			dto.getMensagemOrigemEncaminhada().setId(mensagem.getMensagemOrigemEncaminhada().getId());
		}

		if (mensagem.getMensagemOrigemResposta() != null) {
			dto.setMensagemOrigemResposta(new MensagemDTO());
			dto.getMensagemOrigemResposta().setId(mensagem.getMensagemOrigemResposta().getId());
		}

		// Destinatarios
		mensagem.getDestinatarios().forEach((d) -> dto.getDestinatarios().add(this.mensagemDestinatarioService.convertDTO(d)));

		// Formas notificacao
		mensagem.getFormasNotificacao().forEach((f) -> dto.getFormasNotificacao().add(this.mensagemFormaNotificacaoService.convertDTO(f)));

		dto.setAutor(new FuncionarioDTO());
		dto.getAutor().setId(mensagem.getAutor().getId());
		dto.getAutor().setNome(mensagem.getAutor().getNome());

		return dto;
	}

	public void marcarLida(Integer id) throws BusinessException {
		Mensagem mensagem = this.buscar(id);

		// @formatter:off
		mensagem.getDestinatarios()
			.stream()
			.filter((d) -> d.getDestinatario().getId().equals(this.getSessao().getFuncionario().getId()))
			.forEach((d) -> d.setStatus(StatusMensagemDestinatario.LIDA));
		// @formatter:on

		this.atualizar(mensagem);
	}
}
