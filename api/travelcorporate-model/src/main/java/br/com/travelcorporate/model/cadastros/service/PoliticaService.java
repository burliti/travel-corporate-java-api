package br.com.travelcorporate.model.cadastros.service;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.cadastros.dao.PoliticaDao;
import br.com.travelcorporate.model.cadastros.entities.Politica;

@Stateless
public class PoliticaService extends AbstractService<Integer, Politica, PoliticaDao> {

}
