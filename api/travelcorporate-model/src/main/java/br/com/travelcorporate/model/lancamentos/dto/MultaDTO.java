package br.com.travelcorporate.model.lancamentos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.geral.enums.TipoVeiculo;
import br.com.travelcorporate.model.tabelas.entities.dto.CidadeDTO;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MultaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String numeroMulta;

	private CidadeDTO cidade;

	private String nomeCidade;

	private String uf;

	private TipoVeiculo tipoVeiculo;

	private Calendar dataHora;

	private String codigoMulta;

	private String textoMulta;

	private BigDecimal valorMulta;

	private FuncionarioDTO funcionario;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumeroMulta() {
		return this.numeroMulta;
	}

	public void setNumeroMulta(String numeroMulta) {
		this.numeroMulta = numeroMulta;
	}

	public CidadeDTO getCidade() {
		return this.cidade;
	}

	public void setCidade(CidadeDTO cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return this.uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public TipoVeiculo getTipoVeiculo() {
		return this.tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Calendar getDataHora() {
		return this.dataHora;
	}

	@JsonGetter("dataHora")
	public String getDataHoraString() {
		return FormatUtils.formatDateTimeSemSegundos(this.getDataHora());
	}

	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}

	public String getCodigoMulta() {
		return this.codigoMulta;
	}

	public void setCodigoMulta(String codigoMulta) {
		this.codigoMulta = codigoMulta;
	}

	public String getTextoMulta() {
		return this.textoMulta;
	}

	public void setTextoMulta(String textoMulta) {
		this.textoMulta = textoMulta;
	}

	public BigDecimal getValorMulta() {
		return this.valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public FuncionarioDTO getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(FuncionarioDTO funcionario) {
		this.funcionario = funcionario;
	}

	public String getNomeCidade() {
		return this.nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}

}
