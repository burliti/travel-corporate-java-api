package br.com.travelcorporate.model.cadastros.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dao.ClienteDao;
import br.com.travelcorporate.model.cadastros.entities.Cliente;
import br.com.travelcorporate.model.geral.enums.Status;

@Stateless
public class ClienteService extends AbstractService<Integer, Cliente, ClienteDao> {

	@Inject
	private CentroCustoService centroCustoService;

	public List<Cliente> getAtivos() {
		Map<String, String> filtros = new HashMap<>();
		Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());
		order.put("nome", "asc");

		return this.consultar(filtros, null, null, order);
	}

	@Override
	public void doAntesSalvar(Cliente vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getCentroCusto() != null && vo.getCentroCusto().getId() != null) {
			vo.setCentroCusto(this.centroCustoService.buscar(vo.getCentroCusto()));
		} else {
			vo.setCentroCusto(null);
		}
	}

}
