package br.com.travelcorporate.model.geral.exceptions;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;

public class UsuarioDuplicadoException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public UsuarioDuplicadoException(String message) {
		super(message);
	}

	public UsuarioDuplicadoException(Funcionario usuario) {
		super("O usuário '" + usuario.getNome() + "' já existe e ainda se encontra ativo!");
	}
}