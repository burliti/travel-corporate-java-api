package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.TipoPeriodo;

@Converter(autoApply = true)
public class TipoPeriodoConverter implements AttributeConverter<TipoPeriodo, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoPeriodo tipoPeriodo) {
		if (tipoPeriodo == null) {
			return null;
		}
		return tipoPeriodo.getValor();
	}

	@Override
	public TipoPeriodo convertToEntityAttribute(Integer value) {
		if (value == null) {
			return null;
		}
		return TipoPeriodo.fromValue(value.toString());
	}

}
