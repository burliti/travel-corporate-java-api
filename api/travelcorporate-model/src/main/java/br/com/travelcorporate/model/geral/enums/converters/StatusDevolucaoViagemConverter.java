package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.StatusDevolucaoViagem;

@Converter(autoApply = true)
public class StatusDevolucaoViagemConverter implements AttributeConverter<StatusDevolucaoViagem, Integer> {

	@Override
	public Integer convertToDatabaseColumn(StatusDevolucaoViagem status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public StatusDevolucaoViagem convertToEntityAttribute(Integer source) {
		if (source != null) {
			return StatusDevolucaoViagem.fromValue(source.toString());
		}
		return null;
	}
}