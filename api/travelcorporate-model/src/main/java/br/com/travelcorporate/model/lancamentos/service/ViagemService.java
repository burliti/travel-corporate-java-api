package br.com.travelcorporate.model.lancamentos.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.service.ClienteService;
import br.com.travelcorporate.model.cadastros.service.ProjetoService;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;
import br.com.travelcorporate.model.geral.exceptions.viagem.ViagemException;
import br.com.travelcorporate.model.lancamentos.dao.ViagemDao;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class ViagemService extends AbstractService<Integer, Viagem, ViagemDao> {

	@Inject
	private ClienteService clienteService;

	@Inject
	private ProjetoService projetoService;

	@Inject
	private LancamentoService lancamentoService;

	@Override
	public void doAntesSalvar(Viagem vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getCliente() != null && vo.getCliente().getId() != null) {
			vo.setCliente(this.clienteService.buscar(vo.getCliente()));
		} else {
			vo.setCliente(null);
		}

		if (vo.getProjeto() != null && vo.getProjeto().getId() != null) {
			vo.setProjeto(this.projetoService.buscar(vo.getProjeto()));
		} else {
			vo.setProjeto(null);
		}

		if (vo.getLancamentos() == null) {
			vo.setLancamentos(new ArrayList<>());
		}

		if (vo.getDataIda() != null && vo.getDataVolta() != null) {
			if (vo.getDataVolta().before(vo.getDataIda())) {
				throw new ViagemException("Data de volta não pode ser anterior à data de ida!");
			}
		}

		vo.getLancamentos().sort((o1, o2) -> {
			if (o1.getSequencial() == null) {
				o1.setSequencial(0);
			}
			if (o2.getSequencial() == null) {
				o2.setSequencial(0);
			}
			return o1.getSequencial().compareTo(o2.getSequencial());
		});

		AtomicReference<Integer> i = new AtomicReference<>(0);

		vo.getLancamentos().forEach(l -> {
			i.set(i.get() + 1);
			l.setSequencial(i.get());
		});

		// Busca lancamentos em aberto para inserir adiantamento
		if (vo.isAberto()) {
			List<Lancamento> semViagem = this.lancamentoService.getLancamentosSemViagem(this.getSessao().getFuncionario());

			int sequencial = 0;
			if (vo.getLancamentos().size() > 0) {
				vo.getLancamentos().sort((o1, o2) -> o2.getSequencial().compareTo(o1.getSequencial()));
				sequencial = vo.getLancamentos().get(0).getSequencial();
			}

			for (Lancamento lancamento : semViagem) {
				sequencial++;
				vo.getLancamentos().add(lancamento);
				lancamento.setSequencial(sequencial);
				lancamento.setViagem(vo);

				if (lancamento.getCategoria().getInterna().isSim()) {
					vo.getFechamento().setSaldoAnterior(vo.getFechamento().getSaldoAnterior().add(lancamento.getValorLancamentoM()));
				}
			}
		}

		if (vo.getStatus() == StatusViagem.ANDAMENTO || vo.getStatus() == StatusViagem.REJEITADA || vo.getStatus() == StatusViagem.RECEBIDO_FINANCEIRO) {
			this.calcularSaldos(vo);
		}

	}

	public void calcularSaldos(Viagem vo) {
		if (vo.getFechamento() == null) {
			vo.setFechamento(new Fechamento());
			vo.getFechamento().setSaldoAnterior(BigDecimal.ZERO);
			vo.getFechamento().setSaldoFinal(BigDecimal.ZERO);
		}

		vo.getFechamento().setTotalCreditos(BigDecimal.ZERO);
		vo.getFechamento().setTotalDebitos(BigDecimal.ZERO);

		vo.getLancamentos().forEach((lancamento) -> {
			if (lancamento.getTipoLancamento() == TipoLancamento.CREDITO) {
				vo.getFechamento().setTotalCreditos(vo.getFechamento().getTotalCreditos().add(lancamento.getValorLancamento()));
			} else if (lancamento.getTipoLancamento() == TipoLancamento.DEBITO) {
				vo.getFechamento().setTotalDebitos(vo.getFechamento().getTotalDebitos().add(lancamento.getValorLancamento()));
			}
		});

		// Calcula saldo final
		vo.getFechamento().setSaldoFinal(vo.getFechamento().getTotalCreditos().subtract(vo.getFechamento().getTotalDebitos()));
	}

	public Viagem getUltimaViagemAberta(IFuncionario funcionario) {
		return this.getDao().getUltimaViagemAberta(funcionario);
	}

	public Viagem finalizar(Viagem vo) throws BusinessException {
		if (vo.getLancamentos() == null || vo.getLancamentos().size() <= 0) {
			throw new BusinessException("Nenhum lançamento para este relatório de viagem!");
		}

		vo.setStatus(StatusViagem.FINALIZADA);
		vo.getFechamento().setDataHoraEnviado(Calendar.getInstance());

		return this.atualizar(vo);
	}

	public List<Viagem> getViagensUsuarioSessao(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		if (this.getSessao() == null) {
			return null;
		}
		return this.getViagensFuncionario((Funcionario) this.getSessao().getFuncionario(), filtros, pageSize, pageNumber, order);
	}

	public List<Viagem> getViagensFuncionario(Funcionario f, Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		return this.getDao().getViagensByFuncionario(f, filtros, pageSize, pageNumber, order);
	}

	public Integer getTotalViagensFuncionario(Funcionario f, Map<String, String> filtros) {
		return this.getDao().totalViagensPorFuncionario(f, filtros);
	}

	public Integer getTotalViagensFuncionarioSessao(Map<String, String> filtros) {
		if (this.getSessao() == null) {
			return null;
		}
		return this.getTotalViagensFuncionario((Funcionario) this.getSessao().getFuncionario(), filtros);
	}

	public Integer getNextSequencialEmpresa(IEmpresa empresa) {
		return this.getDao().getNextSequencialEmpresa(empresa);
	}

	public Integer getNextSequencialEmpresa() {
		return this.getDao().getNextSequencialEmpresa(this.getSessao().getEmpresa());
	}

	public Viagem getUltimaViagemAprovada(IFuncionario funcionario) {
		return this.getDao().getUltimaViagemAprovada(funcionario);
	}

	public Viagem getUltimaViagemAprovadaSessao() {
		if (this.getSessao() != null) {
			return this.getUltimaViagemAprovada(this.getSessao().getFuncionario());
		} else {
			return null;
		}
	}

	public Viagem getUltimaViagemAbertaSessao() {
		if (this.getSessao() != null) {
			return this.getUltimaViagemAberta(this.getSessao().getFuncionario());
		} else {
			return null;
		}
	}
}
