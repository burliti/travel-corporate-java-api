package br.com.travelcorporate.model.lancamentos.dao;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.lancamentos.entities.Recibo;

@Stateless
public class ReciboDao extends AbstractDao<Integer, Recibo> {

}
