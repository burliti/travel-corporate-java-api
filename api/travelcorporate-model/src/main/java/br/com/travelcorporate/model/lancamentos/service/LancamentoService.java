package br.com.travelcorporate.model.lancamentos.service;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.configuracao.service.CategoriaService;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.TipoPeriodo;
import br.com.travelcorporate.model.geral.exceptions.RegistroInvalidoException;
import br.com.travelcorporate.model.geral.exceptions.financeiro.LancamentoException;
import br.com.travelcorporate.model.lancamentos.dao.LancamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Recibo;

@Stateless
public class LancamentoService extends AbstractService<Integer, Lancamento, LancamentoDao> {

	@Inject
	private CategoriaService categoriaService;

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private ViagemService viagemService;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private ReciboService reciboService;

	@Override
	public void doAntesInserir(Lancamento vo) throws BusinessException {
		super.doAntesInserir(vo);

		vo.setDataHoraLancamento(Calendar.getInstance());
	}

	@Override
	public void doAntesSalvar(Lancamento vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getGlosado() == null) {
			vo.setGlosado(Flag.NAO);
		}

		if (vo.getEmpresa() == null) {
			vo.setEmpresa(this.getSessao().getEmpresa());
		}

		if (vo.getTipoPeriodo() == null) {
			vo.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		}

		if (vo.getFuncionario() == null || vo.getFuncionario().getId() == null) {
			vo.setFuncionario((Funcionario) this.getSessao().getFuncionario());
		}

		if (vo.getFuncionarioLancamento() == null || vo.getFuncionarioLancamento().getId() == null) {
			vo.setFuncionarioLancamento((Funcionario) this.getSessao().getFuncionario());
		}

		if (!vo.getFuncionarioLancamento().equals(this.getSessao().getFuncionario())) {
			throw new LancamentoException("O Lançamento só pode ser alterado pelo usuário que o cadastrou!");
		}

		if (vo.getCategoria() != null) {
			if (vo.getCategoria().getId() != null) {
				vo.setCategoria(this.categoriaService.buscar(vo.getCategoria()));
				vo.setTipoLancamento(vo.getCategoria().getTipo());
			} else {
				vo.setCategoria(null);
			}
		}

		if (vo.getFuncionario() != null) {
			if (vo.getFuncionario().getId() != null) {
				vo.setFuncionario(this.funcionarioService.buscar(vo.getFuncionario()));
			} else {
				vo.setFuncionario(null);
			}
		}

		if (vo.getViagem() != null) {
			if (vo.getViagem().getId() != null) {
				vo.setViagem(this.viagemService.buscar(vo.getViagem()));
			} else {
				vo.setViagem(null);
			}
		}

		Lancamento buscar = this.buscar(vo);

		if (buscar != null) {
			vo.setDataHoraLancamento(buscar.getDataHoraLancamento());
		} else {
			vo.setDataHoraLancamento(Calendar.getInstance());
		}

		if (vo.getRecibo() != null) {
			if (vo.getRecibo().getId() != null) {
				// Verifica se o recibo é desta empresa
				final Recibo recibo = this.reciboService.buscar(vo.getRecibo());
				if (recibo != null && recibo.getId() != null) {
					if (!this.empresaService.checkEmpresa(recibo)) {
						vo.setRecibo(null);
						throw new RegistroInvalidoException("Recibo inexistente!");
					}
				} else {
					vo.setRecibo(null);
				}

				vo.getRecibo().setEmpresa(recibo.getEmpresa());
				vo.getRecibo().setDataInserido(recibo.getDataInserido());
				vo.getRecibo().setFuncionario(recibo.getFuncionario());

				if (vo.getRecibo().getValor() == null) {
					vo.getRecibo().setValor(vo.getValorLancamento());
				}

				this.reciboService.atualizar(vo.getRecibo());
			} else {
				this.reciboService.inserir(vo.getRecibo());
			}
		}

		if (vo.getTipoPeriodo() == TipoPeriodo.DATA_REFERENCIA) {
			if (vo.getDataInicial() == null) {
				throw new BusinessException("Data de referência do lançamento " + String.format("%04d", vo.getSequencial()) + " é obrigatória!");
			}
		} else if (vo.getTipoPeriodo() == TipoPeriodo.PERIODO) {
			if (vo.getDataInicial() == null) {
				throw new BusinessException("Data inicial do lançamento " + String.format("%04d", vo.getSequencial()) + " é obrigatória!");
			}

			if (vo.getDataFinal() == null) {
				throw new BusinessException("Data final do lançamento " + String.format("%04d", vo.getSequencial()) + " é obrigatória!");
			}
		}

		// Validacao dos outros campos
		if (vo.getCategoria() == null) {
			throw new BusinessException("Categoria do lançamento " + String.format("%04d", vo.getSequencial()) + " é obrigatória!");
		}

		if (vo.getValorLancamento() == null) {
			throw new BusinessException("Valor do lançamento " + String.format("%04d", vo.getSequencial()) + " é obrigatória!");
		}
	}

	public List<Lancamento> getLancamentosNaoFechados(Fechamento fechamento) {
		return this.getDao().getLancamentosNaoFechados(this.getSessao().getFuncionario(), fechamento);
	}

	public List<Lancamento> getLancamentosSemViagem(IFuncionario funcionario) {
		return this.getDao().getLancamentosSemViagem(funcionario);
	}
}
