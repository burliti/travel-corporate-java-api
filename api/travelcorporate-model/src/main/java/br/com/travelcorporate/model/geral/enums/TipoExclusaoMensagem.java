package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoExclusaoMensagem {

	AUTOR(1, "Autor"), DESTINATARIO(2, "Destinatario");

	private String descricao;
	private Integer valor;

	TipoExclusaoMensagem(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoExclusaoMensagem fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final TipoExclusaoMensagem tipoPessoa : TipoExclusaoMensagem.values()) {
			if (tipoPessoa.getValor().toString().equals(source.toUpperCase())) {
				return tipoPessoa;
			} else if (tipoPessoa.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return tipoPessoa;
			} else if (tipoPessoa.toString().equalsIgnoreCase(source)) {
				return tipoPessoa;
			}
		}
		return null;
	}

	@JsonCreator
	public static TipoExclusaoMensagem fromString(String source) {
		return fromValue(source);
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
