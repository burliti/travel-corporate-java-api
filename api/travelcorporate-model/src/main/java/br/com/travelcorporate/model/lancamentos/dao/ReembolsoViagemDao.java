package br.com.travelcorporate.model.lancamentos.dao;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.lancamentos.entities.ReembolsoViagem;

@Stateless
public class ReembolsoViagemDao extends AbstractDao<Integer, ReembolsoViagem> {

	public Integer getNextSequencialEmpresa(IEmpresa empresa) {
		Integer retorno = 0;

		String hql = "SELECT MAX(e.sequencial) FROM " + this.getVOClass().getSimpleName() + " e \n";
		hql += " WHERE 1 = 1 AND e.empresa.id = :p_empresa";

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_empresa", empresa.getId());

		Number result = query.getSingleResult();
		retorno = result != null ? result.intValue() : 0;

		return retorno + 1;
	}

}
