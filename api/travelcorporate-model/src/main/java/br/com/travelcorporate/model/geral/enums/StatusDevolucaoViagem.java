package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusDevolucaoViagem {
	EM_ABERTO(1, "Em aberto"), EM_ANALISE(2, "Em análise"), PAGO(3, "Pago"), REJEITADO(9, "Cancelado");

	private String descricao;
	private Integer valor;

	StatusDevolucaoViagem(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static StatusDevolucaoViagem fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final StatusDevolucaoViagem status : StatusDevolucaoViagem.values()) {
			if (status.getValor().toString().equals(source.toUpperCase())) {
				return status;
			} else if (status.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return status;
			} else if (status.toString().equalsIgnoreCase(source)) {
				return status;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
