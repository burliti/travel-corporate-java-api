package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.StatusFechamento;

@Converter(autoApply = true)
public class StatusFechamentoConverter implements AttributeConverter<StatusFechamento, Integer> {

	@Override
	public Integer convertToDatabaseColumn(StatusFechamento status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public StatusFechamento convertToEntityAttribute(Integer source) {
		if (source == null) {
			return null;
		}
		return StatusFechamento.fromValue(source.toString());
	}
}