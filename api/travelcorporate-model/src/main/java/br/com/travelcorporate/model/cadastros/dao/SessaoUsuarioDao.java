package br.com.travelcorporate.model.cadastros.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.FlushModeType;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuario;

@Stateless
public class SessaoUsuarioDao extends AbstractDao<Integer, SessaoUsuario> {

	public SessaoUsuario getByToken(String token) {
		final String sql = "SELECT e FROM " + SessaoUsuario.class.getSimpleName() + " e WHERE e.chaveSessao = :token";

		final TypedQuery<SessaoUsuario> query = this.getManager().createQuery(sql, SessaoUsuario.class);
		query.setFlushMode(FlushModeType.COMMIT);
		query.setParameter("token", token);

		final List<SessaoUsuario> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}
}
