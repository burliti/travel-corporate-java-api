package br.com.travelcorporate.model.lancamentos.entities;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.TipoVeiculo;
import br.com.travelcorporate.model.tabelas.entities.Cidade;

@Entity
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class Multa extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_multa", sequenceName = "seq_multa", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_multa")
	@Column(name = "id_multa")
	private Integer id;

	@Column(name = "numero")
	@NotEmpty(message = "Número da multa é obrigatório")
	private String numeroMulta;

	@Column(name = "cidade_ocorrencia")
	@NotEmpty(message = "Cidade da multa é obrigatório")
	private String nomeCidade;

	@ManyToOne
	@JoinColumn(name = "id_cidade")
	private Cidade cidade;

	@Column(name = "uf_multa")
	@NotEmpty(message = "UF da multa é obrigatório")
	private String uf;

	@Column(name = "tipo_veiculo")
	@NotNull(message = "Tipo de Veículo é obrigatório")
	private TipoVeiculo tipoVeiculo;

	@Column(name = "data_hora_multa")
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull(message = "Data / Hora é obrigatória")
	private Calendar dataHora;

	@Column(name = "codigo_multa")
	private String codigoMulta;

	@Column(name = "texto_multa")
	private String textoMulta;

	@Column(name = "valor_multa")
	@NotNull(message = "Valor da multa é obrigatório")
	private BigDecimal valorMulta;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_multa")
	private Funcionario funcionario;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_cadastro")
	@NotNull(message = "Funcionário de cadastro é obrigatório")
	private Funcionario funcionarioCadastro;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public String getNumeroMulta() {
		return this.numeroMulta;
	}

	public void setNumeroMulta(String numeroMulta) {
		this.numeroMulta = numeroMulta;
	}

	public String getUf() {
		return this.uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Calendar getDataHora() {
		return this.dataHora;
	}

	@JsonGetter("dataHora")
	public String getDataHoraString() {
		return FormatUtils.formatDateTimeSemSegundos(this.getDataHora());
	}

	public void setDataHora(Calendar dataHora) {
		this.dataHora = dataHora;
	}

	@JsonSetter("dataHora")
	public void setDataHora(String dataHora) {
		this.dataHora = FormatUtils.parseDateTimeSemSegundos(dataHora);
	}

	public String getCodigoMulta() {
		return this.codigoMulta;
	}

	public void setCodigoMulta(String codigoMulta) {
		this.codigoMulta = codigoMulta;
	}

	public String getTextoMulta() {
		return this.textoMulta;
	}

	public void setTextoMulta(String textoMulta) {
		this.textoMulta = textoMulta;
	}

	public BigDecimal getValorMulta() {
		return this.valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	@JsonSetter("valorMulta")
	public void setValorMulta(String valorMulta) {
		this.valorMulta = FormatUtils.parseNumber(valorMulta);
	}

	public TipoVeiculo getTipoVeiculo() {
		return this.tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Funcionario getFuncionarioCadastro() {
		return this.funcionarioCadastro;
	}

	public void setFuncionarioCadastro(Funcionario funcionarioCadastro) {
		this.funcionarioCadastro = funcionarioCadastro;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public String getNomeCidade() {
		return nomeCidade;
	}

	public void setNomeCidade(String nomeCidade) {
		this.nomeCidade = nomeCidade;
	}
}