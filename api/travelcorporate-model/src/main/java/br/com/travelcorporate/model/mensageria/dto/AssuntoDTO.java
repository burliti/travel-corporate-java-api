package br.com.travelcorporate.model.mensageria.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.model.geral.enums.Flag;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AssuntoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String nome;

	private Flag permiteResponder;

	private Flag permiteEncaminhar;

	private Flag exigeConfirmacao;

	private Flag bloqueiaSemResponder;

	private String textoBotaoConfirmacao;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Flag getPermiteResponder() {
		return this.permiteResponder;
	}

	public void setPermiteResponder(Flag permiteResponder) {
		this.permiteResponder = permiteResponder;
	}

	public Flag getPermiteEncaminhar() {
		return this.permiteEncaminhar;
	}

	public void setPermiteEncaminhar(Flag permiteEncaminhar) {
		this.permiteEncaminhar = permiteEncaminhar;
	}

	public Flag getExigeConfirmacao() {
		return this.exigeConfirmacao;
	}

	public void setExigeConfirmacao(Flag exigeConfirmacao) {
		this.exigeConfirmacao = exigeConfirmacao;
	}

	public Flag getBloqueiaSemResponder() {
		return this.bloqueiaSemResponder;
	}

	public void setBloqueiaSemResponder(Flag bloqueiaSemResponder) {
		this.bloqueiaSemResponder = bloqueiaSemResponder;
	}

	public String getTextoBotaoConfirmacao() {
		return this.textoBotaoConfirmacao;
	}

	public void setTextoBotaoConfirmacao(String textoBotaoConfirmacao) {
		this.textoBotaoConfirmacao = textoBotaoConfirmacao;
	}
}