package br.com.travelcorporate.model.configuracao.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.enums.SimNao;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.configuracao.dao.CategoriaDao;
import br.com.travelcorporate.model.configuracao.entities.Categoria;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;

@Stateless
public class CategoriaInternaService extends AbstractService<Integer, Categoria, CategoriaDao> {

	public static final String SALDO_VIAGEM_ANTERIOR = "Saldo viagem anterior";
	public static final String SALDO_DEVEDOR_VIAGEM_ANTERIOR = "Saldo devedor viagem anterior";

	public static final String SALDO_REMANESCENTE_GERADO_PROXIMA_VIAGEM = "Saldo remanescente gerado para outra viagem";
	public static final String SALDO_FALTANTE_GERADO_PROXIMA_VIAGEM = "Saldo faltante gerado para outra viagem";

	public static final String SALDO_REMANESCENTE = "Saldo remanescente da viagem";
	public static final String SALDO_NEGATIVO_REMANESCENTE = "Saldo negativo remanescente";

	public static final String DEVOLUCAO = "Devolução";
	public static final String REEMBOLSO = "Reembolso";

	@Override
	public void doAntesSalvar(Categoria vo) throws BusinessException {

		super.doAntesSalvar(vo);

		if (vo.getInterna() == null) {
			vo.setInterna(Flag.SIM);
		}
	}

	@Override
	public void doAntesConsultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		filtros.put("interna", SimNao.SIM.getValor());

		super.doAntesConsultar(filtros, pageSize, pageNumber, order);
	}

	/**
	 * Cria uma categoria interna
	 *
	 * @param nome
	 *            Nome da ca tegoria
	 * @param tipo
	 *            Tipo da categoria
	 * @return Objeto da categoria
	 * @throws BusinessException
	 */
	public Categoria criarCategoriaInterna(String nome, TipoLancamento tipo) throws BusinessException {
		Categoria c = new Categoria();

		c.setNome(nome);
		c.setTipo(tipo);
		c.setInterna(Flag.SIM);
		c.setStatus(Status.ATIVO);

		return this.inserir(c);
	}

	/**
	 * Retorna uma categoria INTERNA pelo nome
	 *
	 * @param nome
	 *            Nome da categoria
	 * @return Objeto da categoria interna
	 */
	public Categoria getByNome(String nome) {
		Map<String, String> filtros = new HashMap<>();

		filtros.put("nome", nome);
		filtros.put("status", Status.ATIVO.getValor());
		filtros.put("interna", Flag.SIM.getValor());

		Map<String, String> order = new HashMap<>();

		List<Categoria> list = this.consultar(filtros, null, null, order);

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Retorna a categoria interna, caso não exista cria.
	 *
	 * @param nome
	 *            Nome da categoria
	 * @param tipo
	 *            Tipo do lançamento
	 * @return Objeto da categoria interna
	 * @throws BusinessException
	 */
	public Categoria get(String nome, TipoLancamento tipo) throws BusinessException {
		Categoria categoria = this.getByNome(nome);

		if (categoria == null) {
			categoria = this.criarCategoriaInterna(nome, tipo);
		}

		return categoria;
	}

	public Categoria getDevolucao() throws BusinessException {
		return this.get(CategoriaInternaService.DEVOLUCAO, TipoLancamento.DEBITO);
	}

	public Categoria getReembolso() throws BusinessException {
		return this.get(CategoriaInternaService.REEMBOLSO, TipoLancamento.CREDITO);
	}

	public Categoria getSaldoDevedorViagemAnterior() throws BusinessException {
		return this.get(CategoriaInternaService.SALDO_DEVEDOR_VIAGEM_ANTERIOR, TipoLancamento.DEBITO);
	}

	public Categoria getSaldoViagemAnterior() throws BusinessException {
		return this.get(CategoriaInternaService.SALDO_VIAGEM_ANTERIOR, TipoLancamento.CREDITO);
	}

	public Categoria getSaldoRemanescenteGeradoProximaViagem() throws BusinessException {
		return this.get(CategoriaInternaService.SALDO_REMANESCENTE_GERADO_PROXIMA_VIAGEM, TipoLancamento.DEBITO);
	}

	public Categoria getSaldoFaltanteGeradoProximaViagem() throws BusinessException {
		return this.get(CategoriaInternaService.SALDO_FALTANTE_GERADO_PROXIMA_VIAGEM, TipoLancamento.CREDITO);
	}

	public Categoria getSaldoRemanescente() throws BusinessException {
		return this.get(CategoriaInternaService.SALDO_REMANESCENTE, TipoLancamento.DEBITO);
	}

	public Categoria getSaldoNegativoRemanescente() throws BusinessException {
		return this.get(CategoriaInternaService.SALDO_NEGATIVO_REMANESCENTE, TipoLancamento.CREDITO);
	}

}
