package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import br.com.travelcorporate.core.utils.FormatUtils;

public enum StatusMensagemDestinatario {

	NAO_LIDA(1, "Não lida"), LIDA(2, "Lida"), EXCLUIDA(9, "Excluída");

	private String descricao;
	private Integer valor;

	StatusMensagemDestinatario(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static StatusMensagemDestinatario fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}

		for (final StatusMensagemDestinatario status : StatusMensagemDestinatario.values()) {
			if (status.getValor().equals(FormatUtils.parseInt(source))) {
				return status;
			} else if (status.getDescricao().equalsIgnoreCase(source)) {
				return status;
			} else if (status.toString().equalsIgnoreCase(source)) {
				return status;
			}
		}
		return null;
	}

	public static StatusMensagemDestinatario fromValue(Integer source) {
		return fromValue(source == null ? null : source.toString());
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
