package br.com.travelcorporate.model.mensageria.services;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.mensageria.dao.MensagemConfirmacaoLeituraDao;
import br.com.travelcorporate.model.mensageria.entities.MensagemConfirmacaoLeitura;

@Stateless
public class MensagemConfirmacaoLeituraService extends AbstractService<Integer, MensagemConfirmacaoLeitura, MensagemConfirmacaoLeituraDao> {

}
