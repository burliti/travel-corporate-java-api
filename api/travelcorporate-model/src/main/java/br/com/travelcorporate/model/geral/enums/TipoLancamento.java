package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoLancamento {

	CREDITO("C", "Crédito", 1), DEBITO("D", "Débito", -1);

	private String descricao;
	private String valor;
	private int multiplicador;

	TipoLancamento(String valor, String descricao, int multiplicador) {
		this.setValor(valor);
		this.setDescricao(descricao);
		this.setMultiplicador(multiplicador);
	}

	@JsonCreator
	public static TipoLancamento fromValue(String source) {
		for (final TipoLancamento tipoLancamento : TipoLancamento.values()) {
			if (tipoLancamento.getValor().toUpperCase().equals(source.toUpperCase())) {
				return tipoLancamento;
			} else if (tipoLancamento.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return tipoLancamento;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return this.valor;
	}

	private void setValor(String valor) {
		this.valor = valor;
	}

	public int getMultiplicador() {
		return this.multiplicador;
	}

	private void setMultiplicador(int multiplicador) {
		this.multiplicador = multiplicador;
	}
}
