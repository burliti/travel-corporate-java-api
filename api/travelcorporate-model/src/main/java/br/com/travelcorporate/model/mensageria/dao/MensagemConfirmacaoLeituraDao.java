package br.com.travelcorporate.model.mensageria.dao;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.mensageria.entities.MensagemConfirmacaoLeitura;

@Stateless
public class MensagemConfirmacaoLeituraDao extends AbstractDao<Integer, MensagemConfirmacaoLeitura> {

}
