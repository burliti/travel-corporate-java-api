package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.travelcorporate.core.enums.FormaLogoff;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.converters.FormaLogoffConverter;

@Entity
@Table(name = "sessao_usuario_excluida")
public class SessaoUsuarioExcluida extends AbstractEntity<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_sessao_usuario_excluida", sequenceName = "seq_sessao_usuario_excluida", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sessao_usuario_excluida")
	@Column(name = "id_sessao_excluida")
	private Integer id;

	@Column(name = "chave_sessao")
	private String chaveSessao;

	@Column(name = "data_criacao")
	private Calendar dataCriacao;

	@Column(name = "data_logoff")
	private Calendar dataLogoff;

	@Column(name = "endereco_ip")
	private String enderecoIp;

	@Column(name = "forma_logoff")
	@Convert(converter = FormaLogoffConverter.class)
	private FormaLogoff formaLogoff;

	private String hostName;

	@Column(name = "id_sessao_original")
	private Integer idSessaoOriginal;

	@Column(name = "ultima_interacao")
	private Calendar ultimaInteracao;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Funcionario.class)
	@JoinColumn(name = "id_funcionario")
	private IFuncionario funcionario;

	private String observacoes;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getChaveSessao() {
		return chaveSessao;
	}

	public void setChaveSessao(String chaveSessao) {
		this.chaveSessao = chaveSessao;
	}

	public Calendar getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Calendar getDataLogoff() {
		return dataLogoff;
	}

	public void setDataLogoff(Calendar dataLogoff) {
		this.dataLogoff = dataLogoff;
	}

	public String getEnderecoIp() {
		return enderecoIp;
	}

	public void setEnderecoIp(String enderecoIp) {
		this.enderecoIp = enderecoIp;
	}

	public FormaLogoff getFormaLogoff() {
		return formaLogoff;
	}

	public void setFormaLogoff(FormaLogoff formaLogoff) {
		this.formaLogoff = formaLogoff;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Integer getIdSessaoOriginal() {
		return idSessaoOriginal;
	}

	public void setIdSessaoOriginal(Integer idSessaoOriginal) {
		this.idSessaoOriginal = idSessaoOriginal;
	}

	public Calendar getUltimaInteracao() {
		return ultimaInteracao;
	}

	public void setUltimaInteracao(Calendar ultimaInteracao) {
		this.ultimaInteracao = ultimaInteracao;
	}

	@Override
	public IEmpresa getEmpresa() {
		return empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public IFuncionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(IFuncionario funcionario) {
		this.funcionario = funcionario;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

}