package br.com.travelcorporate.model.mensageria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MensagemDadosDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<FuncionarioDTO> funcionarios;

	private List<FormaNotificacaoEmpresaDTO> formasNotificacao;

	private Integer totalRecebidas;

	private Integer totalEnviadas;

	private Integer totalRascunhos;

	public List<FuncionarioDTO> getFuncionarios() {
		if (this.funcionarios == null) {
			this.funcionarios = new ArrayList<>();
		}
		return this.funcionarios;
	}

	public void setFuncionarios(List<FuncionarioDTO> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public List<FormaNotificacaoEmpresaDTO> getFormasNotificacao() {
		if (this.formasNotificacao == null) {
			this.formasNotificacao = new ArrayList<>();
		}
		return this.formasNotificacao;
	}

	public void setFormasNotificacao(List<FormaNotificacaoEmpresaDTO> formasNotificacao) {
		this.formasNotificacao = formasNotificacao;
	}

	public Integer getTotalRecebidas() {
		return totalRecebidas;
	}

	public void setTotalRecebidas(Integer totalRecebidas) {
		this.totalRecebidas = totalRecebidas;
	}

	public Integer getTotalEnviadas() {
		return totalEnviadas;
	}

	public void setTotalEnviadas(Integer totalEnviadas) {
		this.totalEnviadas = totalEnviadas;
	}

	public Integer getTotalRascunhos() {
		return totalRascunhos;
	}

	public void setTotalRascunhos(Integer totalRascunhos) {
		this.totalRascunhos = totalRascunhos;
	}
}
