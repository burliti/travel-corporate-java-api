package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoPessoa {

	FISICA(1, "Física"), JURIDICA(2, "Jurídica");

	private String descricao;
	private Integer valor;

	TipoPessoa(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoPessoa fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final TipoPessoa tipoPessoa : TipoPessoa.values()) {
			if (tipoPessoa.getValor().toString().equals(source.toUpperCase())) {
				return tipoPessoa;
			} else if (tipoPessoa.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return tipoPessoa;
			} else if (tipoPessoa.toString().equalsIgnoreCase(source)) {
				return tipoPessoa;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
