package br.com.travelcorporate.model.lancamentos.dto;

import java.math.BigDecimal;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FechamentoDTO {

	private Integer id;

	private FuncionarioDTO funcionarioFechamento;

	private Calendar dataHoraCadastro;

	private Calendar dataHoraEnviado;

	private Calendar dataHoraAprovacao;

	private Calendar dataHoraRejeicao;

	private String emailDestino;

	private FuncionarioDTO funcionarioRecebimento;

	private FuncionarioDTO funcionarioAprovacao;

	private FuncionarioDTO funcionarioRejeicao;

	private BigDecimal saldoAnterior;

	private BigDecimal totalDebitos;

	private BigDecimal totalCreditos;

	private BigDecimal saldoFinal;

	private String motivoRejeicao;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FuncionarioDTO getFuncionarioFechamento() {
		return this.funcionarioFechamento;
	}

	public void setFuncionarioFechamento(FuncionarioDTO funcionarioFechamento) {
		this.funcionarioFechamento = funcionarioFechamento;
	}

	public Calendar getDataHoraCadastro() {
		return this.dataHoraCadastro;
	}

	public void setDataHoraCadastro(Calendar dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	public Calendar getDataHoraEnviado() {
		return this.dataHoraEnviado;
	}

	public void setDataHoraEnviado(Calendar dataHoraEnviado) {
		this.dataHoraEnviado = dataHoraEnviado;
	}

	public Calendar getDataHoraAprovacao() {
		return this.dataHoraAprovacao;
	}

	public void setDataHoraAprovacao(Calendar dataHoraAprovacao) {
		this.dataHoraAprovacao = dataHoraAprovacao;
	}

	public Calendar getDataHoraRejeicao() {
		return this.dataHoraRejeicao;
	}

	public void setDataHoraRejeicao(Calendar dataHoraRejeicao) {
		this.dataHoraRejeicao = dataHoraRejeicao;
	}

	public String getEmailDestino() {
		return this.emailDestino;
	}

	public void setEmailDestino(String emailDestino) {
		this.emailDestino = emailDestino;
	}

	public FuncionarioDTO getFuncionarioRecebimento() {
		return this.funcionarioRecebimento;
	}

	public void setFuncionarioRecebimento(FuncionarioDTO funcionarioRecebimento) {
		this.funcionarioRecebimento = funcionarioRecebimento;
	}

	public FuncionarioDTO getFuncionarioAprovacao() {
		return this.funcionarioAprovacao;
	}

	public void setFuncionarioAprovacao(FuncionarioDTO funcionarioAprovacao) {
		this.funcionarioAprovacao = funcionarioAprovacao;
	}

	public FuncionarioDTO getFuncionarioRejeicao() {
		return this.funcionarioRejeicao;
	}

	public void setFuncionarioRejeicao(FuncionarioDTO funcionarioRejeicao) {
		this.funcionarioRejeicao = funcionarioRejeicao;
	}

	public BigDecimal getSaldoAnterior() {
		return this.saldoAnterior;
	}

	public void setSaldoAnterior(BigDecimal saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public BigDecimal getTotalDebitos() {
		return this.totalDebitos;
	}

	public void setTotalDebitos(BigDecimal totalDebitos) {
		this.totalDebitos = totalDebitos;
	}

	public BigDecimal getTotalCreditos() {
		return this.totalCreditos;
	}

	public void setTotalCreditos(BigDecimal totalCreditos) {
		this.totalCreditos = totalCreditos;
	}

	public BigDecimal getSaldoFinal() {
		return this.saldoFinal;
	}

	public void setSaldoFinal(BigDecimal saldoFinal) {
		this.saldoFinal = saldoFinal;
	}

	public String getMotivoRejeicao() {
		return this.motivoRejeicao;
	}

	public void setMotivoRejeicao(String motivoRejeicao) {
		this.motivoRejeicao = motivoRejeicao;
	}
}
