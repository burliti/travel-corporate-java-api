package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoPeriodo {

	DATA_REFERENCIA(1, "Data de Referência"), PERIODO(2, "Período");

	private String descricao;
	private Integer valor;

	TipoPeriodo(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoPeriodo fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final TipoPeriodo tipoPeriodo : TipoPeriodo.values()) {
			if (tipoPeriodo.getValor().toString().equals(source.toUpperCase())) {
				return tipoPeriodo;
			} else if (tipoPeriodo.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return tipoPeriodo;
			} else if (tipoPeriodo.toString().equalsIgnoreCase(source)) {
				return tipoPeriodo;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
