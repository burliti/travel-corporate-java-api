package br.com.travelcorporate.model.cadastros.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dao.CentroCustoDao;
import br.com.travelcorporate.model.cadastros.entities.CentroCusto;
import br.com.travelcorporate.model.geral.enums.Status;

@Stateless
public class CentroCustoService extends AbstractService<Integer, CentroCusto, CentroCustoDao> {

	@Override
	public void doAntesSalvar(CentroCusto vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getPai() != null) {
			if (vo.getPai().getId() == null) {
				vo.setPai(null);
			} else {
				vo.setPai(this.buscar(vo.getPai().getId()));
			}
		}

		if (vo.getCodigo() != null) {
			vo.setCodigo(vo.getCodigo().trim());
		}

		if (vo.getNome() != null) {
			vo.setNome(vo.getNome().trim());
		}

		// Valida para não inserir dados repetidos
		this.validaCentroCustoRepetido(vo);
	}

	public void validaCentroCustoRepetido(CentroCusto vo) throws BusinessException {
		// TODO Arrumar este metodo
		// if (!vo.getStatus().isAtivo()) {
		// return;
		// }
		//
		// CentroCusto byNome = this.getByNome(vo.getNome());
		//
		// if (byNome != null && !byNome.getId().equals(vo.getId())) {
		// throw new BusinessException("Centro de custo [" + byNome.getCodigo()
		// + " - " + byNome.getNome() + "] já existe cadastrado com esse
		// nome!");
		// }
		//
		// CentroCusto byCodigo = this.getByCodigo(vo.getCodigo());
		//
		// if (byCodigo != null && !byCodigo.getId().equals(vo.getId())) {
		// throw new BusinessException("Centro de custo [" +
		// byCodigo.getCodigo() + " - " + byCodigo.getNome() + "] já existe
		// cadastrado com esse código!");
		// }
	}

	public CentroCusto getByNome(String nome) {
		Map<String, String> filtros = new HashMap<>();

		filtros.put("nome", nome.trim());
		filtros.put("status", Status.ATIVO.getValor());

		List<CentroCusto> list = this.consultar(filtros, null, null, null);

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public CentroCusto getByCodigo(String codigo) {
		Map<String, String> filtros = new HashMap<>();

		filtros.put("codigo", codigo.trim());
		filtros.put("status", Status.ATIVO.getValor());

		List<CentroCusto> list = this.consultar(filtros, null, null, null);

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

	public List<CentroCusto> getAtivos() {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());

		order.put("codigo", "ASC");

		return this.consultar(filtros, null, null, order);
	}
}
