package br.com.travelcorporate.model.lancamentos.service;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.service.CategoriaInternaService;
import br.com.travelcorporate.model.geral.enums.DestinoSaldo;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.geral.enums.TipoPeriodo;
import br.com.travelcorporate.model.geral.exceptions.financeiro.AprovacaoContasException;
import br.com.travelcorporate.model.lancamentos.dao.AprovacaoContasDao;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class AprovacaoContasService extends AbstractService<Integer, Viagem, AprovacaoContasDao> {

	@Inject
	private LancamentoService lancamentoService;

	@Inject
	private FechamentoService fechamentoService;

	@Inject
	private ViagemService viagemService;

	@Inject
	private CategoriaInternaService categoriaInternaService;

	@Inject
	private DevolucaoViagemService devolucaoViagemService;

	@Inject
	private ReembolsoViagemService reembolsoViagemService;

	@Override
	public void doAntesSalvar(Viagem vo) throws BusinessException {
		super.doAntesSalvar(vo);

		this.viagemService.doAntesSalvar(vo);

		// Validar motivo rejeicao
		if (vo.getStatus() == StatusViagem.REJEITADA) {
			if (vo.getFechamento().getMotivoRejeicao() == null || vo.getFechamento().getMotivoRejeicao().trim().isEmpty()) {
				throw new AprovacaoContasException("Informe o motivo da rejeição!");
			}
		}

		if (vo.getLancamentos() != null) {
			for (Lancamento lancamento : vo.getLancamentos()) {

				this.lancamentoService.doAntesSalvar(lancamento);

				lancamento.setViagem(vo);
			}
		}

		if (vo.getStatus() == StatusViagem.FINALIZADA) {
			vo.setStatus(StatusViagem.RECEBIDO_FINANCEIRO);
		}

		vo.getFechamento().setViagem(vo);

		this.fechamentoService.doAntesSalvar(vo.getFechamento());
	}

	public Viagem aprovar(Viagem viagem) throws BusinessException {
		viagem.setStatus(StatusViagem.APROVADA);
		viagem.getFechamento().setFuncionarioAprovacao((Funcionario) this.getSessao().getFuncionario());
		viagem.getFechamento().setDataHoraAprovacao(Calendar.getInstance());
		viagem.getFechamento().setFuncionarioRejeicao(null);
		viagem.getFechamento().setDataHoraRejeicao((String) null);

		if (viagem.getFechamento().getDestinoSaldo() == null) {
			throw new AprovacaoContasException("Informe o destino do saldo!");
		}

		this.viagemService.doAntesSalvar(viagem);

		if (viagem.getFechamento().getSaldoFinal().compareTo(BigDecimal.ZERO) == 0) {
			viagem.getFechamento().setDestinoSaldo(DestinoSaldo.SEM_SALDO);
		}

		// @formatter:off
		if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.SALDO_POSITIVO_PROXIMA_VIAGEM
				|| viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.ZERAR_SALDO_POSITIVO
				|| viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.DEVOLUCAO) {
			if (viagem.getFechamento().getSaldoFinal().compareTo(BigDecimal.ZERO) <= 0) {
				throw new AprovacaoContasException("O saldo não está positivo!");
			}
		} else if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.SALDO_NEGATIVO_PROXIMA_VIAGEM
				|| viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.ZERAR_SALDO_NEGATIVO
				|| viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.REEMBOLSO) {
			if (viagem.getFechamento().getSaldoFinal().compareTo(BigDecimal.ZERO) >= 0) {
				throw new AprovacaoContasException("O saldo não está negativo!");
			}
		}
		// @formatter:on

		this.viagemService.calcularSaldos(viagem);

		if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.DEVOLUCAO) {
			viagem = this.aprovarViagemDevolucao(viagem);
		} else if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.SALDO_POSITIVO_PROXIMA_VIAGEM) {
			viagem = this.aprovarViagemSaldoPositivoProximaViagem(viagem);
		} else if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.ZERAR_SALDO_POSITIVO) {
			viagem = this.aprovarViagemZerarSaldoPositivo(viagem);
		} else if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.REEMBOLSO) {
			viagem = this.aprovarViagemReembolso(viagem);
		} else if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.ZERAR_SALDO_NEGATIVO) {
			viagem = this.aprovarViagemZerarSaldoNegativo(viagem);
		} else if (viagem.getFechamento().getDestinoSaldo() == DestinoSaldo.SALDO_NEGATIVO_PROXIMA_VIAGEM) {
			viagem = this.aprovarViagemSaldoNegativoProximaViagem(viagem);
		}

		return viagem;
	}

	private Viagem aprovarViagemSaldoNegativoProximaViagem(Viagem viagem) throws BusinessException {
		BigDecimal saldoFinal = viagem.getFechamento().getSaldoFinal().abs();

		// Gera outro lançamento para "zerar" este relatório
		Lancamento lancamento2 = new Lancamento();
		lancamento2.setCategoria(this.categoriaInternaService.getSaldoFaltanteGeradoProximaViagem());
		lancamento2.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento2.setDataInicial(Calendar.getInstance());
		lancamento2.setValorLancamento(saldoFinal);

		viagem.getLancamentos().add(lancamento2);
		viagem.getFechamento().setSaldoFinal(saldoFinal.multiply(new BigDecimal(-1)));

		viagem = this.atualizar(viagem);

		this.flush();

		// Gera lançamento para outra viagem
		Lancamento lancamento = new Lancamento();
		lancamento.setObservacoes("Saldo devedor da viagem " + FormatUtils.fillLeft(viagem.getSequencial(), "0", 6));
		lancamento.setCategoria(this.categoriaInternaService.getSaldoDevedorViagemAnterior());
		lancamento.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento.setDataInicial(Calendar.getInstance());
		lancamento.setValorLancamento(saldoFinal);
		lancamento.setSequencial(0);

		this.lancamentoService.inserir(lancamento);

		this.flush();
		return viagem;
	}

	private Viagem aprovarViagemZerarSaldoNegativo(Viagem viagem) throws BusinessException {
		// Gera um lançamento para "zerar" o saldo negativo. O saldo será
		// perdido.
		Lancamento lancamento = new Lancamento();
		lancamento.setObservacoes("Zerando saldo da viagem " + FormatUtils.fillLeft(viagem.getSequencial(), "0", 6));
		lancamento.setCategoria(this.categoriaInternaService.getSaldoNegativoRemanescente());
		lancamento.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento.setDataInicial(Calendar.getInstance());
		lancamento.setValorLancamento(viagem.getFechamento().getSaldoFinal().abs());

		viagem.getLancamentos().add(lancamento);

		viagem = this.atualizar(viagem);

		this.flush();

		return viagem;
	}

	private Viagem aprovarViagemReembolso(Viagem viagem) throws BusinessException {
		BigDecimal saldoFinal = viagem.getFechamento().getSaldoFinal().abs();

		// Gera um lançamento para "zerar" o saldo positivo. Do saldo será
		// gerada uma devolução.
		Lancamento lancamento = new Lancamento();
		lancamento.setCategoria(this.categoriaInternaService.getReembolso());
		lancamento.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento.setDataInicial(Calendar.getInstance());
		lancamento.setValorLancamento(saldoFinal);

		this.lancamentoService.doAntesSalvar(lancamento);

		viagem.getLancamentos().add(lancamento);
		viagem.getFechamento().setSaldoFinal(saldoFinal.multiply(new BigDecimal(-1)));

		viagem = this.atualizar(viagem);

		this.flush();

		// Gera um registro de devolução - recibo para o usuário devolver
		// dinheiro

		viagem.getLancamentos().sort((o1, o2) -> o2.getId().compareTo(o1.getId()));
		Lancamento lancamento2 = viagem.getLancamentos().get(viagem.getLancamentos().size() - 1);

		this.reembolsoViagemService.criarReembolso(viagem, lancamento2);

		this.flush();

		return viagem;
	}

	private Viagem aprovarViagemZerarSaldoPositivo(Viagem viagem) throws BusinessException {
		BigDecimal saldoFinal = viagem.getFechamento().getSaldoFinal().abs();

		// Gera um lançamento para "zerar" o saldo positivo. O saldo será
		// perdido.
		Lancamento lancamento = new Lancamento();
		lancamento.setObservacoes("Zerando saldo da viagem " + FormatUtils.fillLeft(viagem.getSequencial(), "0", 6));
		lancamento.setCategoria(this.categoriaInternaService.getSaldoRemanescente());
		lancamento.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento.setDataInicial(Calendar.getInstance());
		lancamento.setValorLancamento(saldoFinal);

		viagem.getLancamentos().add(lancamento);
		viagem.getFechamento().setSaldoFinal(saldoFinal);

		viagem = this.atualizar(viagem);

		this.flush();
		return viagem;
	}

	private Viagem aprovarViagemSaldoPositivoProximaViagem(Viagem viagem) throws BusinessException {
		BigDecimal saldoFinal = viagem.getFechamento().getSaldoFinal().abs();

		// Gera outro lançamento para "zerar" este relatório
		Lancamento lancamento2 = new Lancamento();
		lancamento2.setObservacoes("Zerando saldo da viagem " + FormatUtils.fillLeft(viagem.getSequencial(), "0", 6));
		lancamento2.setCategoria(this.categoriaInternaService.getSaldoRemanescenteGeradoProximaViagem());
		lancamento2.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento2.setDataInicial(Calendar.getInstance());
		lancamento2.setValorLancamento(saldoFinal);

		viagem.getLancamentos().add(lancamento2);
		viagem.getFechamento().setSaldoFinal(saldoFinal);

		viagem = this.atualizar(viagem);

		this.flush();

		// Gera lançamento para outra viagem
		Lancamento lancamento = new Lancamento();
		lancamento.setObservacoes("Saldo remanescente da viagem " + FormatUtils.fillLeft(viagem.getSequencial(), "0", 6));
		lancamento.setCategoria(this.categoriaInternaService.getSaldoViagemAnterior());
		lancamento.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento.setDataInicial(Calendar.getInstance());
		lancamento.setValorLancamento(saldoFinal);
		lancamento.setDataHoraLancamento(Calendar.getInstance());
		lancamento.setSequencial(1);

		this.lancamentoService.inserir(lancamento);

		this.flush();
		return viagem;
	}

	private Viagem aprovarViagemDevolucao(Viagem viagem) throws BusinessException {
		// Gera um lançamento para "zerar" o saldo positivo. Do saldo será
		// gerada uma devolução.
		BigDecimal saldoFinal = viagem.getFechamento().getSaldoFinal().abs();

		Lancamento lancamento = new Lancamento();
		lancamento.setObservacoes("Zerando saldo da viagem " + FormatUtils.fillLeft(viagem.getSequencial(), "0", 6));
		lancamento.setCategoria(this.categoriaInternaService.getDevolucao());
		lancamento.setTipoPeriodo(TipoPeriodo.DATA_REFERENCIA);
		lancamento.setDataInicial(Calendar.getInstance());
		lancamento.setValorLancamento(saldoFinal);

		viagem.getLancamentos().add(lancamento);
		viagem.getFechamento().setSaldoFinal(saldoFinal);

		viagem = this.atualizar(viagem);

		this.flush();

		// Gera um registro de devolução - recibo para o usuário devolver
		// dinheiro
		viagem.getLancamentos().sort((o1, o2) -> o2.getId().compareTo(o1.getId()));
		Lancamento lancamento2 = viagem.getLancamentos().get(viagem.getLancamentos().size() - 1);

		this.devolucaoViagemService.criarDevolucao(viagem, lancamento2);

		this.flush();
		return viagem;
	}

	public Viagem rejeitar(Viagem viagem) throws BusinessException {
		viagem.setStatus(StatusViagem.REJEITADA);
		viagem.getFechamento().setFuncionarioRejeicao((Funcionario) this.getSessao().getFuncionario());
		viagem.getFechamento().setDataHoraRejeicao(Calendar.getInstance());

		return this.atualizar(viagem);
	}
}
