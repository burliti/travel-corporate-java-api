package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.TipoLancamento;

@Converter(autoApply = true)
public class TipoLancamentoConverter implements AttributeConverter<TipoLancamento, String> {

	@Override
	public String convertToDatabaseColumn(TipoLancamento status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public TipoLancamento convertToEntityAttribute(String source) {
		return TipoLancamento.fromValue(source);
	}
}