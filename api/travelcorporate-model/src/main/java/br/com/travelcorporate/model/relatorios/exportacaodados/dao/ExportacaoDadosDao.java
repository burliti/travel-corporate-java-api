package br.com.travelcorporate.model.relatorios.exportacaodados.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import br.com.travelcorporate.model.lancamentos.service.ViagemService;

@Stateless
public class ExportacaoDadosDao {

	@Inject
	private ViagemService viagemService;

	public List<Viagem> consultar(ConsultaRequest request) {
		return this.viagemService.consultar(request.getFiltro(), request.getPageSize(), request.getPageNumber(), request.getOrder());
	}

}
