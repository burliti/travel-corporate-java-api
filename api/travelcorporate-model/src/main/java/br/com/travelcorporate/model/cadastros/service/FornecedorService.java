package br.com.travelcorporate.model.cadastros.service;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.cadastros.dao.ClienteDao;
import br.com.travelcorporate.model.cadastros.entities.Cliente;

@Stateless
public class FornecedorService extends AbstractService<Integer, Cliente, ClienteDao> {

}