package br.com.travelcorporate.model.mensageria.services;

import java.util.Calendar;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.service.FuncionarioService;
import br.com.travelcorporate.model.geral.enums.StatusMensagem;
import br.com.travelcorporate.model.geral.enums.StatusMensagemDestinatario;
import br.com.travelcorporate.model.mensageria.dao.MensagemDao;
import br.com.travelcorporate.model.mensageria.entities.Mensagem;

@Stateless
public class MensagemService extends AbstractService<Integer, Mensagem, MensagemDao> {

	@Inject
	private FuncionarioService funcionarioService;

	@Inject
	private FormaNotificacaoService formaNotificacaoService;

	@Override
	public void doAntesSalvar(Mensagem vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getStatus() == StatusMensagem.ENVIADA) {
			if (vo.getDataHoraEnvio() == null) {
				vo.setDataHoraEnvio(Calendar.getInstance());
			}
		} else if (vo.getStatus() == StatusMensagem.EXCLUIDA) {
			if (vo.getDataHoraExclusao() == null) {
				vo.setDataHoraExclusao(Calendar.getInstance());
			}
		}

		vo.getDestinatarios().forEach((d) -> {
			d.setMensagem(vo);
			d.setDestinatario(this.funcionarioService.buscar(d.getDestinatario()));
			d.setEmpresa(vo.getEmpresa());
			if (d.getStatus() == null) {
				d.setStatus(StatusMensagemDestinatario.NAO_LIDA);
			}
		});

		vo.getFormasNotificacao().forEach((fm) -> {
			fm.setMensagem(vo);
			fm.setFormaNotificacao(this.formaNotificacaoService.buscar(fm.getFormaNotificacao()));
			fm.setEmpresa(vo.getEmpresa());
		});

	}
}
