package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.DestinoSaldo;

@Converter(autoApply = true)
public class DestinoSaldoConverter implements AttributeConverter<DestinoSaldo, Integer> {

	@Override
	public Integer convertToDatabaseColumn(DestinoSaldo destinoSaldo) {
		if (destinoSaldo == null) {
			return null;
		}
		return destinoSaldo.getValor();
	}

	@Override
	public DestinoSaldo convertToEntityAttribute(Integer source) {
		if (source != null) {
			return DestinoSaldo.fromValue(source.toString());
		}
		return null;
	}
}