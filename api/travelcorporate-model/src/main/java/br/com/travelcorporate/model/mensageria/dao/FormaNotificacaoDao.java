package br.com.travelcorporate.model.mensageria.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.mensageria.entities.FormaNotificacao;

@Stateless
public class FormaNotificacaoDao extends AbstractDao<Integer, FormaNotificacao> {

	public FormaNotificacao getByNome(String nome) {
		String hql = "SELECT e FROM " + FormaNotificacao.class.getSimpleName() + " e ";
		hql += " WHERE UPPER(e.nome) = :p_nome";

		TypedQuery<FormaNotificacao> query = this.getManager().createQuery(hql, FormaNotificacao.class);
		query.setParameter("p_nome", nome);

		List<FormaNotificacao> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}
}
