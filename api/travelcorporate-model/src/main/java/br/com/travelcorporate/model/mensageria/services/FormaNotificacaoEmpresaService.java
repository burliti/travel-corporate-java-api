package br.com.travelcorporate.model.mensageria.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.mensageria.dao.FormaNotificacaoEmpresaDao;
import br.com.travelcorporate.model.mensageria.entities.FormaNotificacaoEmpresa;

@Stateless
public class FormaNotificacaoEmpresaService extends AbstractService<Integer, FormaNotificacaoEmpresa, FormaNotificacaoEmpresaDao> {

	public List<FormaNotificacaoEmpresa> getAtivos() {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());

		order.put("formaNotificacao.nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}
}
