package br.com.travelcorporate.model.tabelas.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IgnoreEmpresa;

@Entity
@IgnoreEmpresa
@Table(name = "acao_recurso")
public class AcaoRecurso extends AbstractEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_acao_recurso", sequenceName = "seq_acao_recurso", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_acao_recurso")
	@Column(name = "id_acao_recurso")
	private Integer id;

	@JoinColumn(name = "id_recurso")
	@ManyToOne
	private Recurso recurso;

	@JoinColumn(name = "id_acao")
	@ManyToOne
	private Acao acao;

	public AcaoRecurso() {
	}

	public AcaoRecurso(Acao acao, Recurso recurso) {
		this.acao = acao;
		this.recurso = recurso;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {

	}

	@JsonIgnore
	@Override
	public IEmpresa getEmpresa() {
		return null;
	}

	public Recurso getRecurso() {
		return this.recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}

	public Acao getAcao() {
		return this.acao;
	}

	public void setAcao(Acao acao) {
		this.acao = acao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (this.getId() == null ? 0 : this.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof AcaoRecurso)) {
			return false;
		}
		final AcaoRecurso other = (AcaoRecurso) obj;
		if (this.getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!this.getId().equals(other.getId())) {
			return false;
		}
		return true;
	}
}
