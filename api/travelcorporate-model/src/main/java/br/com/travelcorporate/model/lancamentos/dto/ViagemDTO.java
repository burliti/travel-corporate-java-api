package br.com.travelcorporate.model.lancamentos.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.model.cadastros.dto.ClienteDTO;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.ProjetoDTO;
import br.com.travelcorporate.model.geral.enums.StatusViagem;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ViagemDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private Integer sequencial;

	private Calendar dataIda;

	private Calendar dataVolta;

	private StatusViagem status;

	private String descricao;

	private String objetivo;

	private String observacao;

	private FechamentoDTO fechamento;

	private FuncionarioDTO funcionario;

	private List<LancamentoDTO> lancamentos;

	private ClienteDTO cliente;

	private ProjetoDTO projeto;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSequencial() {
		return this.sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public Calendar getDataIda() {
		return this.dataIda;
	}

	public void setDataIda(Calendar dataIda) {
		this.dataIda = dataIda;
	}

	public Calendar getDataVolta() {
		return this.dataVolta;
	}

	public void setDataVolta(Calendar dataVolta) {
		this.dataVolta = dataVolta;
	}

	public StatusViagem getStatus() {
		return this.status;
	}

	@JsonGetter("statusValue")
	public String getStatusValue() {
		return this.getStatus() != null ? this.getStatus().getValor().toString() : null;
	}

	public void setStatus(StatusViagem status) {
		this.status = status;
	}

	@JsonGetter("dataIda")
	public String getDataIdaFormatada() {
		return FormatUtils.formatDate(this.getDataIda());
	}

	@JsonSetter
	public void setDataIda(String dataIda) {
		this.setDataIda(FormatUtils.parseDate(dataIda));
	}

	@JsonGetter("dataVolta")
	public String getDataVoltaFormatada() {
		return FormatUtils.formatDate(this.getDataVolta());
	}

	@JsonSetter
	public void setDataVolta(String dataVolta) {
		this.setDataVolta(FormatUtils.parseDate(dataVolta));
	}

	public FechamentoDTO getFechamento() {
		return this.fechamento;
	}

	public void setFechamento(FechamentoDTO fechamento) {
		this.fechamento = fechamento;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public FuncionarioDTO getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(FuncionarioDTO funcionario) {
		this.funcionario = funcionario;
	}

	public List<LancamentoDTO> getLancamentos() {
		if (this.lancamentos == null) {
			this.lancamentos = new ArrayList<>();
		}
		return this.lancamentos;
	}

	public void setLancamentos(List<LancamentoDTO> lancamentos) {
		this.lancamentos = lancamentos;
	}

	public ClienteDTO getCliente() {
		return this.cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public ProjetoDTO getProjeto() {
		return this.projeto;
	}

	public void setProjeto(ProjetoDTO projeto) {
		this.projeto = projeto;
	}

	public String getObjetivo() {
		return this.objetivo;
	}

	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}

	public String getObservacao() {
		return this.observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
