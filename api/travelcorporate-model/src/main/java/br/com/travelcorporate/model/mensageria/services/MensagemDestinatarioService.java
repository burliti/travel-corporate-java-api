package br.com.travelcorporate.model.mensageria.services;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.mensageria.dao.MensagemDestinatarioDao;
import br.com.travelcorporate.model.mensageria.dto.MensagemDestinatarioDTO;
import br.com.travelcorporate.model.mensageria.entities.MensagemDestinatario;

@Stateless
public class MensagemDestinatarioService extends AbstractService<Integer, MensagemDestinatario, MensagemDestinatarioDao> {

	public MensagemDestinatarioDTO convertDTO(MensagemDestinatario mensagemDestinatario) {
		if (mensagemDestinatario == null) {
			return null;
		}

		MensagemDestinatarioDTO dto = new MensagemDestinatarioDTO();

		dto.setId(mensagemDestinatario.getId());
		dto.setDestinatario(new FuncionarioDTO());
		dto.getDestinatario().setId(mensagemDestinatario.getDestinatario().getId());
		dto.getDestinatario().setNome(mensagemDestinatario.getDestinatario().getNome());

		return dto;
	}
}
