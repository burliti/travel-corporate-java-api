package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.StatusViagem;

@Converter(autoApply = true)
public class StatusViagemConverter implements AttributeConverter<StatusViagem, Integer> {

	@Override
	public Integer convertToDatabaseColumn(StatusViagem status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public StatusViagem convertToEntityAttribute(Integer source) {
		return StatusViagem.fromValue(source);
	}
}