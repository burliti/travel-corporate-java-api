package br.com.travelcorporate.model.tabelas.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.tabelas.entities.Estado;

@Stateless
public class EstadoDao extends AbstractDao<Integer, Estado> {

	public Estado getBySigla(String sigla) {
		final String sql = "SELECT e FROM " + getVOClass().getSimpleName() + " e WHERE e.sigla = :p_sigla ";

		final TypedQuery<Estado> query = getManager().createQuery(sql, getVOClass());

		query.setParameter("p_sigla", sigla);

		final List<Estado> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}
}