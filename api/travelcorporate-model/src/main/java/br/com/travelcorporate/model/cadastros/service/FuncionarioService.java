package br.com.travelcorporate.model.cadastros.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.enums.TipoSessao;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.core.utils.SegurancaUtils;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.cadastros.dao.FuncionarioDao;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuario;
import br.com.travelcorporate.model.configuracao.dto.PerfilDTO;
import br.com.travelcorporate.model.configuracao.service.EmpresaService;
import br.com.travelcorporate.model.configuracao.service.PerfilService;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.exceptions.LoginInvalidoException;
import br.com.travelcorporate.model.geral.exceptions.funcionario.SenhaConfirmacaoInvalidaException;

@Stateless
public class FuncionarioService extends AbstractService<Integer, Funcionario, FuncionarioDao> {

	@Inject
	private HttpServletRequest request;

	@Inject
	private SessaoUsuarioService sessaoUsuarioService;

	@Inject
	private PerfilService perfilService;

	@Inject
	private SetorService setorService;

	@Inject
	private EmpresaService empresaService;

	@Inject
	private CentroCustoService centroCustoService;

	@Override
	public void doAntesAtualizar(Funcionario vo) throws BusinessException {
		if (vo.getSenha() == null || vo.getSenha().trim().isEmpty()) {
			final Funcionario buscar = this.buscar(vo);

			vo.setSenha(buscar.getSenha());
		} else {
			this.validarSenhaConfirmacao(vo);
		}

		super.doAntesAtualizar(vo);
	}

	@Override
	public void doAntesInserir(Funcionario vo) throws BusinessException {
		super.doAntesInserir(vo);

		this.validarSenhaConfirmacao(vo);
	}

	@Override
	public void doAntesSalvar(Funcionario vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getPerfil() != null) {
			if (vo.getPerfil().getId() != null) {
				vo.setPerfil(this.perfilService.buscar(vo.getPerfil().getId()));
			} else {
				vo.setPerfil(null);
			}
		}

		if (vo.getSetor() != null) {
			if (vo.getSetor().getId() != null) {
				vo.setSetor(this.setorService.buscar(vo.getSetor().getId()));
			} else {
				vo.setSetor(null);
			}
		}

		if (vo.getFilial() != null) {
			if (vo.getFilial().getId() == null || vo.getFilial().getId() <= 0) {
				vo.setFilial(null);
			} else {
				vo.setFilial(this.empresaService.buscar(vo.getFilial().getId()));
			}
		}

		if (vo.getCentroCusto() != null) {
			if (vo.getCentroCusto().getId() == null || vo.getCentroCusto().getId() <= 0) {
				vo.setCentroCusto(null);
			} else {
				vo.setCentroCusto(this.centroCustoService.buscar(vo.getCentroCusto().getId()));
			}
		}

		if (vo.getEmail() != null) {
			vo.setEmail(vo.getEmail().trim().toLowerCase());
		}
	}

	private void validarSenhaConfirmacao(Funcionario vo) throws SenhaConfirmacaoInvalidaException {
		if (vo.getSenha() == null || vo.getConfirmacaoSenha() == null || !vo.getSenha().equals(vo.getConfirmacaoSenha())) {
			throw new SenhaConfirmacaoInvalidaException();
		}
	}

	public SessaoUsuario entrar(String email, String senhaMD5, TipoSessao tipoSessao) throws BusinessException {

		if (this.request == null) {
			throw new LoginInvalidoException("Request indisponível no contexto!");
		}

		final Funcionario funcionario = this.getDao().buscarPorEmail(email, Status.ATIVO);

		if (funcionario == null) {
			throw new LoginInvalidoException("Usuário / Senha inválidos!");
		}

		if (funcionario.getEmail().equalsIgnoreCase(email) && funcionario.getSenhaMD5().equalsIgnoreCase(senhaMD5)) {
			if (funcionario.getStatus() == Status.ATIVO) {

				final String ipAddress = SegurancaUtils.getIpFromRequest(this.request);

				final String computerName = SegurancaUtils.getComputerNameFromIP(ipAddress);

				SessaoUsuario su = new SessaoUsuario();

				su.setChaveSessao(this.generateSessionId());

				su.setHostName(computerName);
				su.setEnderecoIp(ipAddress);
				su.setDataCriacao(Calendar.getInstance());
				su.setUltimaInteracao(Calendar.getInstance());
				su.setFuncionario(funcionario);
				su.setEmpresa(funcionario.getEmpresa());
				su.setTipoSessao(tipoSessao == null ? TipoSessao.WEB : tipoSessao);

				su = this.sessaoUsuarioService.inserir(su);

				return su;
			}
		}

		throw new LoginInvalidoException("Usuário / Senha inválidos!");
	}

	public String generateSessionId() {
		return UUID.randomUUID().toString().toUpperCase().replaceAll("-", "");
	}

	public List<Funcionario> getFuncionarios(List<Funcionario> funcionarios) {
		return this.getDao().getFuncionarios(funcionarios);
	}

	public void alterarSenha(String senhaAtual, String novaSenha, String confirmacaoNovaSenha) throws BusinessException {
		final Funcionario funcionario = (Funcionario) this.getSessao().getFuncionario();

		if (senhaAtual != null && !senhaAtual.trim().isEmpty()) {
			if (!funcionario.getSenha().equals(senhaAtual)) {
				throw new LoginInvalidoException("Senha atual inválida!");
			}
		}

		if (novaSenha == null || confirmacaoNovaSenha == null || novaSenha.trim().isEmpty() || confirmacaoNovaSenha.trim().isEmpty()) {
			throw new LoginInvalidoException("Senha não pode ser vazia!");
		}

		Funcionario f = this.buscar(funcionario.getId());

		f.setSenha(novaSenha);
		f.setConfirmacaoSenha(confirmacaoNovaSenha);
		f.setResetarSenha(Flag.NAO);

		// this.atualizar(f);

		this.getDao().atualizarSenha(f);
	}

	public List<FuncionarioDTO> getFuncionariosAtivosDTO(boolean saldo) {

		Map<String, String> filtros = new HashMap<>();
		Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());

		List<Funcionario> list = this.consultar(filtros, null, null, order);

		List<FuncionarioDTO> listDTO = new ArrayList<>(list.size());

		for (Funcionario funcionario : list) {
			FuncionarioDTO f = new FuncionarioDTO();
			PerfilDTO p = new PerfilDTO();
			p.setNome(funcionario.getPerfil().getNome());
			f.setNome(funcionario.getNome());
			f.setPerfil(p);
			f.setId(funcionario.getId());

			if (saldo) {
				f.setSaldo(this.getSaldoFuncionario(funcionario));
			}

			listDTO.add(f);
		}

		return listDTO;
	}

	public BigDecimal getSaldoFuncionario(IFuncionario funcionario) {
		return this.getDao().getSaldo(funcionario);
	}

	public BigDecimal getSaldoFuncionarioSessao() {
		if (this.getSessao() != null) {
			return this.getSaldoFuncionario(this.getSessao().getFuncionario());
		} else {
			return null;
		}
	}

	public Integer getViagensEmAberto(IFuncionario funcionario) {
		return this.getDao().getViagensEmAberto(funcionario);
	}

	public Integer getViagensEmAbertoSessao() {
		if (this.getSessao() != null) {
			return this.getViagensEmAberto(this.getSessao().getFuncionario());
		} else {
			return null;
		}
	}

	public List<Funcionario> getAtivos() {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());

		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}
}
