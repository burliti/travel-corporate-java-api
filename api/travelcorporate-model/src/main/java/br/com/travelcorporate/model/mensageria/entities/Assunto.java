package br.com.travelcorporate.model.mensageria.entities;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.FlagConverter;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class Assunto extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_assunto", sequenceName = "seq_assunto", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_assunto")
	@Column(name = "id_assunto")
	private Integer id;

	@NotEmpty(message = "Nome do Assunto é obrigatória")
	private String nome;

	@NotNull(message = "Permitir responder é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_permite_responder")
	private Flag permiteResponder;

	@NotNull(message = "Permitir Encaminhar é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_permite_encaminhar")
	private Flag permiteEncaminhar;

	@NotNull(message = "Exige confirmação é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_exige_confirmacao")
	private Flag exigeConfirmacao;

	@NotNull(message = "Exige confirmação é obrigatório")
	@Convert(converter = FlagConverter.class)
	@Column(name = "flg_bloqueia_sem_responder")
	private Flag bloqueiaSemResponder;

	@NotNull(message = "Status é obrigatório")
	@Column(name = "status")
	@Convert(converter = StatusConverter.class)
	private Status status;

	@Column(name = "texto_botao_confirmacao")
	private String textoBotaoConfirmacao;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Flag getPermiteResponder() {
		return this.permiteResponder;
	}

	public void setPermiteResponder(Flag permiteResponder) {
		this.permiteResponder = permiteResponder;
	}

	public Flag getPermiteEncaminhar() {
		return this.permiteEncaminhar;
	}

	public void setPermiteEncaminhar(Flag permiteEncaminhar) {
		this.permiteEncaminhar = permiteEncaminhar;
	}

	public Flag getExigeConfirmacao() {
		return this.exigeConfirmacao;
	}

	public void setExigeConfirmacao(Flag exigeConfirmacao) {
		this.exigeConfirmacao = exigeConfirmacao;
	}

	public Flag getBloqueiaSemResponder() {
		return this.bloqueiaSemResponder;
	}

	public void setBloqueiaSemResponder(Flag bloqueiaSemResponder) {
		this.bloqueiaSemResponder = bloqueiaSemResponder;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getTextoBotaoConfirmacao() {
		return this.textoBotaoConfirmacao;
	}

	public void setTextoBotaoConfirmacao(String textoBotaoConfirmacao) {
		this.textoBotaoConfirmacao = textoBotaoConfirmacao;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}
}
