package br.com.travelcorporate.model.mensageria.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.geral.enums.StatusMensagem;
import br.com.travelcorporate.model.geral.enums.StatusMensagemDestinatario;
import br.com.travelcorporate.model.mensageria.entities.Mensagem;

@Stateless
public class MensagemDao extends AbstractDao<Integer, Mensagem> {

	public List<Mensagem> recebidas(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		this.getManager().clear();

		String hql = "SELECT e FROM Mensagem e ";
		hql += " JOIN e.destinatarios d ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND d.destinatario.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND d.status <> :p_status";

		if (filtros.containsKey("assuntoDescricao")) {
			hql += " AND e.assuntoDescricao LIKE :p_assuntoDescricao";
		}

		if (filtros.containsKey("textoMensagem")) {
			hql += " AND e.textoMensagem LIKE :p_textoMensagem";
		}

		if (order != null && order.size() > 0) {
			hql += " ORDER BY ";
			boolean first = true;
			final Iterator<String> iterator = order.keySet().iterator();

			while (iterator.hasNext()) {
				final String campo = iterator.next();

				if (!first) {
					hql += ", ";
				}

				hql += "e." + campo + " " + order.get(campo);
				first = false;
			}
		}

		final TypedQuery<Mensagem> query = this.getManager().createQuery(hql, Mensagem.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagemDestinatario.EXCLUIDA);

		if (filtros.containsKey("assuntoDescricao")) {
			query.setParameter("p_assuntoDescricao", "%" + filtros.get("assuntoDescricao") + "%");
		}

		if (filtros.containsKey("textoMensagem")) {
			query.setParameter("p_textoMensagem", "%" + filtros.get("textoMensagem") + "%");
		}

		// Paginacao
		if (pageSize != null && pageNumber != null) {
			query.setMaxResults(pageSize);

			final int firstResult = (pageNumber - 1) * pageSize;

			query.setFirstResult(firstResult);

			this.log.debug("Page Number: " + pageNumber);
			this.log.debug("Page Size: " + pageSize);
			this.log.debug("Max results calculados: " + query.getMaxResults());
			this.log.debug("First result calculado: " + query.getFirstResult());
		}

		return query.getResultList();
	}

	public List<Mensagem> enviadas(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		this.getManager().clear();

		String hql = "SELECT e FROM Mensagem e ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND e.autor.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND e.status = :p_status";

		if (order != null && order.size() > 0) {
			hql += " ORDER BY ";
			boolean first = true;
			final Iterator<String> iterator = order.keySet().iterator();

			while (iterator.hasNext()) {
				final String campo = iterator.next();

				if (!first) {
					hql += ", ";
				}

				hql += "e." + campo + " " + order.get(campo);
				first = false;
			}
		}

		final TypedQuery<Mensagem> query = this.getManager().createQuery(hql, Mensagem.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagem.ENVIADA);

		// Paginacao
		if (pageSize != null && pageNumber != null) {
			query.setMaxResults(pageSize);

			final int firstResult = (pageNumber - 1) * pageSize;

			query.setFirstResult(firstResult);

			this.log.debug("Page Number: " + pageNumber);
			this.log.debug("Page Size: " + pageSize);
			this.log.debug("Max results calculados: " + query.getMaxResults());
			this.log.debug("First result calculado: " + query.getFirstResult());
		}

		return query.getResultList();
	}

	public List<Mensagem> rascunhos(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		this.getManager().clear();

		String hql = "SELECT e FROM Mensagem e ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND e.autor.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND e.status = :p_status";

		if (order != null && order.size() > 0) {
			hql += " ORDER BY ";
			boolean first = true;
			final Iterator<String> iterator = order.keySet().iterator();

			while (iterator.hasNext()) {
				final String campo = iterator.next();

				if (!first) {
					hql += ", ";
				}

				hql += "e." + campo + " " + order.get(campo);
				first = false;
			}
		}

		final TypedQuery<Mensagem> query = this.getManager().createQuery(hql, Mensagem.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagem.RASCUNHO);

		// Paginacao
		if (pageSize != null && pageNumber != null) {
			query.setMaxResults(pageSize);

			final int firstResult = (pageNumber - 1) * pageSize;

			query.setFirstResult(firstResult);

			this.log.debug("Page Number: " + pageNumber);
			this.log.debug("Page Size: " + pageSize);
			this.log.debug("Max results calculados: " + query.getMaxResults());
			this.log.debug("First result calculado: " + query.getFirstResult());
		}

		return query.getResultList();
	}

	public Integer totalRecebidas(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros) {
		String hql = "SELECT COUNT(1) FROM Mensagem e ";
		hql += " JOIN e.destinatarios d ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND d.destinatario.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND d.status <> :p_status";

		if (filtros.containsKey("assuntoDescricao")) {
			hql += " AND e.assuntoDescricao LIKE :p_assuntoDescricao";
		}

		if (filtros.containsKey("textoMensagem")) {
			hql += " AND e.textoMensagem LIKE :p_textoMensagem";
		}

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagemDestinatario.EXCLUIDA);

		if (filtros.containsKey("assuntoDescricao")) {
			query.setParameter("p_assuntoDescricao", "%" + filtros.get("assuntoDescricao") + "%");
		}

		if (filtros.containsKey("textoMensagem")) {
			query.setParameter("p_textoMensagem", "%" + filtros.get("textoMensagem") + "%");
		}

		final int count = query.getSingleResult().intValue();

		return count;
	}

	public Integer totalRecebidasNaoLidas(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros) {
		String hql = "SELECT COUNT(1) FROM Mensagem e ";
		hql += " JOIN e.destinatarios d ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND d.destinatario.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND d.status = :p_status";

		if (filtros.containsKey("assuntoDescricao")) {
			hql += " AND e.assuntoDescricao LIKE :p_assuntoDescricao";
		}

		if (filtros.containsKey("textoMensagem")) {
			hql += " AND e.textoMensagem LIKE :p_textoMensagem";
		}

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagemDestinatario.NAO_LIDA);

		if (filtros.containsKey("assuntoDescricao")) {
			query.setParameter("p_assuntoDescricao", "%" + filtros.get("assuntoDescricao") + "%");
		}

		if (filtros.containsKey("textoMensagem")) {
			query.setParameter("p_textoMensagem", "%" + filtros.get("textoMensagem") + "%");
		}

		final int count = query.getSingleResult().intValue();

		return count;
	}

	public Integer totalEnviadas(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros) {
		String hql = "SELECT COUNT(1) FROM Mensagem e ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND e.autor.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND e.status = :p_status";

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagem.ENVIADA);
		final int count = query.getSingleResult().intValue();

		return count;
	}

	public Integer totalRascunhos(IEmpresa empresa, IFuncionario funcionario, Map<String, String> filtros) {
		String hql = "SELECT COUNT(1) FROM Mensagem e ";
		hql += " WHERE 1 = 1 ";

		// Build SQL Filtro
		hql += " AND e.autor.id = :p_funcionario";
		hql += " AND e.empresa.id = :p_empresa";
		hql += " AND e.status = :p_status";

		final TypedQuery<Number> query = this.getManager().createQuery(hql, Number.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_empresa", empresa.getId());
		query.setParameter("p_status", StatusMensagem.RASCUNHO);
		final int count = query.getSingleResult().intValue();

		return count;
	}

}
