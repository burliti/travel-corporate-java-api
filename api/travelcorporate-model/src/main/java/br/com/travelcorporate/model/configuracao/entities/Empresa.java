package br.com.travelcorporate.model.configuracao.entities;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.core.vo.IgnoreEmpresa;
import br.com.travelcorporate.model.cadastros.entities.CentroCusto;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.converters.FlagConverter;

@Entity
@IgnoreEmpresa
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Empresa extends AbstractEntity<Integer> implements IEmpresa {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_empresa", sequenceName = "seq_empresa", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_empresa")
	@Column(name = "id_empresa")
	private Integer id;

	@NotEmpty(message = "Fantasia é obrigatório")
	private String fantasia;

	@NotEmpty(message = "Nome da empresa é obrigatório")
	private String nome;

	@NotNull(message = "Principal é obrigatório")
	@Convert(converter = FlagConverter.class)
	private Flag principal;

	@ManyToOne
	@JoinColumn(name = "id_empresa_principal")
	private Empresa empresaPrincipal;

	@Column(name = "chave_acesso")
	private String chaveEmpresa;

	@ManyToOne
	@JoinColumn(name = "id_centro_custo")
	private CentroCusto centroCusto;

	@Column(name = "logo")
	private String logo;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getFantasia() {
		return this.fantasia;
	}

	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}

	@Override
	public String getNome() {
		return this.nome;
	}

	@Override
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Flag getPrincipal() {
		return this.principal;
	}

	public void setPrincipal(Flag principal) {
		this.principal = principal;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
	}

	@Override
	public IEmpresa getEmpresa() {
		return null;
	}

	public String getChaveEmpresa() {
		return this.chaveEmpresa;
	}

	public void setChaveEmpresa(String chaveEmpresa) {
		this.chaveEmpresa = chaveEmpresa;
	}

	@Override
	public Empresa getEmpresaPrincipal() {
		return this.empresaPrincipal;
	}

	public void setEmpresaPrincipal(Empresa empresaPrincipal) {
		this.empresaPrincipal = empresaPrincipal;
	}

	@Override
	public String getLogo() {
		return this.logo;
	}

	@Override
	public InputStream getLogoStream() {
		if (this.getLogo() == null || this.getLogo().trim().isEmpty()) {
			return null;
		}

		return new ByteArrayInputStream(DatatypeConverter.parseBase64Binary(this.getLogo().substring(this.getLogo().indexOf(",") + 1)));
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public CentroCusto getCentroCusto() {
		return this.centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}
}