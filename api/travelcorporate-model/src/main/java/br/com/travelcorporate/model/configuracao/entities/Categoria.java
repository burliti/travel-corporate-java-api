package br.com.travelcorporate.model.configuracao.entities;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;
import br.com.travelcorporate.model.geral.enums.converters.TipoLancamentoConverter;

@Entity
public class Categoria extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_categoria", sequenceName = "seq_categoria", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_categoria")
	@Column(name = "id_categoria")
	private Integer id;

	@NotNull(message = "Status é obrigatório")
	@Column(name = "status")
	@Convert(converter = StatusConverter.class)
	private Status status;

	@NotNull(message = "Flag interno é obrigatório!")
	@Column(name = "flag_interna")
	private Flag interna;

	@NotEmpty(message = "Nome da categoria é obrigatória")
	private String nome;

	@NotNull(message = "Tipo de lançamento é obrigatório")
	@Column(name = "tipo_lancamento_categoria")
	@Convert(converter = TipoLancamentoConverter.class)
	private TipoLancamento tipo;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Flag getInterna() {
		return this.interna;
	}

	public void setInterna(Flag interna) {
		this.interna = interna;
	}

	public TipoLancamento getTipo() {
		return this.tipo;
	}

	public void setTipo(TipoLancamento tipo) {
		this.tipo = tipo;
	}
}