package br.com.travelcorporate.model.configuracao.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.configuracao.dao.ConfiguracaoAprovacaoViagemDao;
import br.com.travelcorporate.model.configuracao.entities.ConfiguracaoAprovacaoViagem;

@Stateless
public class ConfiguracaoAprovacaoViagemService extends AbstractService<Integer, ConfiguracaoAprovacaoViagem, ConfiguracaoAprovacaoViagemDao> {

	@Inject
	private EmpresaService empresaService;

	@Override
	public void doAntesSalvar(ConfiguracaoAprovacaoViagem vo) throws BusinessException {
		super.doAntesSalvar(vo);

		if (vo.getFilial() != null && vo.getFilial().getId() != null) {
			vo.setFilial(this.empresaService.buscar(vo.getFilial()));
		} else {
			vo.setFilial(null);
		}

		vo.getFuncionarios().forEach((f) -> {
			f.setEmpresa(this.getSessao().getEmpresa());
			f.setConfiguracao(vo);
		});
	}
}
