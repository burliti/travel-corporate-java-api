package br.com.travelcorporate.model.lancamentos.service;

import java.util.Calendar;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.geral.enums.StatusDevolucaoViagem;
import br.com.travelcorporate.model.lancamentos.dao.DevolucaoViagemDao;
import br.com.travelcorporate.model.lancamentos.entities.DevolucaoViagem;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class DevolucaoViagemService extends AbstractService<Integer, DevolucaoViagem, DevolucaoViagemDao> {

	@Inject
	private LancamentoService lancamentoService;

	public DevolucaoViagem criarDevolucao(Viagem viagem, Lancamento lancamentoDebito) throws BusinessException {
		DevolucaoViagem devolucao = new DevolucaoViagem();

		devolucao.setFuncionarioLancamento(viagem.getFuncionario());
		devolucao.setFuncionarioDevolucao((Funcionario) this.getSessao().getFuncionario());
		devolucao.setDataLancamento(Calendar.getInstance());
		devolucao.setLancamentoDebito(this.lancamentoService.buscar(lancamentoDebito));
		devolucao.setSequencial(this.getDao().getNextSequencialEmpresa(this.getSessao().getEmpresa()));
		devolucao.setValorDevolucao(viagem.getFechamento().getSaldoFinal().abs());
		devolucao.setStatus(StatusDevolucaoViagem.EM_ABERTO);
		devolucao.setViagem(viagem);

		return this.inserir(devolucao);
	}

}
