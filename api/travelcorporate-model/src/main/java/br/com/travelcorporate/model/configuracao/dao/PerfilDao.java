package br.com.travelcorporate.model.configuracao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Perfil;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.tabelas.entities.AcaoRecurso;
import br.com.travelcorporate.model.tabelas.entities.Recurso;

@Stateless
public class PerfilDao extends AbstractDao<Integer, Perfil> {

	public List<Perfil> getAtivos(IEmpresa empresa) {

		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());
		filtros.put("empresa.id", empresa.getId().toString());

		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}

	public void removerRecursosPermissao(Recurso recurso) {
		String sql = "DELETE FROM perfil_recurso p \n";
		sql += " WHERE p.id_recurso = " + recurso.getId();

		Query query = this.getManager().createNativeQuery(sql);

		int executeUpdate = query.executeUpdate();

		this.log.debug("Foram excluídos " + executeUpdate + " registros.");

		this.getManager().flush();

	}

	public void removerAcoesPermissao(Recurso recurso) {
		if (recurso.getAcoes() == null || recurso.getAcoes().size() <= 0) {
			return;
		}

		String ids = "";
		for (AcaoRecurso acaoRecurso : recurso.getAcoes()) {
			ids += acaoRecurso.getId() + ", ";
		}

		ids = ids.substring(0, ids.length() - 2);

		String sql = "DELETE FROM perfil_acao_recurso p \n";
		sql += " WHERE p.id_acao_recurso IN (" + ids + ")";

		Query query = this.getManager().createNativeQuery(sql);

		query.executeUpdate();

		this.getManager().flush();
	}
}
