package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.StatusMensagem;

@Converter(autoApply = true)
public class StatusMensagemConverter implements AttributeConverter<StatusMensagem, Integer> {

	@Override
	public Integer convertToDatabaseColumn(StatusMensagem status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public StatusMensagem convertToEntityAttribute(Integer source) {
		return StatusMensagem.fromValue(source);
	}
}