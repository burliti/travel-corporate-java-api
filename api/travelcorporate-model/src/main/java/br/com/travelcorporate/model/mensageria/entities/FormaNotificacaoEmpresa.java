package br.com.travelcorporate.model.mensageria.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@Table(name = "forma_notificacao_empresa")
public class FormaNotificacaoEmpresa extends AbstractEntity<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_forma_notificacao_empresa", sequenceName = "seq_forma_notificacao_empresa", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_forma_notificacao_empresa")
	@Column(name = "id_forma_notificacao_empresa")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "id_forma_notificacao")
	private FormaNotificacao formaNotificacao;

	@NotEmpty
	@Convert(converter = StatusConverter.class)
	private Status status;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public FormaNotificacao getFormaNotificacao() {
		return formaNotificacao;
	}

	public void setFormaNotificacao(FormaNotificacao formaNotificacao) {
		this.formaNotificacao = formaNotificacao;
	}
}
