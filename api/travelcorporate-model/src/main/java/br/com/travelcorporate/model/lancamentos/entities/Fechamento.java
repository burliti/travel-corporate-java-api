package br.com.travelcorporate.model.lancamentos.entities;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.DestinoSaldo;
import br.com.travelcorporate.model.geral.enums.converters.DestinoSaldoConverter;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Fechamento extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_fechamento", sequenceName = "seq_fechamento", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_fechamento")
	@Column(name = "id_fechamento")
	private Integer id;

	@JoinColumn(name = "id_funcionario_fechamento")
	@ManyToOne
	private Funcionario funcionarioFechamento;

	@NotNull(message = "Data de cadastro é obrigatório")
	@Column(name = "data_hora_cadastro", updatable = false)
	private Calendar dataHoraCadastro;

	@Column(name = "data_hora_enviado")
	private Calendar dataHoraEnviado;

	@Column(name = "data_hora_aprovacao")
	private Calendar dataHoraAprovacao;

	@Column(name = "data_hora_rejeicao")
	private Calendar dataHoraRejeicao;

	@Column(name = "email_destino")
	private String emailDestino;

	@JoinColumn(name = "id_funcionario_recebimento")
	@ManyToOne
	private Funcionario funcionarioRecebimento;

	@JoinColumn(name = "id_funcionario_aprovacao")
	@ManyToOne
	private Funcionario funcionarioAprovacao;

	@JoinColumn(name = "id_funcionario_rejeicao")
	@ManyToOne
	private Funcionario funcionarioRejeicao;

	@Column(name = "saldo_anterior")
	@NotNull
	private BigDecimal saldoAnterior;

	@NotNull
	@Column(name = "total_debitos")
	private BigDecimal totalDebitos;

	@NotNull
	@Column(name = "total_creditos")
	private BigDecimal totalCreditos;

	@NotNull
	@Column(name = "saldo_final")
	private BigDecimal saldoFinal;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Column(name = "motivo_rejeicao")
	private String motivoRejeicao;

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_viagem")
	private Viagem viagem;

	@Convert(converter = DestinoSaldoConverter.class)
	@Column(name = "fechamento_destino_saldo")
	private DestinoSaldo destinoSaldo;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Funcionario getFuncionarioFechamento() {
		return this.funcionarioFechamento;
	}

	public void setFuncionarioFechamento(Funcionario funcionarioFechamento) {
		this.funcionarioFechamento = funcionarioFechamento;
	}

	@JsonGetter("dataHoraCadastro")
	public String getDataHoraCadastroFormatada() {
		if (this.getDataHoraCadastro() != null) {
			return FormatUtils.formatDateTime(this.getDataHoraCadastro());
		}
		return null;
	}

	public Calendar getDataHoraCadastro() {
		return this.dataHoraCadastro;
	}

	@JsonSetter("dataHoraCadastro")
	public void setDataHoraCadastro(String dataHoraCadastro) {
		this.setDataHoraCadastro(FormatUtils.parseDateTime(dataHoraCadastro));
	}

	public void setDataHoraCadastro(Calendar dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	@JsonGetter("dataHoraEnviado")
	public String getDataHoraEnviadoFormatada() {
		return FormatUtils.formatDateTime(this.getDataHoraEnviado());
	}

	public Calendar getDataHoraEnviado() {
		return this.dataHoraEnviado;
	}

	@JsonSetter("dataHoraEnviado")
	public void setDataHoraEnviado(String dataHoraEnviado) {
		this.setDataHoraEnviado(FormatUtils.parseDateTime(dataHoraEnviado));
	}

	public void setDataHoraEnviado(Calendar dataHoraEnviado) {
		this.dataHoraEnviado = dataHoraEnviado;
	}

	public String getEmailDestino() {
		return this.emailDestino;
	}

	public void setEmailDestino(String emailDestino) {
		this.emailDestino = emailDestino;
	}

	public Funcionario getFuncionarioRecebimento() {
		return this.funcionarioRecebimento;
	}

	public void setFuncionarioRecebimento(Funcionario funcionarioRecebimento) {
		this.funcionarioRecebimento = funcionarioRecebimento;
	}

	public Funcionario getFuncionarioAprovacao() {
		return this.funcionarioAprovacao;
	}

	public void setFuncionarioAprovacao(Funcionario funcionarioAprovacao) {
		this.funcionarioAprovacao = funcionarioAprovacao;
	}

	public BigDecimal getSaldoAnterior() {
		if (this.saldoAnterior == null) {
			this.saldoAnterior = BigDecimal.ZERO;
		}
		return this.saldoAnterior;
	}

	public void setSaldoAnterior(BigDecimal saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}

	public BigDecimal getTotalDebitos() {
		return this.totalDebitos;
	}

	public void setTotalDebitos(BigDecimal totalDebitos) {
		this.totalDebitos = totalDebitos;
	}

	public BigDecimal getTotalCreditos() {
		return this.totalCreditos;
	}

	public void setTotalCreditos(BigDecimal totalCreditos) {
		this.totalCreditos = totalCreditos;
	}

	public BigDecimal getSaldoFinal() {
		if (this.saldoFinal == null) {
			this.saldoFinal = BigDecimal.ZERO;
		}
		return this.saldoFinal;
	}

	public void setSaldoFinal(BigDecimal saldoFinal) {
		this.saldoFinal = saldoFinal;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public String getMotivoRejeicao() {
		return this.motivoRejeicao;
	}

	public void setMotivoRejeicao(String motivoRejeicao) {
		this.motivoRejeicao = motivoRejeicao;
	}

	@JsonGetter("dataHoraAprovacao")
	public String getDataHoraAprovacaoFormatada() {
		if (this.getDataHoraAprovacao() != null) {
			return FormatUtils.formatDateTime(this.getDataHoraAprovacao());
		}
		return null;
	}

	public Calendar getDataHoraAprovacao() {
		return this.dataHoraAprovacao;
	}

	public void setDataHoraAprovacao(String dataHoraAprovacao) {
	}

	public void setDataHoraAprovacao(Calendar dataHoraAprovacao) {
		this.dataHoraAprovacao = dataHoraAprovacao;
	}

	@JsonGetter("dataHoraRejeicao")
	public String getDataHoraRejeicaoFormatada() {
		if (this.getDataHoraRejeicao() != null) {
			return FormatUtils.formatDateTime(this.getDataHoraRejeicao());
		}
		return null;
	}

	public Calendar getDataHoraRejeicao() {
		return this.dataHoraRejeicao;
	}

	public void setDataHoraRejeicao(String dataHoraRejeicao) {
	}

	public void setDataHoraRejeicao(Calendar dataHoraRejeicao) {
		this.dataHoraRejeicao = dataHoraRejeicao;
	}

	public Funcionario getFuncionarioRejeicao() {
		return this.funcionarioRejeicao;
	}

	public void setFuncionarioRejeicao(Funcionario funcionarioRejeicao) {
		this.funcionarioRejeicao = funcionarioRejeicao;
	}

	public Viagem getViagem() {
		return this.viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public DestinoSaldo getDestinoSaldo() {
		return this.destinoSaldo;
	}

	public void setDestinoSaldo(DestinoSaldo destinoSaldo) {
		this.destinoSaldo = destinoSaldo;
	}
}
