package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.TipoPessoa;

@Converter(autoApply = true)
public class TipoPessoaConverter implements AttributeConverter<TipoPessoa, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoPessoa tipoPessoa) {
		if (tipoPessoa == null) {
			return null;
		}
		return tipoPessoa.getValor();
	}

	@Override
	public TipoPessoa convertToEntityAttribute(Integer value) {
		if (value == null) {
			return null;
		}
		return TipoPessoa.fromValue(value.toString());
	}
}