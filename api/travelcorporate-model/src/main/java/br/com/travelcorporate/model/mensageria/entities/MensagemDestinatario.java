package br.com.travelcorporate.model.mensageria.entities;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.StatusMensagemDestinatario;
import br.com.travelcorporate.model.geral.enums.converters.StatusMensagemDestinatarioConverter;

@Entity
@Table(name = "mensagem_destinatarios")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class MensagemDestinatario extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_mensagem_destinatarios", sequenceName = "seq_mensagem_destinatarios", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_mensagem_destinatarios")
	@Column(name = "id_mensagem_destinatario")
	private Integer id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_mensagem")
	private Mensagem mensagem;

	@NotNull(message = " Funcionário é obrigatório")
	@ManyToOne
	@JoinColumn(name = "id_funcionario")
	private Funcionario destinatario;

	@NotNull(message = "Status é obrigatório")
	@Convert(converter = StatusMensagemDestinatarioConverter.class)
	private StatusMensagemDestinatario status;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public Mensagem getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public Funcionario getDestinatario() {
		return this.destinatario;
	}

	public void setDestinatario(Funcionario destinatario) {
		this.destinatario = destinatario;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public StatusMensagemDestinatario getStatus() {
		return this.status;
	}

	public void setStatus(StatusMensagemDestinatario status) {
		this.status = status;
	}
}
