package br.com.travelcorporate.model.mensageria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.com.travelcorporate.core.utils.FormatUtils;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.StatusMensagem;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MensagemDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private AssuntoDTO assunto;

	private String assuntoDescricao;

	private FuncionarioDTO autor;

	private Flag mensagemSistema;

	private Flag permiteResponder;

	private Flag permiteEncaminhar;

	private Flag exigeConfirmacao;

	private Flag bloqueiaSemResponder;

	private String textoBotaoConfirmacao;

	private MensagemDTO mensagemOrigemResposta;

	private MensagemDTO mensagemOrigemEncaminhada;

	private StatusMensagem status;

	private String textoMensagem;

	private Calendar dataHoraCriacao;

	private Calendar dataHoraEnvio;

	private Calendar dataHoraExclusao;

	private List<MensagemDestinatarioDTO> destinatarios;

	private List<MensagemFormaNotificacaoDTO> formasNotificacao;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AssuntoDTO getAssunto() {
		return this.assunto;
	}

	public void setAssunto(AssuntoDTO assunto) {
		this.assunto = assunto;
	}

	public String getAssuntoDescricao() {
		return this.assuntoDescricao;
	}

	public void setAssuntoDescricao(String assuntoDescricao) {
		this.assuntoDescricao = assuntoDescricao;
	}

	public FuncionarioDTO getAutor() {
		return this.autor;
	}

	public void setAutor(FuncionarioDTO autor) {
		this.autor = autor;
	}

	public Flag getMensagemSistema() {
		return this.mensagemSistema;
	}

	public void setMensagemSistema(Flag mensagemSistema) {
		this.mensagemSistema = mensagemSistema;
	}

	public Flag getPermiteResponder() {
		return this.permiteResponder;
	}

	public void setPermiteResponder(Flag permiteResponder) {
		this.permiteResponder = permiteResponder;
	}

	public Flag getPermiteEncaminhar() {
		return this.permiteEncaminhar;
	}

	public void setPermiteEncaminhar(Flag permiteEncaminhar) {
		this.permiteEncaminhar = permiteEncaminhar;
	}

	public Flag getExigeConfirmacao() {
		return this.exigeConfirmacao;
	}

	public void setExigeConfirmacao(Flag exigeConfirmacao) {
		this.exigeConfirmacao = exigeConfirmacao;
	}

	public Flag getBloqueiaSemResponder() {
		return this.bloqueiaSemResponder;
	}

	public void setBloqueiaSemResponder(Flag bloqueiaSemResponder) {
		this.bloqueiaSemResponder = bloqueiaSemResponder;
	}

	public String getTextoBotaoConfirmacao() {
		return this.textoBotaoConfirmacao;
	}

	public void setTextoBotaoConfirmacao(String textoBotaoConfirmacao) {
		this.textoBotaoConfirmacao = textoBotaoConfirmacao;
	}

	public MensagemDTO getMensagemOrigemResposta() {
		return this.mensagemOrigemResposta;
	}

	public void setMensagemOrigemResposta(MensagemDTO mensagemOrigemResposta) {
		this.mensagemOrigemResposta = mensagemOrigemResposta;
	}

	public MensagemDTO getMensagemOrigemEncaminhada() {
		return this.mensagemOrigemEncaminhada;
	}

	public void setMensagemOrigemEncaminhada(MensagemDTO mensagemOrigemEncaminhada) {
		this.mensagemOrigemEncaminhada = mensagemOrigemEncaminhada;
	}

	public String getTextoMensagem() {
		return this.textoMensagem;
	}

	public void setTextoMensagem(String textoMensagem) {
		this.textoMensagem = textoMensagem;
	}

	public List<MensagemDestinatarioDTO> getDestinatarios() {
		if (this.destinatarios == null) {
			this.destinatarios = new ArrayList<>();
		}
		return this.destinatarios;
	}

	public void setDestinatarios(List<MensagemDestinatarioDTO> destinatarios) {
		this.destinatarios = destinatarios;
	}

	public List<MensagemFormaNotificacaoDTO> getFormasNotificacao() {
		if (this.formasNotificacao == null) {
			this.formasNotificacao = new ArrayList<>();
		}
		return this.formasNotificacao;
	}

	public void setFormasNotificacao(List<MensagemFormaNotificacaoDTO> formasNotificacao) {
		this.formasNotificacao = formasNotificacao;
	}

	public Calendar getDataHoraCriacao() {
		return this.dataHoraCriacao;
	}

	public void setDataHoraCriacao(Calendar dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	@JsonGetter("dataHoraCriacao")
	public String getDataHoraCriacaoFormatada() {
		return FormatUtils.formatDateTime(this.getDataHoraCriacao());
	}

	@JsonSetter("dataHoraCriacao")
	public void setDataHoraCriacao(String dataHoraCriacao) {
		this.setDataHoraCriacao(FormatUtils.parseDateTime(dataHoraCriacao));
	}

	public Calendar getDataHoraEnvio() {
		return this.dataHoraEnvio;
	}

	public void setDataHoraEnvio(Calendar dataHoraEnvio) {
		this.dataHoraEnvio = dataHoraEnvio;
	}

	public Calendar getDataHoraExclusao() {
		return this.dataHoraExclusao;
	}

	public void setDataHoraExclusao(Calendar dataHoraExclusao) {
		this.dataHoraExclusao = dataHoraExclusao;
	}

	public StatusMensagem getStatus() {
		return this.status;
	}

	public void setStatus(StatusMensagem status) {
		this.status = status;
	}
}
