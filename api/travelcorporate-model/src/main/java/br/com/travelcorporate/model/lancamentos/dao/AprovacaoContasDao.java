package br.com.travelcorporate.model.lancamentos.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class AprovacaoContasDao extends AbstractDao<Integer, Viagem> {

	@Override
	public String getCustomFilter() {
		return " AND e.status IN (:p_status_fixo) ";
	}

	@Override
	public void setCustomParameters(Query query) {
		List<StatusViagem> list = new ArrayList<>();

		list.add(StatusViagem.FINALIZADA);
		list.add(StatusViagem.RECEBIDO_FINANCEIRO);
		list.add(StatusViagem.APROVADA);
		list.add(StatusViagem.REJEITADA);

		query.setParameter("p_status_fixo", list);
	}
}
