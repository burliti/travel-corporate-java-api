package br.com.travelcorporate.model.tabelas.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.tabelas.entities.Cidade;

@Stateless
public class CidadeDao extends AbstractDao<Integer, Cidade> {

	public Cidade getByNomeAndUf(String ufSigla, String nome) {
		final String sql = "SELECT e FROM " + getVOClass().getSimpleName() + " e WHERE e.nome = :p_nome AND e.estado.sigla = :p_sigla ";

		final TypedQuery<Cidade> query = getManager().createQuery(sql, getVOClass());

		query.setParameter("p_sigla", ufSigla);
		query.setParameter("p_nome", nome);

		final List<Cidade> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

}
