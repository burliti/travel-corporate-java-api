package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.TipoVeiculo;

@Converter(autoApply = true)
public class TipoVeiculoConverter implements AttributeConverter<TipoVeiculo, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoVeiculo tipoVeiculo) {
		if (tipoVeiculo == null) {
			return null;
		}
		return tipoVeiculo.getValor();
	}

	@Override
	public TipoVeiculo convertToEntityAttribute(Integer value) {
		if (value == null) {
			return null;
		}
		return TipoVeiculo.fromValue(value.toString());
	}

}
