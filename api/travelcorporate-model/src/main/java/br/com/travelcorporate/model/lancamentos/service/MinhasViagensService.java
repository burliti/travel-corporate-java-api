package br.com.travelcorporate.model.lancamentos.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.ServletContext;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dto.ClienteDTO;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.ProjetoDTO;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.geral.enums.StatusViagem;
import br.com.travelcorporate.model.lancamentos.dao.ViagemDao;
import br.com.travelcorporate.model.lancamentos.dto.FechamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.LancamentoDTO;
import br.com.travelcorporate.model.lancamentos.dto.ReciboDTO;
import br.com.travelcorporate.model.lancamentos.dto.ViagemDTO;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;

@Stateless
public class MinhasViagensService extends AbstractService<Integer, Viagem, ViagemDao> {

	@Inject
	private LancamentoService lancamentoService;

	@Inject
	private ViagemService viagemService;

	@Inject
	private FechamentoService fechamentoService;

	@Inject
	ServletContext servletContext;

	@Override
	public void doAntesConsultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		super.doAntesConsultar(filtros, pageSize, pageNumber, order);

		filtros.put("funcionario$id", this.getSessao().getFuncionario().getId().toString());
	}

	@Override
	public void doAntesInserir(Viagem vo) throws BusinessException {
		super.doAntesInserir(vo);

		vo.setFuncionario((Funcionario) this.getSessao().getFuncionario());

		if (vo.getLancamentos() != null) {
			for (Lancamento lancamento : vo.getLancamentos()) {
				this.lancamentoService.doAntesInserir(lancamento);
			}
		}

		if (vo.getStatus() == null) {
			vo.setStatus(StatusViagem.ANDAMENTO);
		}

		vo.setSequencial(this.viagemService.getNextSequencialEmpresa());
	}

	@Override
	public Viagem doDepoisBuscar(Viagem vo) {
		if (vo.getFuncionario().getId().equals(this.getSessao().getFuncionario().getId())) {
			return super.doDepoisBuscar(vo);
		}
		return null;
	}

	public Viagem finalizar(Viagem vo) throws BusinessException {
		// Atualiza os dados
		vo = this.buscar(vo);

		if (vo.getLancamentos() == null || vo.getLancamentos().size() <= 0) {
			throw new BusinessException("Nenhum lançamento para este relatório de viagem!");
		}

		if (vo.getStatus() != StatusViagem.ANDAMENTO && vo.getStatus() != StatusViagem.REJEITADA) {
			throw new BusinessException("A viagem não pode ser finalizada, pois não está em andamento!");
		}

		vo.setStatus(StatusViagem.FINALIZADA);
		vo.getFechamento().setDataHoraEnviado(Calendar.getInstance());

		this.viagemService.calcularSaldos(vo);

		return this.atualizar(vo);
	}

	@Override
	public void doAntesSalvar(Viagem vo) throws BusinessException {
		super.doAntesSalvar(vo);

		this.viagemService.doAntesSalvar(vo);

		for (Lancamento lancamento : vo.getLancamentos()) {

			this.lancamentoService.doAntesSalvar(lancamento);

			if (lancamento.getFuncionario() == null) {
				lancamento.setFuncionario((Funcionario) this.getSessao().getFuncionario());
			}

			if (lancamento.getFuncionarioLancamento() == null) {
				lancamento.setFuncionarioLancamento((Funcionario) this.getSessao().getFuncionario());
			}

			lancamento.setViagem(vo);
		}

		if (vo.getFechamento() == null) {
			vo.setFechamento(new Fechamento());
		}

		vo.getFechamento().setViagem(vo);

		if (vo.getFechamento().getDataHoraCadastro() == null) {
			vo.getFechamento().setDataHoraCadastro(Calendar.getInstance());
		}

		if (vo.getFechamento().getFuncionarioFechamento() == null) {
			vo.getFechamento().setFuncionarioFechamento((Funcionario) this.getSessao().getFuncionario());
		}

		this.fechamentoService.doAntesSalvar(vo.getFechamento());
	}

	public byte[] relatorioDeViagem(Integer id) {

		String path = this.servletContext.getRealPath("/");
		String caminhoRelatorio = path + "/WEB-INF/classes/reports/rdv/item_relatorio_viagem.jasper";
		String caminhoSubRelatorio = path + "/WEB-INF/classes/reports/rdv/relatorio_viagem.jasper";

		if (!new File(caminhoRelatorio).exists()) {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();

			caminhoRelatorio = classloader.getResource("reports/rdv/item_relatorio_viagem.jasper").getFile();
			caminhoSubRelatorio = classloader.getResource("/reports/rdv/relatorio_viagem.jasper").getFile();
		}

		Viagem viagem = this.buscar(id);

		if (viagem == null) {
			return null;
		}

		// Ordenar os lançamentos por sequencia
		viagem.getLancamentos().sort((o1, o2) -> o1.getSequencial().compareTo(o2.getSequencial()));

		try {
			JRBeanArrayDataSource dataSource = new JRBeanArrayDataSource(new Viagem[] { viagem });

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			HashMap<String, Object> parameters = new HashMap<>();

			parameters.put("SUBREPORT_FILE", caminhoRelatorio);
			parameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));

			JasperPrint print = JasperFillManager.fillReport(caminhoSubRelatorio, parameters, dataSource);
			JasperExportManager.exportReportToPdfStream(print, baos);

			byte[] bytes = baos.toByteArray();

			return bytes;

		} catch (JRException e) {
			e.printStackTrace();
		}

		return null;
	}

	public ViagemDTO convertViagemToDto(final Viagem viagem) {
		ViagemDTO v = new ViagemDTO();
		v.setDataIda(viagem.getDataIda());
		v.setDataVolta(viagem.getDataVolta());
		v.setId(viagem.getId());
		v.setSequencial(viagem.getSequencial());
		v.setStatus(viagem.getStatus());
		v.setDescricao(viagem.getDescricao());
		v.setObjetivo(viagem.getObjetivo());
		v.setObservacao(viagem.getObservacao());

		if (viagem.getCliente() != null) {
			v.setCliente(new ClienteDTO());
			v.getCliente().setId(viagem.getCliente().getId());
			v.getCliente().setNome(viagem.getCliente().getNome());
		}

		if (viagem.getProjeto() != null) {
			v.setProjeto(new ProjetoDTO());
			v.getProjeto().setId(viagem.getProjeto().getId());
			v.getProjeto().setNome(viagem.getProjeto().getNome());
		}

		v.setFuncionario(new FuncionarioDTO());
		v.getFuncionario().setId(viagem.getFuncionario().getId());
		v.getFuncionario().setNome(viagem.getFuncionario().getNome());

		v.setFechamento(new FechamentoDTO());
		v.getFechamento().setId(viagem.getFechamento().getId());
		v.getFechamento().setSaldoAnterior(viagem.getFechamento().getSaldoAnterior());
		v.getFechamento().setSaldoFinal(viagem.getFechamento().getSaldoFinal());
		v.getFechamento().setTotalCreditos(viagem.getFechamento().getTotalCreditos());
		v.getFechamento().setTotalDebitos(viagem.getFechamento().getTotalDebitos());

		for (Lancamento lancamento : viagem.getLancamentos()) {
			LancamentoDTO l = new LancamentoDTO();
			l.setId(lancamento.getId());
			l.setDataInicial(lancamento.getDataInicial());
			l.setDataFinal(lancamento.getDataFinal());
			l.setDataHoraLancamento(lancamento.getDataHoraLancamento());
			l.setObservacoes(lancamento.getObservacoes());
			l.setTipoLancamento(lancamento.getTipoLancamento());
			l.setTipoPeriodo(lancamento.getTipoPeriodo());
			l.setValorLancamento(lancamento.getValorLancamento());
			l.setSequencial(lancamento.getSequencial());

			l.setFuncionario(new FuncionarioDTO());
			l.getFuncionario().setId(lancamento.getFuncionario().getId());
			l.getFuncionario().setNome(lancamento.getFuncionario().getNome());

			l.setFuncionarioLancamento(new FuncionarioDTO());
			l.getFuncionarioLancamento().setId(lancamento.getFuncionarioLancamento().getId());
			l.getFuncionarioLancamento().setNome(lancamento.getFuncionarioLancamento().getNome());

			if (lancamento.getCategoria() != null) {
				l.setCategoria(new CategoriaDTO());
				l.getCategoria().setId(lancamento.getCategoria().getId());
				l.getCategoria().setNome(lancamento.getCategoria().getNome());
				l.getCategoria().setTipo(lancamento.getCategoria().getTipo());
				l.getCategoria().setInterna(lancamento.getCategoria().getInterna());
			}

			if (lancamento.getRecibo() != null) {
				l.setRecibo(new ReciboDTO());
				l.getRecibo().setId(lancamento.getRecibo().getId());
				l.getRecibo().setCpfCnpjFornecedor(lancamento.getRecibo().getCpfCnpjFornecedor());
				l.getRecibo().setDataInserido(lancamento.getRecibo().getDataInserido());
				l.getRecibo().setDataReferencia(lancamento.getRecibo().getDataReferencia());
				l.getRecibo().setFotoAnexoRecibo(lancamento.getRecibo().getFotoAnexoRecibo());
				l.getRecibo().setNomeFornecedor(lancamento.getRecibo().getNomeFornecedor());
				l.getRecibo().setNumeroDocumento(lancamento.getRecibo().getNumeroDocumento());
				l.getRecibo().setTipoFornecedor(lancamento.getRecibo().getTipoFornecedor());
				l.getRecibo().setChaveNfe(lancamento.getRecibo().getChaveNfe());
				l.getRecibo().setUrlNfe(lancamento.getRecibo().getUrlNfe());
				l.getRecibo().setValor(lancamento.getRecibo().getValor());
				l.getRecibo().setFuncionario(new FuncionarioDTO());
				l.getRecibo().getFuncionario().setId(lancamento.getRecibo().getFuncionario().getId());
			}

			v.getLancamentos().add(l);
		}
		return v;
	}
}
