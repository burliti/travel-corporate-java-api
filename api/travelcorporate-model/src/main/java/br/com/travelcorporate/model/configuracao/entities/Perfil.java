package br.com.travelcorporate.model.configuracao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.geral.Node;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;
import br.com.travelcorporate.model.tabelas.entities.AcaoRecurso;
import br.com.travelcorporate.model.tabelas.entities.Recurso;

@Entity
public class Perfil extends AbstractEntity<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_perfil", sequenceName = "seq_perfil", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_perfil")
	@Column(name = "id_perfil")
	private Integer id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "status")
	@Convert(converter = StatusConverter.class)
	private Status status;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@JoinTable(name = "perfil_recurso", joinColumns = @JoinColumn(name = "id_perfil"), inverseJoinColumns = @JoinColumn(name = "id_recurso"))
	@ManyToMany
	private List<Recurso> recursos;

	@JoinTable(name = "perfil_acao_recurso", joinColumns = @JoinColumn(name = "id_perfil"), inverseJoinColumns = @JoinColumn(name = "id_acao_recurso"))
	@ManyToMany
	private List<AcaoRecurso> acoes;

	@Transient
	private List<Node> nodes;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public List<AcaoRecurso> getAcoes() {
		return this.acoes;
	}

	public void setAcoes(List<AcaoRecurso> acoes) {
		this.acoes = acoes;
	}

	public List<Recurso> getRecursos() {
		return this.recursos;
	}

	public void setRecursos(List<Recurso> recursos) {
		this.recursos = recursos;
	}

	public List<Node> getNodes() {
		if (this.nodes == null) {
			this.nodes = new ArrayList<>();
		}
		return this.nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
}
