package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.core.enums.TipoSessao;

@Converter(autoApply = true)
public class TipoSessaoConverter implements AttributeConverter<TipoSessao, Integer> {

	@Override
	public Integer convertToDatabaseColumn(TipoSessao tipoSessao) {
		if (tipoSessao == null) {
			return null;
		}
		return tipoSessao.getValor();
	}

	@Override
	public TipoSessao convertToEntityAttribute(Integer value) {
		if (value == null) {
			return null;
		}
		return TipoSessao.fromValue(value.toString());
	}

}
