package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum StatusViagem {

	/* @formatter:off */
	SOLICITADA(1, "Solicitada"),
	PLANEJAMENTO(2, "Planejamento"),
	APROVADA_VIAGEM(3, "Viagem aprovada"),
	ANDAMENTO(4, "Em Andamento"),
	FINALIZADA(5, "Finalizado"),
	RECEBIDO_FINANCEIRO(6, "Relatório em análise"),
	APROVADA(7, "Relatório aprovado"),
	REJEITADA(8, "Relatório rejeitado");
	/* @formatter:on */

	private String descricao;
	private Integer valor;

	StatusViagem(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static StatusViagem fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final StatusViagem status : StatusViagem.values()) {
			if (status.getValor().toString().equals(source)) {
				return status;
			} else if (status.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return status;
			}
		}
		return null;
	}

	public static StatusViagem fromValue(Integer source) {
		return fromValue(source == null ? null : source.toString());
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
