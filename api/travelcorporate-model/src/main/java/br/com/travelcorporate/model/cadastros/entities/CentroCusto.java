package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@Table(name = "centro_custo")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class CentroCusto extends AbstractEntity<Integer> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_centro_custo", sequenceName = "seq_centro_custo", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_centro_custo")
	@Column(name = "id_centro_custo")
	private Integer id;

	@NotEmpty(message = "Código é obrigatório")
	private String codigo;

	@NotEmpty(message = "Nome é obrigatório")
	private String nome;

	@Convert(converter = StatusConverter.class)
	@NotNull(message = "Status é obrigatório")
	private Status status;

	@JoinColumn(name = "id_centro_custo_pai")
	@ManyToOne
	private CentroCusto pai;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	@NotNull(message = "Empresa é obrigatório")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public CentroCusto getPai() {
		return this.pai;
	}

	public void setPai(CentroCusto pai) {
		this.pai = pai;
	}

	@JsonIgnore
	public String getDescricaoCompleta() {
		return this.getCodigo() + " - " + this.getNome();
	}

}
