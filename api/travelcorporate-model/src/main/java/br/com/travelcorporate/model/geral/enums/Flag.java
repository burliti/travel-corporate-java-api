package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Flag {

	SIM("S", "Sim"), NAO("N", "Não");

	private String descricao;
	private String valor;

	Flag(String valor, String descricao) {
		setValor(valor);
		setDescricao(descricao);
	}

	@JsonCreator
	public static Flag fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final Flag flag : Flag.values()) {
			if (flag.getValor().equalsIgnoreCase(source)) {
				return flag;
			} else if (flag.getDescricao().equalsIgnoreCase(source)) {
				return flag;
			} else if (flag.toString().equalsIgnoreCase(source)) {
				return flag;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return valor;
	}

	private void setValor(String valor) {
		this.valor = valor;
	}

	public boolean isSim() {
		return this == SIM;
	}

	public boolean isNao() {
		return this == NAO;
	}
}