package br.com.travelcorporate.model.configuracao.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(content = Include.NON_EMPTY)
public class ConfiguracaoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<FuncionarioDTO> funcionarios;

	private List<EmpresaDTO> empresas;

	public List<FuncionarioDTO> getFuncionarios() {
		if (this.funcionarios == null) {
			this.funcionarios = new ArrayList<>();
		}
		return this.funcionarios;
	}

	public void setFuncionarios(List<FuncionarioDTO> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public List<EmpresaDTO> getEmpresas() {
		if (this.empresas == null) {
			this.empresas = new ArrayList<>();
		}
		return this.empresas;
	}

	public void setEmpresas(List<EmpresaDTO> empresas) {
		this.empresas = empresas;
	}
}
