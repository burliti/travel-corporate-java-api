package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;

import br.com.travelcorporate.core.enums.TipoSessao;

public class LoginRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String email;

	private String senhaMD5;

	private TipoSessao tipoSessao;

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String login) {
		this.email = login;
	}

	public String getSenhaMD5() {
		return this.senhaMD5;
	}

	public void setSenhaMD5(String senha) {
		this.senhaMD5 = senha;
	}

	public TipoSessao getTipoSessao() {
		return this.tipoSessao;
	}

	public void setTipoSessao(TipoSessao tipoSessao) {
		this.tipoSessao = tipoSessao;
	}

}
