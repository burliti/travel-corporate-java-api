package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoVeiculo {

	EMPRESA(1, "Empresa"), ALUGADO(2, "Alugado"), OUTROS(3, "Outros");

	private String descricao;
	private Integer valor;

	TipoVeiculo(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoVeiculo fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final TipoVeiculo tipoPessoa : TipoVeiculo.values()) {
			if (tipoPessoa.getValor().toString().equals(source.toUpperCase())) {
				return tipoPessoa;
			} else if (tipoPessoa.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return tipoPessoa;
			} else if (tipoPessoa.toString().equalsIgnoreCase(source)) {
				return tipoPessoa;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
