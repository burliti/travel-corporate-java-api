package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.StatusReembolsoViagem;

@Converter(autoApply = true)
public class StatusReembolsoViagemConverter implements AttributeConverter<StatusReembolsoViagem, Integer> {

	@Override
	public Integer convertToDatabaseColumn(StatusReembolsoViagem status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public StatusReembolsoViagem convertToEntityAttribute(Integer source) {
		if (source != null) {
			return StatusReembolsoViagem.fromValue(source.toString());
		}
		return null;
	}
}