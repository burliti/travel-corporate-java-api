package br.com.travelcorporate.model.lancamentos.entities;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.StatusReembolsoViagem;
import br.com.travelcorporate.model.geral.enums.converters.StatusReembolsoViagemConverter;

@Entity
@Table(name = "reembolso_viagem")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler" }, ignoreUnknown = true)
public class ReembolsoViagem extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_reembolso_viagem", sequenceName = "seq_reembolso_viagem", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_reembolso_viagem")
	@Column(name = "id_reembolso_viagem")
	private Integer id;

	@NotNull
	private Integer sequencial;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Column(name = "data_lancamento")
	private Calendar dataLancamento;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_lancamento")
	@NotNull
	private Funcionario funcionarioLancamento;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_reembolso")
	@NotNull
	private Funcionario funcionarioReembolso;

	@ManyToOne
	@NotNull
	@JoinColumn(name = "id_viagem")
	private Viagem viagem;

	@ManyToOne
	@NotNull
	@JoinColumn(name = "id_lancamento_credito")
	private Lancamento lancamentoCredito;

	@Column(name = "valor_reembolso")
	private BigDecimal valorReembolso;

	@Convert(converter = StatusReembolsoViagemConverter.class)
	@NotNull(message = "Status é obrigatório")
	private StatusReembolsoViagem status;

	@Column(name = "data_reembolso")
	private Calendar dataReembolso;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_registro_reembolso")
	private Funcionario funcionarioRegistroReembolso;

	@Column(name = "data_cancelado")
	private Calendar dataCancelamento;

	@ManyToOne
	@JoinColumn(name = "id_funcionario_cancelamento")
	private Funcionario funcionarioCancelamento;

	private String observacoes;

	@Column(name = "motivo_cancelamento")
	private String motivoCancelamento;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Calendar getDataLancamento() {
		return this.dataLancamento;
	}

	public void setDataLancamento(Calendar dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public Integer getSequencial() {
		return this.sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public Funcionario getFuncionarioLancamento() {
		return this.funcionarioLancamento;
	}

	public void setFuncionarioLancamento(Funcionario funcionarioLancamento) {
		this.funcionarioLancamento = funcionarioLancamento;
	}

	public Viagem getViagem() {
		return this.viagem;
	}

	public void setViagem(Viagem viagem) {
		this.viagem = viagem;
	}

	public Calendar getDataCancelamento() {
		return this.dataCancelamento;
	}

	public void setDataCancelamento(Calendar dataCancelamento) {
		this.dataCancelamento = dataCancelamento;
	}

	public Funcionario getFuncionarioCancelamento() {
		return this.funcionarioCancelamento;
	}

	public void setFuncionarioCancelamento(Funcionario funcionarioCancelamento) {
		this.funcionarioCancelamento = funcionarioCancelamento;
	}

	public String getObservacoes() {
		return this.observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getMotivoCancelamento() {
		return this.motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public Funcionario getFuncionarioReembolso() {
		return this.funcionarioReembolso;
	}

	public void setFuncionarioReembolso(Funcionario funcionarioReembolso) {
		this.funcionarioReembolso = funcionarioReembolso;
	}

	public BigDecimal getValorReembolso() {
		return this.valorReembolso;
	}

	public void setValorReembolso(BigDecimal valorReembolso) {
		this.valorReembolso = valorReembolso;
	}

	public StatusReembolsoViagem getStatus() {
		return this.status;
	}

	public void setStatus(StatusReembolsoViagem status) {
		this.status = status;
	}

	public Calendar getDataReembolso() {
		return this.dataReembolso;
	}

	public void setDataReembolso(Calendar dataReembolso) {
		this.dataReembolso = dataReembolso;
	}

	public Funcionario getFuncionarioRegistroReembolso() {
		return this.funcionarioRegistroReembolso;
	}

	public void setFuncionarioRegistroReembolso(Funcionario funcionarioRegistroReembolso) {
		this.funcionarioRegistroReembolso = funcionarioRegistroReembolso;
	}

	public Lancamento getLancamentoCredito() {
		return this.lancamentoCredito;
	}

	public void setLancamentoCredito(Lancamento lancamentoCredito) {
		this.lancamentoCredito = lancamentoCredito;
	}
}
