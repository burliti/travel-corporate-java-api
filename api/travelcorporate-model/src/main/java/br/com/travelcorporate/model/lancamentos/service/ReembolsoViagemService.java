package br.com.travelcorporate.model.lancamentos.service;

import java.util.Calendar;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;
import br.com.travelcorporate.model.geral.enums.StatusReembolsoViagem;
import br.com.travelcorporate.model.lancamentos.dao.ReembolsoViagemDao;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.ReembolsoViagem;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class ReembolsoViagemService extends AbstractService<Integer, ReembolsoViagem, ReembolsoViagemDao> {

	@Inject
	private LancamentoService lancamentoService;

	public ReembolsoViagem criarReembolso(Viagem viagem, Lancamento lancamento) throws BusinessException {
		ReembolsoViagem reembolso = new ReembolsoViagem();

		reembolso.setFuncionarioLancamento(viagem.getFuncionario());
		reembolso.setFuncionarioReembolso((Funcionario) this.getSessao().getFuncionario());
		reembolso.setDataLancamento(Calendar.getInstance());
		reembolso.setLancamentoCredito(this.lancamentoService.buscar(lancamento));
		reembolso.setSequencial(this.getDao().getNextSequencialEmpresa(this.getSessao().getEmpresa()));
		reembolso.setValorReembolso(viagem.getFechamento().getSaldoFinal().abs());
		reembolso.setStatus(StatusReembolsoViagem.EM_ABERTO);
		reembolso.setViagem(viagem);

		return this.inserir(reembolso);
	}

}
