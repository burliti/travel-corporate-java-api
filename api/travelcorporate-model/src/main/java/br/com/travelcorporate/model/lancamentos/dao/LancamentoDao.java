package br.com.travelcorporate.model.lancamentos.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.core.vo.IFuncionario;
import br.com.travelcorporate.model.lancamentos.entities.Fechamento;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;

@Stateless
public class LancamentoDao extends AbstractDao<Integer, Lancamento> {

	public Integer getNextSequencial(IFuncionario funcionario) {

		String sql = "SELECT MAX(e.sequencial) FROM " + this.getVOClass().getSimpleName() + " e WHERE e.funcionario.id = :p_funcionario";

		TypedQuery<Integer> query = this.getManager().createQuery(sql, Integer.class);

		query.setParameter("p_funcionario", funcionario.getId());

		Integer result = query.getSingleResult();

		if (result == null) {
			result = 0;
		}

		return result + 1;
	}

	public List<Lancamento> getLancamentosNaoFechados(IFuncionario funcionario, Fechamento fechamento) {

		String sql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e \n";
		sql += " WHERE e.funcionario.id = :p_funcionario \n";
		sql += "   AND e.id NOT IN (SELECT l.id FROM " + Fechamento.class.getSimpleName() + " f \n";
		sql += " 					  JOIN f.lancamentos l \n";
		sql += "                     WHERE f.id <> :p_fechamento)\n";
		sql += "	ORDER BY e.sequencial DESC";

		TypedQuery<Lancamento> query = this.getManager().createQuery(sql, Lancamento.class);

		query.setParameter("p_funcionario", funcionario.getId());
		query.setParameter("p_fechamento", fechamento.getId());

		return query.getResultList();
	}

	public List<Lancamento> getLancamentosSemViagem(IFuncionario funcionario) {
		String sql = "SELECT e FROM " + this.getVOClass().getSimpleName() + " e \n";
		sql += " WHERE e.funcionario.id = :p_funcionario \n";
		sql += "   AND e.viagem IS NULL \n";
		sql += "	ORDER BY e.sequencial DESC";

		TypedQuery<Lancamento> query = this.getManager().createQuery(sql, Lancamento.class);

		query.setParameter("p_funcionario", funcionario.getId());

		return query.getResultList();
	}
}
