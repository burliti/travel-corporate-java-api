package br.com.travelcorporate.model.cadastros.service;

import java.util.Calendar;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.enums.FormaLogoff;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.dao.SessaoUsuarioExcluidaDao;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuario;
import br.com.travelcorporate.model.cadastros.entities.SessaoUsuarioExcluida;

@Stateless
public class SessaoUsuarioExcluidaService {

	@Inject
	private SessaoUsuarioExcluidaDao dao;

	public void criarSessaoExcluida(SessaoUsuario sessaoUsuario, FormaLogoff formaLogoff, String observacoes) throws BusinessException {
		// Criar o registro da sessao excluida

		final SessaoUsuarioExcluida sessaoExcluida = new SessaoUsuarioExcluida();
		sessaoExcluida.setDataCriacao(sessaoUsuario.getDataCriacao());
		sessaoExcluida.setUltimaInteracao(sessaoUsuario.getUltimaInteracao());
		sessaoExcluida.setEmpresa(sessaoUsuario.getEmpresa());
		sessaoExcluida.setEnderecoIp(sessaoUsuario.getEnderecoIp());
		sessaoExcluida.setHostName(sessaoUsuario.getHostName());
		sessaoExcluida.setFuncionario(sessaoUsuario.getFuncionario());
		sessaoExcluida.setDataLogoff(Calendar.getInstance());
		sessaoExcluida.setFormaLogoff(formaLogoff);
		sessaoExcluida.setIdSessaoOriginal(sessaoUsuario.getId());
		sessaoExcluida.setChaveSessao(sessaoUsuario.getChaveSessao());
		sessaoExcluida.setObservacoes(observacoes);

		dao.inserir(sessaoExcluida, false);
	}
}
