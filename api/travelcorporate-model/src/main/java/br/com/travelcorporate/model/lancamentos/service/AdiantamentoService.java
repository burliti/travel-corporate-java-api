package br.com.travelcorporate.model.lancamentos.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.lancamentos.dao.LancamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;

@Stateless
public class AdiantamentoService extends AbstractService<Integer, Lancamento, LancamentoDao> {

	@Inject
	LancamentoService lancamentoService;

	@Inject
	ViagemService viagemService;

	@Override
	public void doAntesInserir(Lancamento vo) throws BusinessException {
		this.lancamentoService.doAntesInserir(vo);

		if (vo.getSequencial() == null) {
			vo.setSequencial(9999);
		}

		super.doAntesInserir(vo);
	}

	@Override
	public void doDepoisInserir(Lancamento vo) throws BusinessException {
		super.doDepoisInserir(vo);

		// Busca uma viagem em aberto para inserir
		Viagem viagemAberta = this.viagemService.getUltimaViagemAberta(vo.getFuncionario());

		if (viagemAberta != null) {
			// Busca ultimo sequencial
			if (viagemAberta.getLancamentos().size() > 0) {
				viagemAberta.getLancamentos().sort((o1, o2) -> o2.getSequencial().compareTo(o1.getSequencial()));
				Integer sequencial = viagemAberta.getLancamentos().get(0).getSequencial();
				vo.setSequencial(sequencial + 1);
			} else {
				vo.setSequencial(1);
			}

			viagemAberta.getLancamentos().add(vo);
			vo.setViagem(viagemAberta);
			this.viagemService.atualizar(viagemAberta);
		}
	}

	@Override
	public void doAntesSalvar(Lancamento vo) throws BusinessException {
		this.lancamentoService.doAntesSalvar(vo);
		super.doAntesSalvar(vo);
	}
}
