package br.com.travelcorporate.model.configuracao.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.geral.enums.Flag;

@Entity
@Table(name = "configuracao_aprovacao_viagem")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfiguracaoAprovacaoViagem extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_configuracao_aprovacao_viagem", sequenceName = "seq_configuracao_aprovacao_viagem", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_configuracao_aprovacao_viagem")
	@Column(name = "id_configuracao_aprovacao_viagem")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "id_filial", nullable = true)
	private Empresa filial;

	@NotNull(message = "Flag possui aprovação de nível é obrigatório!")
	@Column(name = "flag_aprovacao_nivel")
	private Flag possuiAprovacaoNivel;

	@OneToMany(mappedBy = "configuracao", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<FuncionariosAprovacao> funcionarios;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Empresa getFilial() {
		return this.filial;
	}

	public void setFilial(Empresa filial) {
		this.filial = filial;
	}

	public Flag getPossuiAprovacaoNivel() {
		return this.possuiAprovacaoNivel;
	}

	public void setPossuiAprovacaoNivel(Flag possuiAprovacaoNivel) {
		this.possuiAprovacaoNivel = possuiAprovacaoNivel;
	}

	public List<FuncionariosAprovacao> getFuncionarios() {
		if (this.funcionarios == null) {
			this.funcionarios = new ArrayList<>();
		}
		return this.funcionarios;
	}

	public void setFuncionarios(List<FuncionariosAprovacao> funcionarios) {
		this.funcionarios = funcionarios;
	}

}
