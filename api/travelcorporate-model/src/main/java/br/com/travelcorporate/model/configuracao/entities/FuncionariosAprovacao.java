package br.com.travelcorporate.model.configuracao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;

@Entity
@Table(name = "configuracao_funcionarios_aprovacao")
public class FuncionariosAprovacao extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_configuracao_funcionarios_aprovacao", sequenceName = "seq_configuracao_funcionarios_aprovacao", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_configuracao_funcionarios_aprovacao")
	@Column(name = "id_funcionarios_aprovacao")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull(message = "Funcionário de aprovação é obrigatório")
	@JoinColumn(name = "id_funcionario")
	private Funcionario funcionario;

	@Column(name = "prazo_aprovacao_horas")
	private Integer prazoAprovacaoHoras;

	@NotNull(message = "Ordem é obrigatório!")
	private Integer ordem;

	@ManyToOne
	@JoinColumn(name = "id_configuracao_aprovacao_viagem")
	@NotNull(message = "Configuração pai é obrigatório")
	@JsonIgnore
	private ConfiguracaoAprovacaoViagem configuracao;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	private IEmpresa empresa;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public ConfiguracaoAprovacaoViagem getConfiguracao() {
		return this.configuracao;
	}

	public void setConfiguracao(ConfiguracaoAprovacaoViagem configuracao) {
		this.configuracao = configuracao;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Integer getPrazoAprovacaoHoras() {
		return this.prazoAprovacaoHoras;
	}

	public void setPrazoAprovacaoHoras(Integer prazoAprovacaoHoras) {
		this.prazoAprovacaoHoras = prazoAprovacaoHoras;
	}

	public Integer getOrdem() {
		return this.ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

}
