package br.com.travelcorporate.model.geral.enums.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.travelcorporate.model.geral.enums.StatusMensagemDestinatario;

@Converter(autoApply = true)
public class StatusMensagemDestinatarioConverter implements AttributeConverter<StatusMensagemDestinatario, Integer> {

	@Override
	public Integer convertToDatabaseColumn(StatusMensagemDestinatario status) {
		if (status == null) {
			return null;
		}
		return status.getValor();
	}

	@Override
	public StatusMensagemDestinatario convertToEntityAttribute(Integer source) {
		return StatusMensagemDestinatario.fromValue(source);
	}
}