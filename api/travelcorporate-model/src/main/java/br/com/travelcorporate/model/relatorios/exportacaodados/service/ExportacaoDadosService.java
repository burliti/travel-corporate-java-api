package br.com.travelcorporate.model.relatorios.exportacaodados.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.com.travelcorporate.core.services.ConsultaRequest;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;
import br.com.travelcorporate.model.lancamentos.entities.Viagem;
import br.com.travelcorporate.model.lancamentos.service.AdiantamentoService;
import br.com.travelcorporate.model.lancamentos.service.ViagemService;

@Stateless
public class ExportacaoDadosService {

	@Inject
	private ViagemService viagemService;

	@Inject
	private AdiantamentoService adiantamentoService;

	public byte[] processarExportacaoViagem(ConsultaRequest request) throws IOException {

		List<Viagem> list = this.viagemService.consultar(request.getFiltro(), null, null, request.getOrder());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Viagens");

		AtomicReference<Integer> linha = new AtomicReference<>(0);

		XSSFRow cabecalho = sheet.createRow(linha.get());

		int colunaCabecalho = 0;

		cabecalho.createCell(colunaCabecalho++).setCellValue("Número da viagem");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Descrição");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Status");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Data ida");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Data volta");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Funcionário");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Cliente");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Projeto");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Objetivo");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Observações");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Total Créditos");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Total Débitos");

		linha.set(linha.get() + 1);

		// Cabeçalho
		list.forEach(viagem -> {
			int coluna = 0;

			XSSFRow row = sheet.createRow(linha.get());

			row.createCell(coluna++).setCellValue(viagem.getSequencial());
			row.createCell(coluna++).setCellValue(viagem.getDescricao());
			row.createCell(coluna++).setCellValue(viagem.getStatus().getDescricao());
			row.createCell(coluna++).setCellValue(viagem.getDataIdaFormatada());
			row.createCell(coluna++).setCellValue(viagem.getDataVoltaFormatada());
			row.createCell(coluna++).setCellValue(viagem.getFuncionario().getNome());
			row.createCell(coluna++).setCellValue(viagem.getCliente() != null ? viagem.getCliente().getNome() : "");
			row.createCell(coluna++).setCellValue(viagem.getProjeto() != null ? viagem.getProjeto().getNome() : "");
			row.createCell(coluna++).setCellValue(viagem.getObjetivo());
			row.createCell(coluna++).setCellValue(viagem.getObservacao());
			row.createCell(coluna++).setCellValue(viagem.getFechamento().getTotalCreditos().doubleValue());
			row.createCell(coluna++).setCellValue(viagem.getFechamento().getTotalDebitos().doubleValue());

			linha.set(linha.get() + 1);
		});

		wb.write(baos);
		wb.close();

		return baos.toByteArray();
	}

	public byte[] processarExportacaoLancamento(ConsultaRequest request) throws IOException {

		List<Lancamento> list = this.adiantamentoService.consultar(request.getFiltro(), null, null, request.getOrder());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Viagens");

		AtomicReference<Integer> linha = new AtomicReference<>(0);

		int colunaCabecalho = 0;

		XSSFRow cabecalho = sheet.createRow(linha.get());

		cabecalho.createCell(colunaCabecalho++).setCellValue("Número da viagem");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Descrição");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Status");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Data ida");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Data volta");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Funcionário");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Cliente");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Projeto");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Objetivo");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Observações");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Total Créditos");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Total Débitos");

		cabecalho.createCell(colunaCabecalho++).setCellValue("Número do lançamento");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Tipo de Período");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Data inicial / data de referência");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Data final");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Tipo de lançamento");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Valor");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Valor (com sinal)");
		cabecalho.createCell(colunaCabecalho++).setCellValue("Categoria");

		cabecalho.createCell(colunaCabecalho++).setCellValue("Funcionário do Lançamento");

		linha.set(linha.get() + 1);

		list.forEach(lancamento -> {
			XSSFRow row = sheet.createRow(linha.get());

			Viagem viagem = lancamento.getViagem();

			int coluna = 0;
			row.createCell(coluna++).setCellValue(viagem.getSequencial());
			row.createCell(coluna++).setCellValue(viagem.getDescricao());
			row.createCell(coluna++).setCellValue(viagem.getStatus().getDescricao());
			row.createCell(coluna++).setCellValue(viagem.getDataIdaFormatada());
			row.createCell(coluna++).setCellValue(viagem.getDataVoltaFormatada());
			row.createCell(coluna++).setCellValue(viagem.getFuncionario().getNome());
			row.createCell(coluna++).setCellValue(viagem.getCliente() != null ? viagem.getCliente().getNome() : "");
			row.createCell(coluna++).setCellValue(viagem.getProjeto() != null ? viagem.getProjeto().getNome() : "");
			row.createCell(coluna++).setCellValue(viagem.getObjetivo());
			row.createCell(coluna++).setCellValue(viagem.getObservacao());
			row.createCell(coluna++).setCellValue(viagem.getFechamento().getTotalCreditos().doubleValue());
			row.createCell(coluna++).setCellValue(viagem.getFechamento().getTotalDebitos().doubleValue());
			row.createCell(coluna++).setCellValue(lancamento.getSequencial());
			row.createCell(coluna++).setCellValue(lancamento.getTipoPeriodo().getDescricao());
			row.createCell(coluna++).setCellValue(lancamento.getDataInicialFormatada());
			row.createCell(coluna++).setCellValue(lancamento.getDataFinalFormatada());
			row.createCell(coluna++).setCellValue(lancamento.getTipoLancamento().getDescricao());

			row.createCell(coluna++).setCellValue(lancamento.getValorLancamento().doubleValue());
			row.createCell(coluna++).setCellValue(lancamento.getValorLancamento().multiply(new BigDecimal(lancamento.getTipoLancamento().getMultiplicador())).doubleValue());

			row.createCell(coluna++).setCellValue(lancamento.getCategoria().getNome());
			row.createCell(coluna++).setCellValue(lancamento.getFuncionarioLancamento() != null ? lancamento.getFuncionarioLancamento().getNome() : "");

			linha.set(linha.get() + 1);
		});

		wb.write(baos);
		wb.close();

		return baos.toByteArray();
	}
}
