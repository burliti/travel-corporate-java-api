package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.travelcorporate.core.vo.AbstractEntity;
import br.com.travelcorporate.core.vo.IEmpresa;
import br.com.travelcorporate.model.configuracao.entities.Empresa;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.converters.StatusConverter;

@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Setor extends AbstractEntity<Integer> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_setor", sequenceName = "seq_setor", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_setor")
	@Column(name = "id_setor")
	private Integer id;

	private String nome;

	@Convert(converter = StatusConverter.class)
	@NotNull(message = "Status é obrigatório")
	private Status status;

	@JsonIgnore
	@ManyToOne(targetEntity = Empresa.class)
	@JoinColumn(name = "id_empresa")
	@NotNull(message = "Empresa é obrigatório")
	private IEmpresa empresa;

	@JoinColumn(name = "id_setor_pai")
	@ManyToOne
	private Setor pai;

	@ManyToOne
	@JoinColumn(name = "id_centro_custo")
	private CentroCusto centroCusto;

	@ManyToOne
	@JoinColumn(name = "id_empresa_filial")
	private Empresa filial;

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public IEmpresa getEmpresa() {
		return this.empresa;
	}

	@Override
	public void setEmpresa(IEmpresa empresa) {
		this.empresa = empresa;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Setor getPai() {
		return this.pai;
	}

	public void setPai(Setor pai) {
		this.pai = pai;
	}

	public CentroCusto getCentroCusto() {
		return this.centroCusto;
	}

	public void setCentroCusto(CentroCusto centroCusto) {
		this.centroCusto = centroCusto;
	}

	public Empresa getFilial() {
		return this.filial;
	}

	public void setFilial(Empresa filial) {
		this.filial = filial;
	}
}
