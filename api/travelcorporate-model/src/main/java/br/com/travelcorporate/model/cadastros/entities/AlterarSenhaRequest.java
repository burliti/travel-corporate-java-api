package br.com.travelcorporate.model.cadastros.entities;

import java.io.Serializable;

public class AlterarSenhaRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String senha;

	private String novaSenha;

	private String confirmacaoNovaSenha;

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getConfirmacaoNovaSenha() {
		return confirmacaoNovaSenha;
	}

	public void setConfirmacaoNovaSenha(String confirmacaoNovaSenha) {
		this.confirmacaoNovaSenha = confirmacaoNovaSenha;
	}

}
