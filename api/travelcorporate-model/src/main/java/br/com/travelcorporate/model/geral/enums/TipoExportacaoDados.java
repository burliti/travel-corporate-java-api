package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoExportacaoDados {

	VIAGEM("V", "Viagem"), LANCAMENTO("L", "Lançamento");

	private String descricao;
	private String valor;

	TipoExportacaoDados(String valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static TipoExportacaoDados fromString(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final TipoExportacaoDados tipo : TipoExportacaoDados.values()) {
			if (tipo.getValor().equalsIgnoreCase(source)) {
				return tipo;
			} else if (tipo.getDescricao().equalsIgnoreCase(source)) {
				return tipo;
			} else if (tipo.toString().equalsIgnoreCase(source)) {
				return tipo;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getValor() {
		return this.valor;
	}

	private void setValor(String valor) {
		this.valor = valor;
	}
}
