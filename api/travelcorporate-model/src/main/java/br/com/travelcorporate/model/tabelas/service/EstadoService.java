package br.com.travelcorporate.model.tabelas.service;

import java.util.List;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.tabelas.dao.EstadoDao;
import br.com.travelcorporate.model.tabelas.entities.Estado;

@Stateless
public class EstadoService extends AbstractService<Integer, Estado, EstadoDao> {

	public Estado getBySigla(String sigla) {
		return this.getDao().getBySigla(sigla);
	}

	public void criarEstados() throws BusinessException {
		this.createEstadoIfNotExists("AC", "Acre");
		this.createEstadoIfNotExists("AL", "Alagoas");
		this.createEstadoIfNotExists("AP", "Amapá");
		this.createEstadoIfNotExists("AM", "Amazonas");
		this.createEstadoIfNotExists("BA", "Bahia");
		this.createEstadoIfNotExists("CE", "Ceará");
		this.createEstadoIfNotExists("DF", "Distrito Federal");
		this.createEstadoIfNotExists("ES", "Espírito Santo");
		this.createEstadoIfNotExists("GO", "Goiás");
		this.createEstadoIfNotExists("MA", "Maranhão");
		this.createEstadoIfNotExists("MT", "Mato Grosso");
		this.createEstadoIfNotExists("MS", "Mato Grosso do Sul");
		this.createEstadoIfNotExists("MG", "Minas Gerais");
		this.createEstadoIfNotExists("PA", "Pará");
		this.createEstadoIfNotExists("PB", "Paraíba");
		this.createEstadoIfNotExists("PR", "Paraná");
		this.createEstadoIfNotExists("PE", "Pernambuco");
		this.createEstadoIfNotExists("PI", "Piauí");
		this.createEstadoIfNotExists("RJ", "Rio de Janeiro");
		this.createEstadoIfNotExists("RN", "Rio Grande do Norte");
		this.createEstadoIfNotExists("RS", "Rio Grande do Sul");
		this.createEstadoIfNotExists("RO", "Rondônia");
		this.createEstadoIfNotExists("RR", "Roraima");
		this.createEstadoIfNotExists("SC", "Santa Catarina");
		this.createEstadoIfNotExists("SP", "São Paulo");
		this.createEstadoIfNotExists("SE", "Sergipe");
		this.createEstadoIfNotExists("TO", "Tocantins");
	}

	public void createEstadoIfNotExists(String sigla, String nome) throws BusinessException {
		Estado estado = this.getBySigla(sigla);

		if (estado == null) {
			estado = new Estado();
			estado.setNome(nome);
			estado.setSigla(sigla);

			this.inserir(estado);
		} else {
			estado.setNome(nome);
			estado.setSigla(sigla);

			this.atualizar(estado);
		}
	}

	public List<Estado> getAll() {
		return this.getDao().todos("nome");
	}
}
