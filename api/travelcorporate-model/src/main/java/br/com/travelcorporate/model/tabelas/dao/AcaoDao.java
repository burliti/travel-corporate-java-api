package br.com.travelcorporate.model.tabelas.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.tabelas.entities.Acao;

@Stateless
public class AcaoDao extends AbstractDao<Integer, Acao> {

	public Acao getByNome(String nome) {
		final String sql = "SELECT e FROM " + getVOClass().getSimpleName() + " e WHERE e.nome = :p_nome AND e.status = :p_status";

		final TypedQuery<Acao> query = getManager().createQuery(sql, getVOClass());

		query.setParameter("p_nome", nome);
		query.setParameter("p_status", Status.ATIVO);

		final List<Acao> list = query.getResultList();

		if (list.size() > 0) {
			return list.get(0);
		}

		return null;
	}

}
