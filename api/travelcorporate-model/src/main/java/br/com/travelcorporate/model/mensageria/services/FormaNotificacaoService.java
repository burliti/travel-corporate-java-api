package br.com.travelcorporate.model.mensageria.services;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.mensageria.dao.FormaNotificacaoDao;
import br.com.travelcorporate.model.mensageria.entities.FormaNotificacao;

@Stateless
public class FormaNotificacaoService extends AbstractService<Integer, FormaNotificacao, FormaNotificacaoDao> {

	public void criarFormaNotificacaoPadrao() throws BusinessException {
		this.criar("Push web", "NotDefined", "MENSAGEM_PUSH_WEB", Status.ATIVO);
		this.criar("Push aplicativo", "NotDefined", "MENSAGEM_PUSH_MOBILE", Status.ATIVO);
		this.criar("Email", "NotDefined", "MENSAGEM_EMAIL", Status.ATIVO);
		this.criar("SMS", "NotDefined", "MENSAGEM_SMS", Status.ATIVO);
	}

	public FormaNotificacao getByNome(String nome) {
		return this.getDao().getByNome(nome);
	}

	private void criar(String nome, String classe, String nomeFila, Status status) throws BusinessException {
		FormaNotificacao fm = this.getByNome(nome);

		if (fm == null) {
			fm = new FormaNotificacao();
		}

		fm.setNome(nome);
		fm.setClasse(classe);
		fm.setNomeFila(nomeFila);
		fm.setStatus(status);

		if (fm.getId() == null) {
			this.inserir(fm);
		} else {
			this.atualizar(fm);
		}

	}
}
