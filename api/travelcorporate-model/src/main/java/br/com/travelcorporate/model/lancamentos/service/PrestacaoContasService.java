package br.com.travelcorporate.model.lancamentos.service;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.lancamentos.dao.LancamentoDao;
import br.com.travelcorporate.model.lancamentos.entities.Lancamento;

@Stateless
public class PrestacaoContasService extends AbstractService<Integer, Lancamento, LancamentoDao> {

	@Inject
	private LancamentoService lancamentoService;

	@Override
	public void doAntesConsultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		super.doAntesConsultar(filtros, pageSize, pageNumber, order);

		filtros.put("funcionario$id", this.getSessao().getFuncionario().getId().toString());
	}

	@Override
	public Lancamento doDepoisBuscar(Lancamento vo) {
		if (vo.getFuncionario().getId().equals(this.getSessao().getFuncionario().getId())) {
			return super.doDepoisBuscar(vo);
		}
		return null;
	}

	@Override
	public void doAntesInserir(Lancamento vo) throws BusinessException {
		super.doAntesInserir(vo);

		this.lancamentoService.doAntesInserir(vo);
	}

	@Override
	public void doAntesAtualizar(Lancamento vo) throws BusinessException {
		super.doAntesAtualizar(vo);

		this.lancamentoService.doAntesAtualizar(vo);
	}

	@Override
	public void doAntesSalvar(Lancamento vo) throws BusinessException {
		super.doAntesSalvar(vo);

		this.lancamentoService.doAntesSalvar(vo);
	}
}
