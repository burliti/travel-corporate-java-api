package br.com.travelcorporate.model.configuracao.dao;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.dao.AbstractDao;
import br.com.travelcorporate.model.configuracao.entities.Categoria;

@Stateless
public class CategoriaDao extends AbstractDao<Integer, Categoria> {

}
