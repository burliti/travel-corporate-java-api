package br.com.travelcorporate.model.mensageria.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.geral.enums.StatusMensagemDestinatario;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MensagemDestinatarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private MensagemDTO mensagem;

	private StatusMensagemDestinatario status;

	private FuncionarioDTO destinatario;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MensagemDTO getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(MensagemDTO mensagem) {
		this.mensagem = mensagem;
	}

	public FuncionarioDTO getDestinatario() {
		return this.destinatario;
	}

	public void setDestinatario(FuncionarioDTO destinatario) {
		this.destinatario = destinatario;
	}

	public StatusMensagemDestinatario getStatus() {
		return this.status;
	}

	public void setStatus(StatusMensagemDestinatario status) {
		this.status = status;
	}
}
