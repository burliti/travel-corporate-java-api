package br.com.travelcorporate.model.configuracao.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.core.enums.SimNao;
import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.configuracao.dao.CategoriaDao;
import br.com.travelcorporate.model.configuracao.entities.Categoria;
import br.com.travelcorporate.model.geral.enums.Flag;
import br.com.travelcorporate.model.geral.enums.Status;
import br.com.travelcorporate.model.geral.enums.TipoLancamento;

@Stateless
public class CategoriaService extends AbstractService<Integer, Categoria, CategoriaDao> {

	@Override
	public void doAntesSalvar(Categoria vo) throws BusinessException {

		super.doAntesSalvar(vo);

		if (vo.getInterna() == null) {
			vo.setInterna(Flag.NAO);
		}
	}

	public List<Categoria> getAtivos() {
		return this.getAtivos(null);
	}

	public List<Categoria> getAtivos(TipoLancamento tipo) {
		final Map<String, String> filtros = new HashMap<>();
		final Map<String, String> order = new HashMap<>();

		filtros.put("status", Status.ATIVO.getValor());
		filtros.put("interna", "X");

		if (tipo != null) {
			filtros.put("tipo", tipo.getValor());
		}
		order.put("nome", "ASC");

		return this.consultar(filtros, null, null, order);
	}

	@Override
	public void doAntesConsultar(Map<String, String> filtros, Integer pageSize, Integer pageNumber, Map<String, String> order) {
		if (filtros.get("interna") != null && filtros.get("interna").equals("X")) {
			filtros.remove("interna");
		} else {
			filtros.put("interna", SimNao.NAO.getValor());
		}

		super.doAntesConsultar(filtros, pageSize, pageNumber, order);
	}
}
