package br.com.travelcorporate.model.geral.exceptions;

import br.com.travelcorporate.core.exceptions.BusinessException;
import br.com.travelcorporate.model.cadastros.entities.Funcionario;

public class LoginDuplicadoException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public LoginDuplicadoException(String message) {
		super(message);
	}

	public LoginDuplicadoException(Funcionario funcionario) {
		super("O email '" + funcionario.getEmail() + "' já existe e ainda se encontra ativo!");
	}
}
