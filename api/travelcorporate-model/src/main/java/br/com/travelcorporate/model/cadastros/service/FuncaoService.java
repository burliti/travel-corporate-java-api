package br.com.travelcorporate.model.cadastros.service;

import java.util.List;

import javax.ejb.Stateless;

import br.com.travelcorporate.core.be.AbstractService;
import br.com.travelcorporate.model.cadastros.dao.FuncaoDao;
import br.com.travelcorporate.model.cadastros.entities.Funcao;

@Stateless
public class FuncaoService extends AbstractService<Integer, Funcao, FuncaoDao> {

	public List<Funcao> getAtivos() {
		return this.getDao().getAtivos();
	}
}
