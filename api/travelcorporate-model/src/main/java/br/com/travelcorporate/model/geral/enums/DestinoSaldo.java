package br.com.travelcorporate.model.geral.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum DestinoSaldo {

	// @formatter:off
	SEM_SALDO(1, "Sem saldo"),
	DEVOLUCAO(2, "Devolução"),
	SALDO_POSITIVO_PROXIMA_VIAGEM(3, "Saldo positivo para próxima viagem"),
	ZERAR_SALDO_POSITIVO(4, "Zerar saldo positivo"),
	REEMBOLSO(5, "Reembolso"),
	ZERAR_SALDO_NEGATIVO (6, "Zerar saldo negativo"),
	SALDO_NEGATIVO_PROXIMA_VIAGEM(7, "Saldo negativo para próxima viagem");
	// @formatter:on

	private String descricao;
	private Integer valor;

	DestinoSaldo(Integer valor, String descricao) {
		this.setValor(valor);
		this.setDescricao(descricao);
	}

	@JsonCreator
	public static DestinoSaldo fromValue(String source) {
		if (source == null || source.trim().isEmpty()) {
			return null;
		}
		for (final DestinoSaldo status : DestinoSaldo.values()) {
			if (status.getValor().toString().equals(source.toUpperCase())) {
				return status;
			} else if (status.getDescricao().toUpperCase().equals(source.toUpperCase())) {
				return status;
			} else if (status.stringNome().equalsIgnoreCase(source)) {
				return status;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getDescricao();
	}

	private String stringNome() {
		return super.toString();
	}

	@JsonValue
	public String getDescricao() {
		return this.descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return this.valor;
	}

	private void setValor(Integer valor) {
		this.valor = valor;
	}
}
