package br.com.travelcorporate.model.lancamentos.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.tabelas.entities.dto.CidadeDTO;
import br.com.travelcorporate.model.tabelas.entities.dto.EstadoDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MultaDadosDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<FuncionarioDTO> funcionarios;

	private List<CidadeDTO> cidades;

	private List<EstadoDTO> estados;

	public List<CidadeDTO> getCidades() {
		if (this.cidades == null) {
			this.cidades = new ArrayList<>();
		}
		return this.cidades;
	}

	public void setCidades(List<CidadeDTO> cidades) {
		this.cidades = cidades;
	}

	public List<EstadoDTO> getEstados() {
		if (this.estados == null) {
			this.estados = new ArrayList<>();
		}
		return this.estados;
	}

	public void setEstados(List<EstadoDTO> estados) {
		this.estados = estados;
	}

	public List<FuncionarioDTO> getFuncionarios() {
		if (this.funcionarios == null) {
			this.funcionarios = new ArrayList<>();
		}
		return this.funcionarios;
	}

	public void setFuncionarios(List<FuncionarioDTO> funcionarios) {
		this.funcionarios = funcionarios;
	}
}
