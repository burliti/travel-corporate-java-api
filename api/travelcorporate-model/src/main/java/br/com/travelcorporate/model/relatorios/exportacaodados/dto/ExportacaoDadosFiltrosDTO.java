package br.com.travelcorporate.model.relatorios.exportacaodados.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.travelcorporate.model.cadastros.dto.CentroCustoDTO;
import br.com.travelcorporate.model.cadastros.dto.ClienteDTO;
import br.com.travelcorporate.model.cadastros.dto.FuncionarioDTO;
import br.com.travelcorporate.model.cadastros.dto.ProjetoDTO;
import br.com.travelcorporate.model.cadastros.dto.SetorDTO;
import br.com.travelcorporate.model.configuracao.dto.CategoriaDTO;
import br.com.travelcorporate.model.configuracao.dto.EmpresaDTO;
import br.com.travelcorporate.model.configuracao.dto.PerfilDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExportacaoDadosFiltrosDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<FuncionarioDTO> funcionarios;

	private List<SetorDTO> setores;

	private List<EmpresaDTO> empresas;

	private List<CategoriaDTO> categorias;

	private List<ProjetoDTO> projetos;

	private List<ClienteDTO> clientes;

	private List<CentroCustoDTO> centrosCusto;

	private List<PerfilDTO> perfis;

	public List<FuncionarioDTO> getFuncionarios() {
		if (this.funcionarios == null) {
			this.funcionarios = new ArrayList<>();
		}
		return this.funcionarios;
	}

	public void setFuncionarios(List<FuncionarioDTO> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public List<SetorDTO> getSetores() {
		if (this.setores == null) {
			this.setores = new ArrayList<>();
		}
		return this.setores;
	}

	public void setSetores(List<SetorDTO> setores) {
		this.setores = setores;
	}

	public List<EmpresaDTO> getEmpresas() {
		if (this.empresas == null) {
			this.empresas = new ArrayList<>();
		}
		return this.empresas;
	}

	public void setEmpresas(List<EmpresaDTO> empresas) {
		this.empresas = empresas;
	}

	public List<CategoriaDTO> getCategorias() {
		if (this.categorias == null) {
			this.categorias = new ArrayList<>();
		}
		return this.categorias;
	}

	public void setCategorias(List<CategoriaDTO> categorias) {
		this.categorias = categorias;
	}

	public List<ProjetoDTO> getProjetos() {
		if (this.projetos == null) {
			this.projetos = new ArrayList<>();
		}
		return this.projetos;
	}

	public void setProjetos(List<ProjetoDTO> projetos) {
		this.projetos = projetos;
	}

	public List<ClienteDTO> getClientes() {
		if (this.clientes == null) {
			this.clientes = new ArrayList<>();
		}
		return this.clientes;
	}

	public void setClientes(List<ClienteDTO> clientes) {
		this.clientes = clientes;
	}

	public List<CentroCustoDTO> getCentrosCusto() {
		if (this.centrosCusto == null) {
			this.centrosCusto = new ArrayList<>();
		}
		return this.centrosCusto;
	}

	public void setCentrosCusto(List<CentroCustoDTO> centrosCusto) {
		this.centrosCusto = centrosCusto;
	}

	public List<PerfilDTO> getPerfis() {
		if (this.perfis == null) {
			this.perfis = new ArrayList<>();
		}
		return this.perfis;
	}

	public void setPerfis(List<PerfilDTO> perfis) {
		this.perfis = perfis;
	}
}
