package br.com.travelcorporate.model.geral.exceptions.funcionario;

import br.com.travelcorporate.core.exceptions.BusinessException;

public class SenhaConfirmacaoInvalidaException extends BusinessException {
	private static final long serialVersionUID = 1L;

	public SenhaConfirmacaoInvalidaException(String message) {
		super(message);
	}

	public SenhaConfirmacaoInvalidaException() {
		super("Senha e confirmação de senha devem ser iguais!");
	}

}
